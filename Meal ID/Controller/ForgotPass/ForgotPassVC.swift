//
//  ForgotPassVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 06/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class ForgotPassVC: UIViewController {
    
    @IBOutlet var viewEmailRounded: UIView!
    @IBOutlet var tfEmail: UITextField!
    
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    
    //MARK:- button action
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSend(_ sender: UIButton) {
        if (tfEmail.text?.containsWhitespace == true) {
            setPresentAlert(withTitle: AppName, message: "White space not allowed in Email.")
            return
        } else if(tfEmail.text! == "") {
            setPresentAlert(withTitle: AppName, message: "Please enter your Email.")
            return
        } else if(isValidEmail(testStr: tfEmail.text!) != true) {
            setPresentAlert(withTitle: AppName, message: "invalid email address")
            tfEmail.text = ""
            return
        }  else {
            callForgotPasswordAPI()
        }
    }
    
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self

    }
    //MARK:- Setup Layout
    func setUpLayout() {
        viewEmailRounded.setroundedviewRadius(borderWidth: 1, borderColour: UIColor.clear)
    }
    
    
    // MARK:- API Calling & Parsing
      func callForgotPasswordAPI(){
          if utils.connected(){
              
              let param = [
                "email" : tfEmail.text!
              ]
              
              let webPath = BaseURL + ForgotPassword
              MBProgressHUD.showAdded(to: self.view, animated: true)
              let header = ["":""]
               self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: ForgotPassword, params: param, header: header)
          } else {
              setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
          }
      }
      
      func parseForgotPass(response: Any) {
        print(response)
          if let response = response as? NSDictionary {
              let success = response["success"] as! Bool
              if  success == false {
                let msg = response["message"] as! String
                   setPresentAlert(withTitle: AppName, message: msg)
                
              } else {
                  
                  let alertController = UIAlertController(title: AppName, message: "please visit your email", preferredStyle: .alert)
                  
                  let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                      self.navigationController?.popViewController(animated: true)
                  }
                  let Cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                      self.navigationController?.popViewController(animated: true)
                  }
                  
                  alertController.addAction(OKAction)
                  alertController.addAction(Cancel)
                  self.present(alertController, animated: true, completion: nil)
              }
          }
          MBProgressHUD.hide(for: self.view, animated: true)
      }
 }
 

// MARKS:- API Calling
extension ForgotPassVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == ForgotPassword {
                self.parseForgotPass(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
