//
//  WaterVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 03/03/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class WaterVC: UIViewController {
    
    @IBOutlet var vwWater: UIView!
    
     override func viewDidLoad() {
        super.viewDidLoad()
        
     }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let vv = UIView(frame: CGRect(x: 0, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: 0))

         vwWater.backgroundColor = UIColor.blue
        self.view.addSubview(vwWater)
        UIView.animate(withDuration: 5.0) {
            self.vwWater.frame = CGRect(x: 0, y: self.view.bounds.size.height / 3.0, width: self.view.bounds.size.width, height: self.view.bounds.size.height * 2 / 3.0)
        }
    }
}
