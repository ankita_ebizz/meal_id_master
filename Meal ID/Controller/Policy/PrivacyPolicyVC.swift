//
//  PrivacyPolicyVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 23/04/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD

class PrivacyPolicyVC: UIViewController {
    
    
    @IBOutlet var webView: WKWebView!
    
    var activityIndicator: UIActivityIndicatorView!
    
    let urlPath = "https://drive.google.com/file/d/1neUR_c6ewKCW4v5nz0g5JgOUmEwauD6J/view"
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        
        let url = URL (string: urlPath)
        let requestObj = URLRequest(url: url!)
         webView.load(requestObj)
        
    }
    
    @IBAction func btnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- Initialization
    func initialize() {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .medium
        activityIndicator.isHidden = true
        
        view.addSubview(activityIndicator)
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
}
extension PrivacyPolicyVC :WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
}
