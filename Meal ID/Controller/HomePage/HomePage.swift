//
//  HomePage.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 08/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class HomePage: UITabBarController,UITabBarControllerDelegate {
    
    let centerBtn = UIButton.init(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        
    }
    
    //MARK:- Initialization
    func initialize() {
        centerBtn.setTitle("", for: .normal)
        centerBtn.setBackgroundImage(UIImage(named: ic_add), for: .normal)
        
        centerBtn.clipsToBounds = true
        centerBtn.contentMode = .scaleAspectFit
        centerBtn.addTarget(self, action: #selector(handleTouchTabbarCenter), for: .touchUpInside)
        
        self.view.insertSubview(centerBtn, aboveSubview: self.tabBar)
    }
    
    override func viewDidLayoutSubviews() {
        
        let size = self.tabBar.frame.height
        //                    tabBar.frame.size.height = tabBar.frame.height + 10
        
        switch UIScreen.main.nativeBounds.height {
        case 1136,1334,1920,2208:
            tabBar.frame.size.height = 60
            tabBar.frame.origin.y = view.frame.height - 60
            
            centerBtn.frame.size = CGSize(width: size * 1.2, height: size * 1.2)
            centerBtn.center = CGPoint(x: self.tabBar.center.x , y: self.view.bounds.height - size-10)
            
            break
        default :
            centerBtn.frame.size = CGSize(width: size, height: size)
            centerBtn.center = CGPoint(x: self.tabBar.center.x , y: self.view.bounds.height - size)
        }
        self.centerBtn.RoundedButton()
        
    }
    @objc func handleTouchTabbarCenter(sender : UIButton) {
        let vc = storyboard?.instantiateViewController(identifier: id_GenerateMealPlanVC) as! GenerateMealPlanVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion : nil)
    }
    
    //MARK:- Setup Layout
    func setUpLayout() {
        UITabBar.appearance().unselectedItemTintColor = UIColor.white
        UITabBar.appearance().barTintColor = UIColor(red: 12/255, green: 12/255, blue: 14/255, alpha: 1.0)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(red: 235/255, green: 218/255, blue: 59/255, alpha: 1.0)], for:.selected)
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
        //        UITabBarItem.appearance()
    }
    
}
