//
//  SignInVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 06/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD
import FacebookLogin
import AuthenticationServices
import FBSDKLoginKit

class SignInVC: UIViewController {
    
    @IBOutlet var btnFB: UIButton!
    @IBOutlet var btnApple: UIButton!
    
    @IBOutlet var viewEmailRounded: UIView!
    @IBOutlet var viewPassRounded: UIView!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    
    let serviceManager = ServiceManager()
    let utils = Utils()
    var fbDict: [String : AnyObject]!
    var login_type = "0"
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    
    //MARK:- button action
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignUp(_ sender: UIButton) {
        
        // check for root view controller here then proceed to signup  // kakarot
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnShowPassOnClick(_ sender: UIButton) {
        if sender.tag == 0 {
            tfPassword.isSecureTextEntry = false
            sender.setImage(UIImage(named: "ic_eyeClose"), for: UIControl.State.normal)
            sender.tag += 1
        } else {
            tfPassword.isSecureTextEntry = true
            sender.setImage(UIImage(named: "ic_eye"), for: UIControl.State.normal)
            sender.tag -= 1
        }
    }
    
    @IBAction func btnSignInOnClick(_ sender: UIButton) {
        
        if (tfEmail.text?.containsWhitespace == true) {
            setPresentAlert(withTitle: AppName, message: "White space not allowed in Email.")
            return
        } else if(tfEmail.text! == "") {
            setPresentAlert(withTitle: AppName, message: "Please enter your Email.")
            return
        } else if(isValidEmail(testStr: tfEmail.text!) != true) {
            setPresentAlert(withTitle: AppName, message: "invalid email address")
            tfEmail.text = ""
            return
        } else if(tfPassword.text! == "") {
            setPresentAlert(withTitle: AppName, message: "Please enter your Password")
            return
        } else {
            callSignInAPI()
        }
    }
    
    @IBAction func btnFBOnClick(_ sender: UIButton) {
        loginWithReadPermissions()
     }
    @IBAction func btnAppleOnClick(_ sender: UIButton) {
        let vc = MainInStoryboard.instantiateViewController(withIdentifier: id_HomePage) as! HomePage
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnForgotPassOnClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: id_ForgotPassVC) as! ForgotPassVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self
    }
    
    //MARK:- Setup Layout
    func setUpLayout() {
        btnFB.setCircularButton()
        btnApple.setCircularButton()
        
        viewEmailRounded.setroundedviewRadius(borderWidth: 1, borderColour: UIColor.clear)
        viewPassRounded.setroundedviewRadius(borderWidth: 1, borderColour: UIColor.clear)
    }
    
    
    // MARK:- API Calling & Parsing
    func callSignInAPI(){
        tfEmail.resignFirstResponder()
        tfPassword.resignFirstResponder()
        
        if utils.connected(){
            let param = [
                "email" : tfEmail.text!,
                "password" : tfPassword.text!,
                "device_token" : FireBaseToken,
                "login_type" : 0
                ] as [String : Any]
            
            print(param)
            let webPath = BaseURL + LogIn
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["":""]
            
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: LogIn, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
            print("No Internet Connection")
        }
    }
    
    func parseLogIn(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
                //                self.navigationController?.popToRootViewController(animated: true)
            } else {
                utils.setUserData(data: (response["data"] as! [String : Any]))
                utils.setUserAuth(data: (response["data"] as! NSDictionary)["authToken"] as! String )
                //                print(utils.getUserData()!)
                //                print(utils.getUserAuth()!)
                
                utils.showToast(message: "Login done successfully.", uiView: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    let vc = MainInStoryboard.instantiateViewController(withIdentifier: id_HomePage) as! HomePage
                    user.setValue(true, forKey: K_isLogIn)
                    print(user.value(forKey: K_isLogIn)!)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    // MARK:- API Calling & Parsing
    func callSignInSocialAPI(_ email: String, login_type: String, social_key: String){
        if utils.connected(){
            let param = [
                "email" : email,
                "login_type" : login_type,
                "social_key" : social_key,
                "device_token" : FireBaseToken,
            ]
            
            print(param)
            let webPath = BaseURL + LogIn
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["":""]
            
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: LogIn, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
            print("No Internet Connection")
        }
    }
    func parseSocialLogIn(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                //                setPresentAlert(withTitle: "Please try again.", message: "")
                let vc = self.storyboard?.instantiateViewController(identifier: id_SignUpInfoVC) as! SignUpInfoVC
                vc.fbDict = fbDict
                vc.login_type = login_type
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                utils.setUserData(data: (response["data"] as! [String : Any]))
                utils.setUserAuth(data: (response["data"] as! NSDictionary)["authToken"] as! String )
                //                print(utils.getUserData()!)
                //                print(utils.getUserAuth()!)
                
                utils.showToast(message: "Login done successfully.", uiView: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    let vc = MainInStoryboard.instantiateViewController(withIdentifier: id_HomePage) as! HomePage
                    user.setValue(true, forKey: K_isLogIn)
                    print(user.value(forKey: K_isLogIn)!)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

// MARKS:- API Calling
extension SignInVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == LogIn {
                self.parseLogIn(response: response)
                self.parseSocialLogIn(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        print("Tagname: ",tagname ?? "" + "Error: " , error ?? "")
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

//MARK:- fb login permission
extension SignInVC {
    // MARK:- Facebook Login
    func loginManagerDidComplete(_ result: LoginResult) {
        let alertController: UIAlertController
        switch result {
        case .cancelled:
            alertController = UIAlertController(
                title: "Login Cancelled",
                message: "User cancelled login.",
                preferredStyle: .alert
            )
        case .failed(let error):
            alertController = UIAlertController(
                title: "Login Fail",
                message: "Login failed with error \(error)",
                preferredStyle: .alert
            )
        case .success(let grantedPermissions, let declinedPermissions, let accessToken):
            alertController = UIAlertController(
                title: "Login Success",
                message: "Login succeeded with granted permissions: \(grantedPermissions)",
                preferredStyle: .alert
            )
            getFBUserData()
        }
        
        alertController.addAction(UIAlertAction(title: "Done", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    func getFBUserData(){
        if(AccessToken.current != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.fbDict = result as! [String : AnyObject]
                    self.login_type = "1"
                    self.callSignInSocialAPI(self.fbDict["email"] as! String, login_type: self.login_type, social_key: self.fbDict["id"] as! String)
                }
            })
        }
    }
    
    private func loginWithReadPermissions() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let loginManager = LoginManager()
        loginManager.logIn(
            permissions: [.publicProfile, .email], //, .userFriends
            viewController: self
        ) { result in
            self.loginManagerDidComplete(result)
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}
