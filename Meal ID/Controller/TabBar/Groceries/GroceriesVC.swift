//
//  GroceriesVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 11/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class GroceriesVC: UIViewController {
    
    @IBOutlet var tblGroceries: UITableView!
    @IBOutlet var vwDownloadPdf: UIView!
    
    var arrItem = NSArray()
    var pdfUrlTodownload = String()
    var pdfURL: URL!
    
    let serviceManager = ServiceManager()
    let utils = Utils()
    var selectIndex = IndexPath()
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        
        let nibName = UINib(nibName: id_GroceriesMenuTVC, bundle:nil)
        tblGroceries.register(nibName, forCellReuseIdentifier: id_GroceriesMenuTVC)
    }
    
    //MARK:- button click action
    @IBAction func btnNotiAlert(_ sender: UIButton) {
        //        let vc = MainInStoryboard.instantiateViewController(identifier: "NotificationListVc") as! NotificationListVc
        //        self.navigationController?.pushViewController(vc, animated: true)
        let vc = MainInStoryboard.instantiateViewController(identifier: "UpdateWeightFatVC") as! UpdateWeightFatVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPdfDownload(_ sender: UIButton) {
        //        guard let urlString = URL(string: pdfUrlTodownload) else { return }
        
        let urlString = pdfUrlTodownload
        print(pdfUrlTodownload)
        let url = URL(string: urlString)
        let fileName = String((url!.lastPathComponent)) as NSString
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let fileURL = URL(string: urlString)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL!)
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
//                    self.setPresentAlert(withTitle: AppName, message: "pdfsuccessfully downloaded.")
                }
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    do {
                        //Show UIActivityViewController to save the downloaded file
                        let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                        for indexx in 0..<contents.count {
                            if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
                                let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                self.present(activityViewController, animated: true, completion: nil)
                            }
                        }
                    }
                    catch (let err) {
                        print("error: \(err)")
                    }
                } catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
            } else {
                print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
            }
        }
        task.resume()
    }
    
    //    func showSavedPdf(url:String, fileName:String) {
    //        if #available(iOS 10.0, *) {
    //            do {
    //                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    //                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
    //                for url in contents {
    //                    if url.description.contains("\(fileName).pdf") {
    //                        // its your file! do what you want with it!
    //
    //                    }
    //                }
    //            } catch {
    //                print("could not locate pdf file !!!!!!!")
    //            }
    //        }
    //    }
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self
 
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
    override func viewWillAppear(_ animated: Bool) {
                callGroceriesListAPI()
        callGroceriePdfAPI()
    }
    // MARK:- API Calling & Parsing
    func callGroceriePdfAPI() {
        if utils.connected(){
            let webPath = BaseURL + GroceriePdf
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: GroceriePdf, params: ["":""], header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    func parseGroceriePdf(response: Any) {
                print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
                vwDownloadPdf.isHidden = true
            } else {
                let msg = response["message"] as? String
                if msg == "user don't have any meal plan" {
                    vwDownloadPdf.isHidden = true
                 } else {
                    pdfUrlTodownload = response["data"] as? String ?? ""
                    vwDownloadPdf.isHidden = false
                }
             }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func callGroceriesListAPI() {
        if utils.connected(){
            let webPath = BaseURL + GroceriesList
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            self.serviceManager.callWebServiceWithGET(webpath: webPath, withTag: GroceriesList, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    func parseUpdateDietType(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                let msg = response["message"] as! String
                if msg == "You don't have any meal plan generated." {
                    setPresentAlert(withTitle: AppName, message: msg)
                    vwDownloadPdf.isHidden = true
                 } else {
                    vwDownloadPdf.isHidden = true
                    setPresentAlert(withTitle: AppName, message: "Please try again.")
                }
            } else {
                vwDownloadPdf.isHidden = false
                arrItem = (response["data"] as! NSArray)
                //                print(arrItem)
                tblGroceries.reloadData()
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
//extension GroceriesVC : URLSessionDownloadDelegate {
//    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
//        print("downloadLocation:", location)
//        // create destination URL with the original pdf name
//        guard let url = downloadTask.originalRequest?.url else { return }
//        let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
//        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
//        // delete original copy
//        try? FileManager.default.removeItem(at: destinationURL)
//        // copy from temp to Document
//        do {
//            try FileManager.default.copyItem(at: location, to: destinationURL)
//            self.pdfURL = destinationURL
//            //            var fileUrl = URL(string: pdfUrlTodownload)
//
//            //            fileUrl = destinationURL
//        } catch let error {
//            print("Copy Error: \(error.localizedDescription)")
//        }
//    }
//
//}

// MARKS:- API Calling
extension GroceriesVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == GroceriesList {
                self.parseUpdateDietType(response: response)
            } else if tagname == GroceriePdf {
                self.parseGroceriePdf(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        print("Tagname: ",tagname ?? "" + "Error: " , error ?? "")
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

extension GroceriesVC : UITableViewDelegate,UITableViewDataSource {
    
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        return arrType?.count ?? 0
    //    }
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 50
    //    }
    //
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let view = UIView(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width, height: 50))
    //        view.backgroundColor = .clear
    //        let lbl = UILabel(frame: view.frame)
    //        lbl.font = UIFont.boldSystemFont(ofSize: 19)
    //        lbl.text = arrType?[section].capitalized
    //        lbl.textColor = UIColor(red: 77/255, green: 77/255, blue: 77/255, alpha: 1.0)
    //        view.addSubview(lbl)
    //
    //        return view
    //    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItem.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_GroceriesMenuTVC, for: indexPath) as! GroceriesMenuTVC
        cell.selectionStyle = .none
        let subDictionary = arrItem[indexPath.row] as? NSDictionary
        
        let types = subDictionary?.filter({ (arg) -> Bool in
            let (key, value) = arg
            return (key as! String) != "mealplantype" && (key as! String) != "title"
        }).map({ (arg0) -> String in
            let (key, value) = arg0
            return value as! String
        })
        
        cell.setSubTitle(types ?? [])
        
        let title = subDictionary!["title"] as? String
        cell.lblTitlename.text = title?.capitalized
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectIndex == indexPath {
            let subDictionary = arrItem[indexPath.row] as? NSDictionary
            let types = subDictionary?.filter({ (arg) -> Bool in
                let (key, value) = arg
                return (key as! String) != "mealplantype" && (key as! String) != "title"
            }).map({ (arg0) -> String in
                let (key, value) = arg0
                return value as! String
            }) ?? []
            
            return CGFloat((types.count * 50) - 1 + 60)
        }
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for myCell in (tableView.visibleCells as! [GroceriesMenuTVC]) {
            myCell.btnCheck.setImage(UIImage(named: "ic_right_arrow"), for: .normal)
        }
        
        let myCell = tableView.cellForRow(at: indexPath) as! GroceriesMenuTVC
        if selectIndex == indexPath {
            selectIndex = IndexPath()
        } else {
            selectIndex = indexPath
            myCell.btnCheck.setImage(UIImage(named: "ic_bottom_arrow"), for: .normal)
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
