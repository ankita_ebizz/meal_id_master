//
//  GroceriesMenuTVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 11/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class GroceriesMenuTVC: UITableViewCell {
    
    @IBOutlet var lblTitlename: UILabel!
    @IBOutlet var btnCheck: UIButton!
    @IBOutlet var lblSubtitle: [UILabel]!
    
    var btnCheckUncheckOnClick: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func btnCheckUncheckOnClick(_ sender: UIButton) {
        btnCheckUncheckOnClick?()
    }
    
    func setSubTitle(_ arrStr: [String]) {
        let cnt = arrStr.count
        var i=0;
        while i<cnt {
            lblSubtitle[i].text = arrStr[i]
            i += 1;
        }
    }
}
