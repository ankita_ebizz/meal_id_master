//
//  ActivityVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 17/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import FSCalendar
import Charts
import MBProgressHUD

private class CubicLineSampleFillFormatter: IFillFormatter {
    func getFillLinePosition(dataSet: ILineChartDataSet, dataProvider: LineChartDataProvider) -> CGFloat {
        return -10
    }
}

class ActivityVC: UIViewController {
    
    @IBOutlet var vwNoChartDataAvailable: UIView!
    @IBOutlet var chartView: LineChartView!
    @IBOutlet var calendar: FSCalendar!
    
    @IBOutlet var viewCircleProgressCurrentWeight: CircleProgressView!
    @IBOutlet var viewCircleProgressCurrentBodyFat: CircleProgressView!
    @IBOutlet var viewCircleProgressCurrentLBM: CircleProgressView!
    @IBOutlet var viewCircleProgressCurrentBMR: CircleProgressView!
    
    @IBOutlet var lblWeightValue: UILabel!
    @IBOutlet var lblCurrentWeightUnit: UILabel!
    @IBOutlet var lblBodyFat: UILabel!
    @IBOutlet var lblLBMvalue: UILabel!
    @IBOutlet var lblCurrentLBMUnit: UILabel!
    @IBOutlet var lblBMRvalue: UILabel!
    
    // filter
    @IBOutlet var vwFilterChartData: UIView!
    @IBOutlet var btnByDate: UIButton!
    @IBOutlet var tfStartDate: UITextField!
    @IBOutlet var tfEndDate: UITextField!
    @IBOutlet var cnstForBydateTFHieght: NSLayoutConstraint!
    
    @IBOutlet var vwBydate: UIView!
    @IBOutlet var btnByLastWeek: UIButton!
    @IBOutlet var btnByLastMonth: UIButton!
    
    
    let utils = Utils()
    let serviceManager = ServiceManager()
    
    var filterType = Int()
    var startDate = String()
    var endDate = String()
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceManager.delegate = self
        
        initialize()
        setUpLayout()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        calendar.reloadData()
        callUserCurrentStatusAPI()
        
        filterType = 1
        callUserActivityGraphAPI()
    }
    
    @objc func handleStartDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd"
        tfStartDate.text = dateFormatter.string(from: sender.date)
    }
    @objc func handleEndDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd"
        tfEndDate.text = dateFormatter.string(from: sender.date)
    }
    
    //MARK:- button click action
    
    @IBAction func btnCloseFilter(_ sender: UIButton) {
        vwFilterChartData.isHidden = true
    }
    
    @IBAction func btnApplyFilter(_ sender: UIButton) {
        callUserActivityGraphAPI()
        
    }
    @IBAction func btnOpenFilter(_ sender: UIButton) {
        vwFilterChartData.isHidden = false
    }
    
    @IBAction func btnByDateSelect(_ sender: UIButton) {
        if btnByDate.currentImage == UIImage(named: "ic_Uncheckbox") {
            sender.setImage(UIImage(named: "ic_checkboxEllipse"), for: .normal)
            cnstForBydateTFHieght.constant = 120
            vwBydate.isHidden = false
            
            btnByLastWeek.setImage(UIImage(named: "ic_Uncheckbox"), for: .normal)
            btnByLastMonth.setImage(UIImage(named: "ic_Uncheckbox"), for: .normal)
            
            filterType = 3
        } else {
            sender.setImage(UIImage(named: "ic_Uncheckbox"), for: .normal)
            cnstForBydateTFHieght.constant = 40
            vwBydate.isHidden = true
        }
    }
    @IBAction func btnByLastWeekSelect(_ sender: UIButton) {
        if btnByLastWeek.currentImage == UIImage(named: "ic_Uncheckbox") {
            sender.setImage(UIImage(named: "ic_checkboxEllipse"), for: .normal)
            btnByDate.setImage(UIImage(named: "ic_Uncheckbox"), for: .normal)
            btnByLastMonth.setImage(UIImage(named: "ic_Uncheckbox"), for: .normal)
            cnstForBydateTFHieght.constant = 40
            vwBydate.isHidden = true
            
            filterType = 1
            
        } else {
            sender.setImage(UIImage(named: "ic_Uncheckbox"), for: .normal)
        }
    }
    @IBAction func btnByLastMonthSelect(_ sender: UIButton) {
        if btnByLastMonth.currentImage == UIImage(named: "ic_Uncheckbox") {
            sender.setImage(UIImage(named: "ic_checkboxEllipse"), for: .normal)
            btnByDate.setImage(UIImage(named: "ic_Uncheckbox"), for: .normal)
            btnByLastWeek.setImage(UIImage(named: "ic_Uncheckbox"), for: .normal)
            cnstForBydateTFHieght.constant = 40
            vwBydate.isHidden = true
            
            filterType = 2
        } else {
            sender.setImage(UIImage(named: "ic_Uncheckbox"), for: .normal)
        }
    }
    
    
    
    @IBAction func btnNotiAlert(_ sender: UIButton) {
        //        let vc = MainInStoryboard.instantiateViewController(identifier: "NotificationListVc") as! NotificationListVc
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        let vc = MainInStoryboard.instantiateViewController(identifier: "UpdateWeightFatVC") as! UpdateWeightFatVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnUploadImage(_ sender: UIButton) {
        let vc = MainInStoryboard.instantiateViewController(identifier: id_UploadPhotoVC) as! UploadPhotoVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Initialization
    func initialize() {
        chartSetUp()
        vwFilterChartData.isHidden = true
        vwNoChartDataAvailable.isHidden = true
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        calendar.scope = .week
        calendar.appearance.titleTodayColor = yellowColour
        
        //     calendar.appearance.subtitleTodayColor = UIColor.purple
        //     calendar.appearance.titleTodayColor = UIColor.YellowColourCalendarBG
        
        tfEndDate.layer.cornerRadius = 5
        tfStartDate.layer.cornerRadius = 5
        tfStartDate.delegate = self
        tfEndDate.delegate = self
        cnstForBydateTFHieght.constant = 40
        vwBydate.isHidden = true
    }
    
    // MARK:- API Calling & Parsing
    func callUserCurrentStatusAPI() {
        if utils.connected(){
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
            dateFormatter.dateFormat = "yyyy-MM-dd" // "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            let webPath = BaseURL + UserCurrentStatus + "?pickedDate=\(dateFormatter.string(from: Date()))T00:00:00.000Z"
            
            self.serviceManager.callWebServiceWithGET(webpath: webPath, withTag: UserCurrentStatus, header: header)
            
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parseUserCurrentStatus(response: Any) {
        if let response = response as? NSDictionary {
            print(response)
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                let data = response["data"] as! NSDictionary
                lblWeightValue.text = String(data["weight"] as? Double ?? 0.0)
                lblBodyFat.text = String(data["bodyfat"] as? Double ?? 0.0) + " %"
                lblLBMvalue.text = String(data["lbm"] as? Double ?? 0.0)
                lblBMRvalue.text = String(data["bmr"] as? Double ?? 0.0)
                
                viewCircleProgressCurrentWeight.progress = CGFloat(data["weight"] as! Double / 100.0)
                viewCircleProgressCurrentBodyFat.progress = CGFloat(data["bodyfat"] as! Double / 100.0)
                viewCircleProgressCurrentLBM.progress = CGFloat(data["lbm"] as! Double / 100.0)
                viewCircleProgressCurrentBMR.progress = 0.80
                
                let weight_type = data["weightType"] as? Int
                if weight_type == 1 {
                    lblCurrentWeightUnit.text = "lbs"
                    lblCurrentLBMUnit.text = "lbs"
                } else {
                    lblCurrentWeightUnit.text = "kg"
                    lblCurrentLBMUnit.text = "kg"
                }
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func callUserActivityGraphAPI(){
        if utils.connected(){
            var param = [
                "filterType" : filterType,
                "startdate" : "0",
                "enddate" : "0"
                ] as [String : Any]
            
            if filterType == 3 {
                param["filterType"] = filterType;
                param["startdate"] = tfStartDate.text! + "T00:00:00.000Z";
                param["enddate"] = tfEndDate.text! + "T00:00:00.000Z"
            }
            
            print(param)
            let webPath = BaseURL + UserActivityGraph
            MBProgressHUD.showAdded(to: self.view, animated: true)
//            let header = ["Authorization":utils.getUserAuth()!]
           let header = ["Authorization":TempAuth]

            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: UserActivityGraph, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    
    func parseUserActivityGraph(response: Any) {
        if let response = response as? NSDictionary {
            print(response)
            let success = response["success"] as! Bool
            if  success == false {
                let code = response["code"] as! Int
                if code == 500 {
                    setPresentAlert(withTitle: AppName, message: response["message"] as! String)
                } else {
                    setPresentAlert(withTitle: "Please try again.", message: "")
                }
                vwFilterChartData.isHidden = true
                vwNoChartDataAvailable.isHidden = false
                
            } else {
                let data = response["data"] as! NSDictionary
                vwFilterChartData.isHidden = true
                vwNoChartDataAvailable.isHidden = true
                
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}
// MARK:- textfield delegate method
extension ActivityVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfStartDate {
            
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleStartDatePicker(sender:)), for: .valueChanged)
            
        } else if textField == tfEndDate {
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleEndDatePicker(sender:)), for: .valueChanged)
        }
    }
}

// MARK:- API Calling
extension ActivityVC : ServiceManagerDelegate {
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == UserCurrentStatus {
                self.parseUserCurrentStatus(response: response)
            } else if tagname == UserActivityGraph {
                self.parseUserActivityGraph(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
 
// MARK:- chart delegate method
extension ActivityVC : ChartViewDelegate {
    
    func chartSetUp() {
        super.viewDidLoad()
        
        chartView.delegate = self
        
        chartView.setViewPortOffsets(left: 0, top: 20, right: 0, bottom: 0)
        chartView.backgroundColor = UIColor.black
        
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = false
        chartView.maxHighlightDistance = 300
        
        chartView.xAxis.enabled = false
        //        chartView.x
        let yAxis = chartView.leftAxis
        yAxis.labelFont = UIFont(name: FontName, size:12)!
        yAxis.setLabelCount(6, force: false)
        yAxis.labelTextColor = .white
        yAxis.labelPosition = .insideChart
        yAxis.axisLineColor = .white
        
        let xAxis = chartView.xAxis
        xAxis.labelFont = UIFont(name: FontName, size:12)!
        xAxis.setLabelCount(6, force: false)
        xAxis.labelTextColor = .white
        xAxis.labelPosition = .bottomInside
        xAxis.axisLineColor = .white
        
        chartView.rightAxis.enabled = false
        chartView.xAxis.enabled = true
        chartView.legend.enabled = false
        chartView.gridBackgroundColor = .green
        
        chartView.animate(xAxisDuration: 2, yAxisDuration: 2)
        setDataCount(11, range: 28, BodyFatCount: 5, BodyFatRange: 18)
    }
    func setDataCount(_ count: Int, range: UInt32 , BodyFatCount : Int, BodyFatRange : UInt32) {
        
        let yVals1 = (0..<count).map { (i) -> ChartDataEntry in
            let mult = range + 1
            let val = Double(arc4random_uniform(mult) + 20)
            return ChartDataEntry(x: Double(i), y: val)
        }
        
        let set1 = LineChartDataSet(entries: yVals1, label: "DataSet 1")
        set1.mode = .cubicBezier
        set1.drawCirclesEnabled = false
        set1.lineWidth = 3.0
        set1.colors = [orangeColour]
        set1.circleRadius = 4
        set1.setCircleColor(.white)
        set1.highlightColor = UIColor.white
        
        set1.drawFilledEnabled = true
        set1.fillAlpha = 0.20
        set1.fillColor = orangeColour
        set1.drawHorizontalHighlightIndicatorEnabled = false
        set1.fillFormatter = CubicLineSampleFillFormatter()
        
        let xVals1 = (0..<BodyFatCount).map { (i) -> ChartDataEntry in
            let mult = BodyFatRange + 1
            let val = Double(arc4random_uniform(mult) + 20)
            return ChartDataEntry(x: Double(i), y: val)
        }
        
        let set2 = LineChartDataSet(entries: xVals1, label: "DataSet 2")
        set2.mode = .cubicBezier
        set2.drawCirclesEnabled = false
        set2.lineWidth = 3.0
        set2.colors = [cyanColour]
        set2.circleRadius = 4
        set2.setCircleColor(.white)
        set2.highlightColor = UIColor.white
        
        set2.drawFilledEnabled = true
        set2.fillAlpha = 0.20
        set2.fillColor = cyanColour
        set2.drawHorizontalHighlightIndicatorEnabled = false
        set2.fillFormatter = CubicLineSampleFillFormatter()
        
        
        let LBMVal = (0..<count).map { (i) -> ChartDataEntry in
            let mult = range + 1
            let val = Double(arc4random_uniform(mult) + 20)
            return ChartDataEntry(x: Double(i), y: val)
        }
        
        let set3 = LineChartDataSet(entries: LBMVal, label: "DataSet 3")
        set3.mode = .cubicBezier
        set3.drawCirclesEnabled = false
        set3.lineWidth = 3.0
        set3.colors = [yellowColour]
        set3.circleRadius = 4
        set3.setCircleColor(.white)
        set3.highlightColor = UIColor.white
        
        set3.drawFilledEnabled = true
        set3.fillAlpha = 0.20
        set3.fillColor = yellowColour
        set3.drawHorizontalHighlightIndicatorEnabled = false
        set3.fillFormatter = CubicLineSampleFillFormatter()
        
        let BMRVal = (0..<BodyFatCount).map { (i) -> ChartDataEntry in
            let mult = BodyFatRange + 1
            let val = Double(arc4random_uniform(mult) + 20)
            return ChartDataEntry(x: Double(i), y: val)
        }
        
        let set4 = LineChartDataSet(entries: BMRVal, label: "DataSet 3")
        set4.mode = .cubicBezier
        set4.drawCirclesEnabled = false
        set4.lineWidth = 3.0
        set4.colors = [greenColour]
        set4.circleRadius = 4
        set4.setCircleColor(.white)
        set4.highlightColor = UIColor.white
        
        set4.drawFilledEnabled = true
        set4.fillAlpha = 0.20
        set4.fillColor = greenColour
        set4.drawHorizontalHighlightIndicatorEnabled = false
        set4.fillFormatter = CubicLineSampleFillFormatter()
        
        let FinalData = LineChartData(dataSets: [set1,set2,set3,set4])
        FinalData.setValueFont(UIFont(name: FontName, size: 9)!)
        FinalData.setDrawValues(false)
        chartView.data = FinalData
    }
}

