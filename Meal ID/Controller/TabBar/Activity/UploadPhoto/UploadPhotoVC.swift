//
//  UploadPhotoVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 18/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class UploadPhotoVC: UIViewController{
    
    @IBOutlet var tblData: UITableView!
    @IBOutlet var imgUploadPhoto: UIImageView!
    
    @IBOutlet var btnWeightVal: UIButton!
    @IBOutlet var btnBodyFatVal: UIButton!
    @IBOutlet var btnLBMVal: UIButton!
    
    var varProfileImgSelected = false
    let serviceManager = ServiceManager()
    var arrItem = NSArray()
    let utils = Utils()
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        
        let nibName = UINib(nibName: id_UploadPhotoTVC, bundle:nil)
        tblData.register(nibName, forCellReuseIdentifier: id_UploadPhotoTVC)
    }
    
    //MARK:- button click action
    @IBAction func btnAddPhoto(_ sender: UIButton) {
        openCameraMenu()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveData(_ sender: UIButton) {
        let weight = self.btnWeightVal.titleLabel?.text
        let bodyFat = self.btnBodyFatVal.titleLabel?.text
        let lbm = self.btnLBMVal.titleLabel?.text
        
        if(!varProfileImgSelected) {
            setPresentAlert(withTitle: AppName, message: "Please attach Photo from Gallery or Camera.")
        } else if(weight == "------") {
            setPresentAlert(withTitle: AppName, message: "Please select Body Weight.")
        } else if(bodyFat == "------") {
            setPresentAlert(withTitle: AppName, message: "Please select Body Fat.")
        } else if(lbm == "NA") {
            setPresentAlert(withTitle: AppName, message: "Please select LBM.")
        } else {
            callAddPhotoAPI()
            self.btnWeightVal.setTitle("------", for: .normal)
            self.btnBodyFatVal.setTitle("------", for: .normal)
            self.btnLBMVal.setTitle("NA", for: .normal)
            imgUploadPhoto.image = UIImage(named: "ic_placeholder")
        }
    }
    
    //MARK:- custom picker function for actionsheet
    func pickerActionsheet() {
        
        let alertController = UIAlertController(title: "", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let margin:CGFloat = 10
        //            let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: self.view.frame.height * 0.40)
        let rect = CGRect(x: margin, y: margin, width: UIScreen.main.bounds.width - margin * 3.8 , height: self.view.frame.height * 0.38)
        
        let customView = UIView(frame: rect)
        customView.backgroundColor = .clear
        
        DispatchQueue.main.async {
            
            let myView = Bundle.main.loadNibNamed(id_PickerForActivity, owner: self, options: nil)![0] as! UIView
            myView.frame = CGRect(x: 0, y: 0, width: customView.frame.width , height: self.view.frame.height * 0.38)
            customView .addSubview(myView)
        }
        customView.layer.cornerRadius = 10
        customView.layer.fillMode = .backwards
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.60)
        
        alertController.view.addConstraint(height)
        alertController.view.addSubview(customView)
        
        alertController.overrideUserInterfaceStyle = .dark
        
        let doneAction = UIAlertAction(title: "Done", style: .default, handler: {
            (alert: UIAlertAction!) in
            
            if PickerForActivityTracker == "WeightpickerValForActivity" {
                self.btnWeightVal.setTitle(leftPickerValue + "." + RightPickerValue, for: .normal)
            } else if PickerForActivityTracker == "HeightpickerValForActivity" {
                self.btnBodyFatVal.setTitle(leftPickerValue + "." + RightPickerValue, for: .normal)
            } else {
                self.btnLBMVal.setTitle(leftPickerValue + "." + RightPickerValue, for: .normal)
            }
            let deadlineTime = DispatchTime.now() + 0.2
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                let weight = self.btnWeightVal.titleLabel!.text!
                let bodyFat = self.btnBodyFatVal.titleLabel!.text!
                if(weight != "------" && bodyFat != "------")
                {
                    if let w = Float(weight) {
                        if let b = Float(bodyFat) {
                            let x = (w - (w * b / 100))
                            self.btnLBMVal.setTitle("\(Float(round(10*x)/10))", for: .normal)
                        }
                    }
                }
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        
        doneAction.setValue(UIColor.white, forKey: "titleTextColor")
        cancelAction.setValue(UIColor.white, forKey: "titleTextColor")
        
        alertController.addAction(doneAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
        return
    }
    
    @IBAction func btnInputValueOnClick(_ sender: UIButton) {
        if sender.tag == 100 {
            PickerForActivityTracker = "WeightpickerValForActivity"
            pickerActionsheet()
        } else if sender.tag == 101 {
            PickerForActivityTracker = "HeightpickerValForActivity"
            pickerActionsheet()
        } else {
            //     PickerForActivityTracker = "FatpickerValForActivity"
            //    pickerActionsheet()
        }
    }
    
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self
        
        callPhotoListAPI()
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        imgUploadPhoto.roundedProfileImage(borderWidth: 1, borderColour: UIColor.clear)
    }
    
    // MARK:- Call API
    func callAddPhotoAPI()
    {
        if utils.connected(){
            let webPath = BaseURL + UserUploadPhoto
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            
            let weight = self.btnWeightVal.titleLabel?.text
            let bodyFat = self.btnBodyFatVal.titleLabel?.text
            let lbm = self.btnLBMVal.titleLabel?.text
            
            var param = [String: Any]()
            
            param["weight"] = weight
            param["bodyfat"] = bodyFat
            param["lbm"] = lbm
            
            let img = AttachmentViewModel()
            img.Image = imgUploadPhoto.image!
            img.ImageFileName = "photo"
            
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: UserUploadPhoto, params: param, imgArray: [img], header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    func parseAddUserPhoto(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                arrItem = response["data"] as! NSArray
                print(arrItem)
                tblData.reloadData()
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    func callPhotoListAPI()
    {
        if utils.connected(){
            let webPath = BaseURL + Userphotolist
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            
            self.serviceManager.callWebServiceWithGET(webpath: webPath, withTag: Userphotolist, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    func parseGetUserPhotoList(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                arrItem = (response["data"] as! NSArray)
                print(arrItem)
                tblData.reloadData()
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    func callPhotoDeleteAPI(_ id: String)
    {
        if utils.connected(){
            let webPath = BaseURL + userdeletephoto + id
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            
            self.serviceManager.callWebServiceWithDELETE(webpath: webPath, withTag: userdeletephoto, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    func parseDeleteUserPhoto(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                arrItem = (response["data"] as! NSArray)
                print(arrItem)
                tblData.reloadData()
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
//MARK:- tableview method
extension UploadPhotoVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_UploadPhotoTVC, for: indexPath) as! UploadPhotoTVC
        cell.selectionStyle = .none
        
        let dict = arrItem[indexPath.row] as! NSDictionary
        
        cell.imgUploadedPhoto.sd_setImage(with: URL(string: dict["photourl"] as! String), completed: nil)
        cell.lblWeight.text = "\(dict["userweight"] as! Double)"
        cell.lblBodyFat.text = "\(dict["userbodyfat"] as! Double)"
        cell.lblLBM.text = "\(dict["userlbm"] as! Double)"
        cell.btnDeletePost = {
            let id = dict["_id"] as! String
            
            
            let alertController = UIAlertController(title: AppName, message: "Are you sure you want to permanently delete this record?", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                self.callPhotoDeleteAPI(id)
            }
            let Cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(OKAction)
            alertController.addAction(Cancel)
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from:dict["photodate"] as! String)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        cell.lblDate.text = formatter.string(from: date)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}

//MARK:- open camera/gallery method
extension UploadPhotoVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    func openCameraMenu() {
        let alertCon = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alertCon.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alertCon.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alertCon.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertCon, animated: true, completion: nil)
    }
    
    //MARK:- open camera method
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
            
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- open gallery method
    func openGallary() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            imgUploadPhoto.contentMode = .scaleToFill
            imgUploadPhoto.image = pickedImage
            varProfileImgSelected = true
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARKS:- API Response
extension UploadPhotoVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == Userphotolist
            {
                self.parseGetUserPhotoList(response: response)
            }
            else if(tagname == UserUploadPhoto)
            {
                self.parseAddUserPhoto(response: response)
            }
            else if(tagname == userdeletephoto)
            {
                self.parseDeleteUserPhoto(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        print("Tagname: ",tagname ?? "" + "Error: " , error ?? "")
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
