//
//  PickerForActivity.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 20/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class PickerForActivity: UIView {
    
    @IBOutlet var pickerLeft: UIPickerView!
    @IBOutlet var pickerRight: UIPickerView!
    @IBOutlet var lblTitle: UILabel!
    
    var InitialWeight = [String]()
    let WeightGram = ["0","1","2","3","4","5","6","7","8","9"]
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        pickerLeft.dataSource = self
        pickerLeft.delegate = self
        pickerRight.dataSource = self
        pickerRight.delegate = self
        pickerLeft.selectRow(3, inComponent: 0, animated: true)
        pickerRight.selectRow(3, inComponent: 0, animated: true)
        
        if PickerForActivityTracker == "WeightpickerValForActivity" {
            lblTitle.text! = "How much do you weigh ?"
        } else if PickerForActivityTracker == "HeightpickerValForActivity" {
            lblTitle.text! = "What is your body fat %"
        } else {
            lblTitle.text! = "What is your body LBM ?"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if PickerForActivityTracker == "WeightpickerValForActivity" {
            InitialWeight = ["30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89"," 90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110"]
        } else if PickerForActivityTracker == "HeightpickerValForActivity" {
            InitialWeight = ["60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89"," 90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110"]
        } else {
            //            FatpickerValForActivity
            InitialWeight = ["01","02","03","04","05","06","07","08","09","10"]
        }
    }
}

extension PickerForActivity : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView,numberOfRowsInComponent component: Int) -> Int {
        
        pickerView.subviews[1].backgroundColor = UIColor.white
        pickerView.subviews[2].backgroundColor = UIColor.white
        pickerView.subviews[1].frame.size.height = 1.0
        pickerView.subviews[2].frame.size.height = 1.0
        
        if pickerView == pickerLeft {
            return InitialWeight.count
        } else  {
            return WeightGram.count
        }
    }
    func pickerView(_ pickerView: UIPickerView,titleForRow row: Int,forComponent component: Int) -> String? {
        
        if pickerView == pickerLeft {
            return InitialWeight[row]
        } else  {
            return WeightGram[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont.systemFont(ofSize: 24)
        label.textAlignment = .center
        label.textColor = UIColor.white
        if pickerView == pickerLeft {
            label.text =  InitialWeight[row]
        } else {
            label.text =  WeightGram[row]
        }
        return label
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerLeft {
            leftPickerValue = InitialWeight[row]
            RightPickerValue = String(pickerRight.selectedRow(inComponent: 0))
        } else {
            RightPickerValue = WeightGram[row]
            leftPickerValue = String(pickerLeft.selectedRow(inComponent: 0))
        }
    }
}
