//
//  UploadPhotoTVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 19/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class UploadPhotoTVC: UITableViewCell {
    
    @IBOutlet var imgUploadedPhoto: UIImageView!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var lblBodyFat: UILabel!
    @IBOutlet var lblLBM: UILabel!
    
    var btnDeletePost: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func btnDeletePost(_ sender: UIButton) {
        btnDeletePost?()
    }
    
}
