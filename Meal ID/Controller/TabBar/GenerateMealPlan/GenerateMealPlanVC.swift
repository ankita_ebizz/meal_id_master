//
//  GenerateMealPlanVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 12/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class InstructionTVC: UITableViewCell {
    @IBOutlet var lblNumberTitle: UILabel!
    @IBOutlet var lblInstructionTitle: UILabel!
}

class GenerateMealPlanVC: UIViewController,ChooseLevelProtocol {
    
    func ChooseLevelVCSelect() {
        let vc = MainInStoryboard.instantiateViewController(identifier: id_ChooseLevelVC) as! ChooseLevelVC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBOutlet var lblCurrentPageValue: UILabel!
    @IBOutlet var clvGenerateMealSteps: UICollectionView!
    @IBOutlet var btnNextStep: CutomButtonUi!
    
    @IBOutlet var vwInstruction: UIView!
    
    let arrNumber = ["1.","2.","3.","4.","5.","6.","7.","8.","9.","10."]
//    let arrInstruction = [
//        "Follow the meal plan as close as possible.",
//        "Eat each meal about 2 to 3 hours apart.",
//        "Never skip a meal.",
//        "On your 4th day, you can eat more and or deviate from the meal plan.",
//        "Stay away from sugary drinks. Drink water!",
//        "Meal prep for at least 3 days at a time.",
//        "Depending on your goal, you may need to incorporate cardio and or resistance training.",
//        "Go to bed on time.",
//        "Don’t be too hard on yourself.",
//        "Challenge a friend to do this with you."
//    ]
    let arrInstruction = [
        "Follow the meal plan as close as possible.",
        "Eat each meal about 2 to 3 hours apart",
        "Never skip a meal",
        "Stay away from sugary drinks. Drink water!",
        "Meal prep for at least 3 days at a time.",
        "Incorporate resistance training and or Cardio depending on your goal.",
        "Get enough rest.",
        "Don’t be too hard on yourself.",
        "Challenge a friend to do this with you.",
        "Follow us on Instagram @mealid.fit for tips and motivation"
    ]
    var CurrentPage = 0 // to track registeration xib flow
    var btnGenerateMeal = false // to click generate meal action for dismiss screen
    
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    
    
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    
    //MARK:- Initialization
    func initialize() {
        btnNextStep.setTitle("Next", for: .normal)
        lblCurrentPageValue.isHidden = true
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
    
    @IBAction func btnInsta(_ sender: UIButton) {
//        kakarot
        let Username =  "mealid.fit" // Your Instagram Username here
        let appURL = URL(string: "instagram://user?username=\(Username)")!
        let application = UIApplication.shared

        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            // if Instagram app is not installed, open URL inside Safari
            let webURL = URL(string: "https://instagram.com/\(Username)")!
            application.open(webURL)
        }
    }
    
    //MARK:- button click action
    @IBAction func btnPrevClick(_ sender: UIButton) {
        btnNextStep.setTitle("Next", for: .normal)
        btnGenerateMeal = false
        
        if(CurrentPage > 0) && CurrentPage < 6 {
            CurrentPage -= 1
            let indexPath = IndexPath(item: CurrentPage, section: 0)
            clvGenerateMealSteps.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            lblCurrentPageValue.text = String(CurrentPage + 1)+"/6"
        } else {
            //            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnNextClick(_ sender: UIButton) {
        
        switch CurrentPage
        {
        case 0:
            let index = IndexPath(row: CurrentPage, section: 0)
            let cell: GenerateMealStep1CVC = self.clvGenerateMealSteps.cellForItem(at: index) as! GenerateMealStep1CVC
            
            //            if insertMealPlanDic["bodytype"] as? String == "" {
            //                setPresentAlert(withTitle: AppName, message: "please select body type")
            //                return
            //            }
            break;
            
        case 1:
            let index = IndexPath(row: CurrentPage, section: 0)
            let cell: GenerateMealStep2CVC = self.clvGenerateMealSteps.cellForItem(at: index) as! GenerateMealStep2CVC
            
            if insertMealPlanDic["workouttype"] as? String == "" {
                setPresentAlert(withTitle: AppName, message: "please select workout type")
                return
            }
            
            break;
            
        case 2:
            
            //            insertRegisterationDataDic["gender"] = Gender
            
            break;
        case 3:
            let index = IndexPath(row: CurrentPage, section: 0)
            //            let cell: GenerateMealStep3CVC = self.clvGenerateMealSteps.cellForItem(at: index) as! GenerateMealStep3CVC
            
            
            break;
        case 4:
            let index = IndexPath(row: CurrentPage, section: 0)
            //            let cell : SignUpStep7CVC = self.clvGenerateMealSteps.cellForItem(at: index) as! SignUpStep7CVC
            
            break;
        case 5 :
            let index = IndexPath(row: CurrentPage, section: 0)
            //            let cell: SignUpStep8CVC = self.clvGenerateMealSteps.cellForItem(at: index) as! SignUpStep8CVC
            
            break;
            
        case 6 :
            let index = IndexPath(row: CurrentPage, section: 0)
            //            let cell: SignUpStep9CVC = self.clvGenerateMealSteps.cellForItem(at: index) as! SignUpStep9CVC
            
            break;
            
        default:
            print("Default")
            break;
        }
        
        
        if btnGenerateMeal == true {
            if sender.currentTitle == "GENERATE MEAL PLAN" {
                
                callRegisterationAPI()
//                self.dismiss(animated: true, completion: nil)
            }
        }
        
        if(CurrentPage < 5) {
            btnGenerateMeal = false
            CurrentPage += 1
            let indexPath = IndexPath(item: CurrentPage, section: 0)
            clvGenerateMealSteps.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            lblCurrentPageValue.text = String(CurrentPage + 1)+"/6"
        }
        
        if CurrentPage == 5 {
            btnNextStep.setTitle("GENERATE MEAL PLAN", for: .normal)
            btnGenerateMeal = true
        }
    }
    
    @IBAction func btnInstructionGotIt(_ sender: UIButton) {
        vwInstruction.isHidden = true
        lblCurrentPageValue.isHidden = false
    }
    
}

//MARK:- UIcollectionView Delegate & DataSource
extension GenerateMealPlanVC: UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let nibName = UINib(nibName: id_GenerateMealStep1CVC, bundle:nil)
            clvGenerateMealSteps.register(nibName, forCellWithReuseIdentifier: id_GenerateMealStep1CVC)
            let cell = clvGenerateMealSteps.dequeueReusableCell(withReuseIdentifier: id_GenerateMealStep1CVC, for: indexPath)as! GenerateMealStep1CVC
            cell.btnSelectMacro = {
                let vc = MainInStoryboard.instantiateViewController(identifier: id_SelectMacroVC) as! SelectMacroVC
                self.present(vc, animated: true, completion: nil)
            }
            if Gender == "Female" {
                cell.btnEctomorph = {
                    if  cell.imgEctomorph.image == UIImage(named: ectomorph) {
                        cell.imgEctomorph.image = UIImage(named: ectomorphSelect)
                        
                        cell.imgMesomorph.image = UIImage(named: mesomorph) //
                        cell.imgEndomorph.image = UIImage(named: endomorph) //
                    } else {
                        //                        cell.imgEctomorph.image = UIImage(named: ectomorph)
                    }
                    insertMealPlanDic["bodytype"] = "Ectomorph"
                    insertMealPlanDic["carbs"] = 55
                    insertMealPlanDic["protein"] = 25
                    insertMealPlanDic["fat"] = 20
                }
                cell.btnMesomorph = {
                    if  cell.imgMesomorph.image == UIImage(named: mesomorph) {
                        cell.imgMesomorph.image = UIImage(named: mesomorphSelect)
                        
                        cell.imgEctomorph.image = UIImage(named: ectomorph) //
                        cell.imgEndomorph.image = UIImage(named: endomorph) //
                    } else {
                        //                        cell.imgMesomorph.image = UIImage(named: mesomorph)
                    }
                    insertMealPlanDic["bodytype"] = "Mesomorph"
                    insertMealPlanDic["carbs"] = 50
                    insertMealPlanDic["protein"] = 30
                    insertMealPlanDic["fat"] = 20
                    
                }
                cell.btnEndomorph = {
                    if  cell.imgEndomorph.image == UIImage(named: endomorph) {
                        cell.imgEndomorph.image = UIImage(named: endomorphSelect)
                        
                        cell.imgEctomorph.image = UIImage(named: ectomorph) //
                        cell.imgMesomorph.image = UIImage(named: mesomorph) //
                    } else {
                        //                        cell.imgEndomorph.image = UIImage(named: endomorph)
                    }
                    insertMealPlanDic["bodytype"] = "Endomorph"
                    insertMealPlanDic["carbs"] = 40
                    insertMealPlanDic["protein"] = 40
                    insertMealPlanDic["fat"] = 20
                }
                
            } else { // male
                
                cell.btnEctomorph = {
                    if  cell.imgEctomorph.image == UIImage(named: ectomorph_male) {
                        cell.imgEctomorph.image = UIImage(named: ectomorph_maleSelect)
                        
                        cell.imgMesomorph.image = UIImage(named: mesomorph_male) //
                        cell.imgEndomorph.image = UIImage(named: endomorph_male) //
                    } else {
                        //                        cell.imgEctomorph.image = UIImage(named: ectomorph_male)
                    }
                    insertMealPlanDic["bodytype"] = "Ectomorph"
                    insertMealPlanDic["carbs"] = 55
                    insertMealPlanDic["protein"] = 25
                    insertMealPlanDic["fat"] = 20
                }
                cell.btnMesomorph = {
                    if  cell.imgMesomorph.image == UIImage(named: mesomorph_male) {
                        cell.imgMesomorph.image = UIImage(named: mesomorph_maleSelect)
                        
                        cell.imgEctomorph.image = UIImage(named: ectomorph_male) //
                        cell.imgEndomorph.image = UIImage(named: endomorph_male) //
                    } else {
                        //                        cell.imgMesomorph.image = UIImage(named: mesomorph_male)
                    }
                    insertMealPlanDic["bodytype"] = "Mesomorph"
                    insertMealPlanDic["carbs"] = 50
                    insertMealPlanDic["protein"] = 30
                    insertMealPlanDic["fat"] = 20
                }
                cell.btnEndomorph = {
                    if  cell.imgEndomorph.image == UIImage(named: endomorph_male) {
                        cell.imgEndomorph.image = UIImage(named: endomorph_maleSelect)
                        
                        cell.imgEctomorph.image = UIImage(named: ectomorph_male) //
                        cell.imgMesomorph.image = UIImage(named: mesomorph_male) //
                    } else {
                        //                        cell.imgEndomorph.image = UIImage(named: endomorph_male)
                    }
                    insertMealPlanDic["bodytype"] = "Endomorph"
                    insertMealPlanDic["carbs"] = 40
                    insertMealPlanDic["protein"] = 40
                    insertMealPlanDic["fat"] = 20
                }
            }
            return cell
        }
        else if indexPath.row == 1 {
            let nibName = UINib(nibName: id_GenerateMealStep2CVC, bundle:nil)
            clvGenerateMealSteps.register(nibName, forCellWithReuseIdentifier: id_GenerateMealStep2CVC)
            let cell = clvGenerateMealSteps.dequeueReusableCell(withReuseIdentifier: id_GenerateMealStep2CVC, for: indexPath)as! GenerateMealStep2CVC
            return cell
        }
        else if indexPath.row == 2 {
            let nibName = UINib(nibName: id_GenerateMealStep3CVC, bundle:nil)
            clvGenerateMealSteps.register(nibName, forCellWithReuseIdentifier: id_GenerateMealStep3CVC)
            let cell = clvGenerateMealSteps.dequeueReusableCell(withReuseIdentifier: id_GenerateMealStep3CVC, for: indexPath)as! GenerateMealStep3CVC
            
            cell.delegate = self  // kakarot
            //            cell.selectedORnot == true
            return cell
        }
        else if indexPath.row == 3 {
            // how much do you weight?
            let nibName = UINib(nibName: id_SignUpStep4CVC, bundle:nil)
            clvGenerateMealSteps.register(nibName, forCellWithReuseIdentifier: id_SignUpStep4CVC)
            let cell = clvGenerateMealSteps.dequeueReusableCell(withReuseIdentifier: id_SignUpStep4CVC, for: indexPath)as! SignUpStep4CVC
            return cell
        }
        else if indexPath.row == 4 {
            let nibName = UINib(nibName: id_SignUpStep5CVC, bundle:nil)
            clvGenerateMealSteps.register(nibName, forCellWithReuseIdentifier: id_SignUpStep5CVC)
            let cell = clvGenerateMealSteps.dequeueReusableCell(withReuseIdentifier: id_SignUpStep5CVC, for: indexPath)as! SignUpStep5CVC
            
            cell.btnHowToCalcFat = {
                let vc = SignUpStoryboard.instantiateViewController(identifier: id_BodyFatCalcVC) as! BodyFatCalcVC
                self.present(vc, animated: true, completion: nil)
                
            }
            return cell
        }
        else if indexPath.row == 5 {
            let nibName = UINib(nibName: id_GenerateMealStep6CVC, bundle:nil)
            clvGenerateMealSteps.register(nibName, forCellWithReuseIdentifier: id_GenerateMealStep6CVC)
            let cell = clvGenerateMealSteps.dequeueReusableCell(withReuseIdentifier: id_GenerateMealStep6CVC, for: indexPath)as! GenerateMealStep6CVC
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: collectionView.frame.size.width, height: collectionView.layer.frame.size.height)
        return size
    }
}

extension GenerateMealPlanVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrInstruction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_instructionCell, for: indexPath) as! InstructionTVC
        cell.lblInstructionTitle.text = arrInstruction[indexPath.row]
        cell.lblNumberTitle.text = arrNumber[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension GenerateMealPlanVC {
    // MARK:- API Calling & Parsing
    
    func callRegisterationAPI() {
        
        if utils.connected() {
            
            print(insertMealPlanDic)
            let param =   [
                "body" : [
                    "bodytype" : insertMealPlanDic["bodytype"] as! String,
                    "carbs" : insertMealPlanDic["carbs"] as! Int,
                    "protein" : insertMealPlanDic["protein"] as! Int,
                    "fat" : insertMealPlanDic["fat"] as! Int,
                ],
                "workout" : [
                    "workouttype" : insertMealPlanDic["workouttype"] as! String,
                    "workpercentage" :insertMealPlanDic["workpercentage"] as! Int
                ],
                "goal" : insertMealPlanDic["goal"] as! Int,
                "level" : [
                    "leveltype" : insertMealPlanDic["leveltype"] as! String,
                    "levelpercentage" : insertMealPlanDic["levelpercentage"] as! Int
                ],
                "weight" : insertMealPlanDic["weight"] as! Int,
                "bodyfat" : insertMealPlanDic["bodyfat"] as! Int,
                "diettype" : insertMealPlanDic["diettype"] as! Int ,
                "mealpercentage" : [21,17,17,17,16,12]
                ] as [String : Any]
            
            
            let header = ["":""]
            
            let webPath = BaseURL + GenerateMealPlan
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: GenerateMealPlan, params: param , header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
            print("No Internet Connection")
        }
    }
    
    func parseGenerateMealPlan(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
                self.navigationController?.popToRootViewController(animated: true)
            } else {
                
                setPresentAlert(withTitle: AppName, message: "User meal generated successfully")
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}
// MARKS:- API Calling
extension GenerateMealPlanVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == GenerateMealPlan {
                self.parseGenerateMealPlan(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        print("Tagname: ",tagname ?? "" + "Error: " , error ?? "")
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}


// insert data into dictionary
