//
//  SelectMacroVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 13/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class MacroSelectTVC : UITableViewCell {
    @IBOutlet var lblMacroTitle: UILabel!
    @IBOutlet var vwBackground: UIView!
}

class SelectMacroVC: UIViewController {
    
    @IBOutlet var tblMacro: UITableView!
    
    let arrMacroList = ["55% carbs, 25% proteins, 20% fats"," 50% carbs, 30% proteins, 20% fats","40% carbs, 40% proteins, 20% fats","40% carbs, 30% proteins, 30% fats"," 5% carbs, 25% proteins, 70% fats \nKetogenic"]
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    
    //MARK:- Initialization
    func initialize() {
        
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
    
    //MARK:- button action
    @IBAction func btnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDone(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension SelectMacroVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMacroList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_macroCell, for: indexPath) as! MacroSelectTVC
        cell.lblMacroTitle.text = arrMacroList[indexPath.row]
        cell.selectionStyle = .none
        cell.vwBackground.backgroundColor = greyCellColour
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:MacroSelectTVC = tblMacro.cellForRow(at: indexPath) as! MacroSelectTVC
        
        insertMealPlanDic["bodytype"] = ""
        
        if indexPath.row == 0 {
            insertMealPlanDic["carbs"] = 55
            insertMealPlanDic["protein"] = 25
            insertMealPlanDic["fat"] = 20
        } else if indexPath.row == 1 {
            insertMealPlanDic["carbs"] = 50
            insertMealPlanDic["protein"] = 30
            insertMealPlanDic["fat"] = 20
        } else if indexPath.row == 2 {
            insertMealPlanDic["carbs"] = 40
            insertMealPlanDic["protein"] = 40
            insertMealPlanDic["fat"] = 20
        } else if indexPath.row == 3 {
            insertMealPlanDic["carbs"] = 40
            insertMealPlanDic["protein"] = 30
            insertMealPlanDic["fat"] = 30
        }
        else if indexPath.row == 4 {
            insertMealPlanDic["carbs"] = 5
            insertMealPlanDic["protein"] = 25
            insertMealPlanDic["fat"] = 7
        }

        
        if indexPath.row == 4 {
            if cell.vwBackground.backgroundColor == purpleColour  {
                cell.vwBackground.backgroundColor = greyCellColour
            } else {
                cell.vwBackground.backgroundColor = purpleColour
            }
        }
    }
    
}
