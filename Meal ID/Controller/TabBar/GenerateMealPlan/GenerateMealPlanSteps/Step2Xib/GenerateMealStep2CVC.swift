//
//  GenerateMealStep2CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 12/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class GenerateMealStep2CVC: UICollectionViewCell {
    
    @IBOutlet var tblWorkOut: UITableView!
    
    let arrWorkOutFreq = ["Sedentary","Lightly Active","Moderately Active","Very Active","Extremely Active"]
    let arrWorkOutReason = ["I don’t work out","I workout 1 to 3 days a week","I workout 3 to 5 days a week","I workout 6 to 7 days a week","I do intense workouts everyday"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tblWorkOut.delegate = self
        tblWorkOut.dataSource = self
        
        let nibName = UINib(nibName: id_WorkOutFreqTVC, bundle:nil)
        tblWorkOut.register(nibName, forCellReuseIdentifier: id_WorkOutFreqTVC)
    }
    
}
extension GenerateMealStep2CVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrWorkOutFreq.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_WorkOutFreqTVC, for: indexPath) as! WorkOutFreqTVC
        cell.lblWorkOutFreq.text = arrWorkOutFreq[indexPath.row]
        cell.lblWorkOutReason.text = arrWorkOutReason[indexPath.row]
        cell.selectionStyle = .none
        
        cell.btnCheckUncheckOnClick = {
            if cell.btnCheckUncheck.currentImage == UIImage(named: ic_checkbox) {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_Uncheckbox), for: .normal)
            } else {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_checkbox), for: .normal)
            }
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            insertMealPlanDic["workouttype"] = "Sedentary"
            insertMealPlanDic["workpercentage"] = 1.2
        } else if indexPath.row == 1 {
            insertMealPlanDic["workouttype"] = "Lightly Active"
            insertMealPlanDic["workpercentage"] = 1.375
        } else if indexPath.row == 2 {
            insertMealPlanDic["workouttype"] = "Moderately Active"
            insertMealPlanDic["workpercentage"] = 1.55
        } else if indexPath.row == 3 {
            insertMealPlanDic["workouttype"] = "Very Active"
            insertMealPlanDic["workpercentage"] = 1.725
        } else if indexPath.row == 4 {
            insertMealPlanDic["workouttype"] = "Extremely Active"
            insertMealPlanDic["workpercentage"] = 1.9
        }
    }
}
