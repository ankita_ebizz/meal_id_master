//
//  WorkOutFreqTVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 13/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class WorkOutFreqTVC: UITableViewCell {

    @IBOutlet var btnCheckUncheck: UIButton!
    @IBOutlet var lblWorkOutFreq: UILabel!
    @IBOutlet var lblWorkOutReason: UILabel!
    
    var btnCheckUncheckOnClick: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnCheckUncheckOnClick(_ sender: UIButton) {
        btnCheckUncheckOnClick?()
    }
    
}
