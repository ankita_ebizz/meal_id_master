//
//  SelectGoalTVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 13/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SelectGoalTVC: UITableViewCell {

    @IBOutlet var vwBackground: UIView!
    @IBOutlet var lblGoalTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
