//
//  GenerateMealStep3CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 13/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

protocol ChooseLevelProtocol {
    func ChooseLevelVCSelect()
}

class GenerateMealStep3CVC: UICollectionViewCell {
    
    @IBOutlet var tblSelectGoal: UITableView!
    
    
    let arrGoalList = ["I want to eat healthy and maintain my weight","Gain weight","Shed Body Fat"]
    
    var selectedORnot = false // to track weather option in tableview selected or not
    
    var delegate: ChooseLevelProtocol!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tblSelectGoal.delegate = self
        tblSelectGoal.dataSource = self
        
        let nibName = UINib(nibName: id_SelectGoalTVC, bundle:nil)
        tblSelectGoal.register(nibName, forCellReuseIdentifier: id_SelectGoalTVC)
        
    }
    
}
extension GenerateMealStep3CVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrGoalList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_SelectGoalTVC, for: indexPath) as! SelectGoalTVC
        cell.lblGoalTitle.text = arrGoalList[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedORnot = true
        if indexPath.row == 0 {
            insertMealPlanDic["goal"] = 1
            
        } else if indexPath.row == 1 {
            insertMealPlanDic["goal"] = 2
            
        } else if indexPath.row == 2 {
            insertMealPlanDic["goal"] = 3
             self.delegate.ChooseLevelVCSelect()
        }
    }
}
