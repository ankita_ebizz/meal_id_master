//
//  GenerateMealStep1CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 12/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class GenerateMealStep1CVC: UICollectionViewCell {
    
    @IBOutlet var imgEctomorph: UIImageView!
    @IBOutlet var imgMesomorph: UIImageView!
    @IBOutlet var imgEndomorph: UIImageView!
    
    var btnSelectMacro: (() -> Void)?
    var btnEctomorph: (() -> Void)?
    var btnMesomorph: (() -> Void)?
    var btnEndomorph: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if Gender == "Female" {
            imgEctomorph.image = UIImage(named: ectomorphSelect)
            imgMesomorph.image = UIImage(named: mesomorph)
            imgEndomorph.image = UIImage(named: endomorph)
        }
        else {
            imgEctomorph.image = UIImage(named: ectomorph_maleSelect)
            imgMesomorph.image = UIImage(named: mesomorph_male)
            imgEndomorph.image = UIImage(named: endomorph_male)
        }
    }
    
    @IBAction func btnSelectMacro(_ sender: UIButton) {
        btnSelectMacro?()
    }
    
    @IBAction func btnEctomorph(_ sender: UIButton) {
        btnEctomorph?()
        
    }
    @IBAction func btnMesomorph(_ sender: UIButton) {
        btnMesomorph?()
        
    }
    @IBAction func btnEndomorph(_ sender: UIButton) {
        btnEndomorph?()
        
    }
    
}
