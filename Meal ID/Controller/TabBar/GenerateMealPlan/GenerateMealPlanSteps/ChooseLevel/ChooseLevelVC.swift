//
//  ChooseLevelVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 14/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class ChooseLevelVC: UIViewController {
    
    @IBOutlet var tblChooseLevel: UITableView!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var lblMainTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    
    let arrLevelName = ["Conservative","Moderate","Aggressive"]
    var isSettingChooseALevel = Bool()
    var LevelDataDict = [String:Any]()
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        tblChooseLevel.register(UINib(nibName: id_ChooseLevelTVC, bundle: nil), forCellReuseIdentifier: id_ChooseLevelTVC)
    }
    
    //MARK:- Initialization
    func initialize() {
        lblMainTitle.isHidden = true
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        if isSettingChooseALevel{
            btnDone.isHidden = true
            lblMainTitle.isHidden = false
            lblSubTitle.isHidden = true
            btnClose.setImage(UIImage(named: ic_leftarrow), for: .normal)
        }
    }
    
    //MARK:- button action
    @IBAction func btnCloseOnClick(_ sender: UIButton) {
        if isSettingChooseALevel{
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnDoneOnClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension ChooseLevelVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLevelName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_ChooseLevelTVC, for: indexPath) as! ChooseLevelTVC
        cell.lblLevelTitle.text = arrLevelName[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell:ChooseLevelTVC = tblChooseLevel.cellForRow(at: indexPath) as! ChooseLevelTVC
        if cell.vwBackground.backgroundColor == yellowColour {
            cell.vwBackground.backgroundColor = bgCell
            cell.lblLevelTitle.textColor = CellLabelTextColour
        } else {
            cell.vwBackground.backgroundColor = yellowColour
            cell.lblLevelTitle.textColor = UIColor.black
        }
        
        // kakarot logic started
        if isSettingChooseALevel {
            if indexPath.row == 0 {
                LevelDataDict = ["SubTitle": "Conservative","percentage":20]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChooseALevel"), object: nil, userInfo: LevelDataDict)
            } else if indexPath.row == 1 {
                LevelDataDict = ["SubTitle": "Moderate","percentage":25]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChooseALevel"), object: nil, userInfo: LevelDataDict)
            } else if indexPath.row == 2 {
                LevelDataDict = ["SubTitle": "Aggressive","percentage":30]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChooseALevel"), object: nil, userInfo: LevelDataDict)
            }
            self.navigationController?.popViewController(animated: true)
        } else {
            if indexPath.row == 0 {
                insertMealPlanDic["leveltype"] = 1
                insertMealPlanDic["levelpercentage"] = 20
                
            } else if indexPath.row == 1 {
                insertMealPlanDic["leveltype"] = 1
                insertMealPlanDic["levelpercentage"] = 25
                
            } else if indexPath.row == 2 {
                insertMealPlanDic["leveltype"] = 1
                insertMealPlanDic["levelpercentage"] = 30
            }
        }
    }
}
