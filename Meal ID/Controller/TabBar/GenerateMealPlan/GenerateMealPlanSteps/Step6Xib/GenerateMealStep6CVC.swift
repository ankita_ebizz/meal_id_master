//
//  GenerateMealStep6CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 13/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class GenerateMealStep6CVC: UICollectionViewCell {
    
    @IBOutlet var tblDietType: UITableView!
    
    let arrDietType = ["Paleo","Mediterranean","No preference"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tblDietType.delegate = self
        tblDietType.dataSource = self
        
        let nibName = UINib(nibName: id_DietTypeTVC, bundle:nil)
        tblDietType.register(nibName, forCellReuseIdentifier: id_DietTypeTVC)
    }
    
}
extension GenerateMealStep6CVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDietType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_DietTypeTVC, for: indexPath) as! DietTypeTVC
        cell.lblDietType.text = arrDietType[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            insertMealPlanDic["diettype"] = 1
        } else if indexPath.row == 1 {
            insertMealPlanDic["diettype"] = 2
        } else if indexPath.row == 2 {
            insertMealPlanDic["diettype"] = 3 
        }
    }
}
