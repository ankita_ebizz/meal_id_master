//
//  SettingVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 11/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class SettingVC: UIViewController {
    
    @IBOutlet var tblSetting: UITableView!
 
    var notificationSwitch = Bool()
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    struct Objects {
        var sectionName : String!
        var sectionObjects : [String]!
    }
    
    var objectArray = [Objects]()
    
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        
        objectArray.append(Objects(sectionName: "PROFILE SETTINGS", sectionObjects: ["Personal details"]))
        objectArray.append(Objects(sectionName: "MEAL PLAN SETTINGS", sectionObjects: ["Body type","Activity level","Goal","Weight","Body fat","Diet type"]))
        
        objectArray.append(Objects(sectionName: "ACCOUNT SETTINGS", sectionObjects: ["Notification","Account","Subscription plan"]))
        objectArray.append(Objects(sectionName: "", sectionObjects: ["Logout"]))
    }
    
    //MARK:- button click action
    @IBAction func btnNotiAlert(_ sender: UIButton) {
        //        let vc = MainInStoryboard.instantiateViewController(identifier: "NotificationListVc") as! NotificationListVc
        //        self.navigationController?.pushViewController(vc, animated: true)
        let vc = MainInStoryboard.instantiateViewController(identifier: "UpdateWeightFatVC") as! UpdateWeightFatVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Initialization
    func initialize() {
        let nibName = UINib(nibName: id_SettingTVC, bundle:nil)
        tblSetting.register(nibName, forCellReuseIdentifier: id_SettingTVC)
        serviceManager.delegate = self
        
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
    
    // MARK:- API Calling & Parsing
    
    func callSwitchNotificationAPI(){
        
        if utils.connected(){
            let webPath = BaseURL + switchNotification
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let param =   ["notification" : false]
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: switchNotification, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parseSwitchNotification(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                utils.showToast(message: response["message"] as! String, uiView: self.view)
                notificationSwitch = response["data"] as! Bool
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
extension SettingVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return objectArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectArray[section].sectionObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_SettingTVC, for: indexPath) as! SettingTVC
        cell.lblTitle.text = objectArray[indexPath.section].sectionObjects[indexPath.row]
        cell.selectionStyle = .none
        
        // switch of notification in table
        if indexPath.section == 2 {
            if indexPath.row == 0 {
                cell.switchNoti.isHidden = false
                cell.imgRightArrw.isHidden = true
            } else {
                cell.switchNoti.isHidden = true
                cell.imgRightArrw.isHidden = false
            }
        } else {
            cell.switchNoti.isHidden = true
            cell.imgRightArrw.isHidden = false
        }
        
        cell.switchNotificationOnOff = {
            //            call api here
            self.callSwitchNotificationAPI()
        }
        if notificationSwitch == true {
            cell.switchNoti.isOn = true
        } else {
            cell.switchNoti.isOn = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return objectArray[section].sectionName
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 15, y: 8, width: 320, height: 14)
        myLabel.font = UIFont.boldSystemFont(ofSize: 14)
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        myLabel.textColor = UIColor(red: 77/255, green: 77/255, blue: 77/255, alpha: 1.0)
        let headerView = UIView()
        headerView.addSubview(myLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 { // profile setting
            
            let vc = self.storyboard?.instantiateViewController(identifier: id_PersonalDetailVC) as! PersonalDetailVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if indexPath.section == 1 { // Meal plan settings
            
            if indexPath.row == 0 { // Body type
                let vc = self.storyboard?.instantiateViewController(identifier: id_SettingBodyTypeVC) as! SettingBodyTypeVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }  else if indexPath.row == 1 { // activity level
                let vc = self.storyboard?.instantiateViewController(identifier: id_SettingActivityLevelVC) as! SettingActivityLevelVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if indexPath.row == 2 { // Goal
                let vc = self.storyboard?.instantiateViewController(identifier: id_SettingGoalVC) as! SettingGoalVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if indexPath.row == 3 { // weight loss
                let vc = self.storyboard?.instantiateViewController(identifier: id_SettingWeightLossVC) as! SettingWeightLossVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if indexPath.row == 4 { // body fat
                let vc = self.storyboard?.instantiateViewController(identifier: id_SettingBodyFatVC) as! SettingBodyFatVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else { // diet type
                let vc = self.storyboard?.instantiateViewController(identifier: id_SettingDietTypeVC) as! SettingDietTypeVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        } else if indexPath.section == 2 { // account settings
            
            if indexPath.row == 1 { // account
                let vc = self.storyboard?.instantiateViewController(identifier: id_AccountSettingVC) as! AccountSettingVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if indexPath.row == 2 { // subscription plan
                let vc = self.storyboard?.instantiateViewController(identifier: id_SubscriptionVC) as! SubscriptionVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        } else { // Logout
            user.setValue(false, forKey: K_isLogIn)  // for again login
            
            let SignInVC = SignUpStoryboard.instantiateViewController(withIdentifier: id_SignInVC) as! SignInVC
            let SignUpVC = SignUpStoryboard.instantiateViewController(withIdentifier: id_SignUpVC) as! SignUpVC
            
            let navC = UINavigationController(rootViewController: SignUpVC)
            navC.viewControllers.append(SignInVC)
            navC.navigationBar.isHidden = true
            UIApplication.shared.windows.first?.rootViewController = navC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
}

// MARKS:- API Calling
extension SettingVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == switchNotification {
                self.parseSwitchNotification(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        print("Tagname: ",tagname ?? "" + "Error: " , error ?? "")
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
