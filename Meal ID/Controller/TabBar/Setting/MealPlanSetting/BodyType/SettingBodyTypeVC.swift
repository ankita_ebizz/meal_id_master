//
//  SettingBodyTypeVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 27/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class SettingBodyTypeVC: UIViewController {
    
    @IBOutlet var imgEctomorph: UIImageView!
    @IBOutlet var imgMesomorph: UIImageView!
    @IBOutlet var imgEndomorph: UIImageView!
    
    //    let cell = GenerateMealStep1CVC()
    let serviceManager = ServiceManager()
    let utils = Utils()
    var bodyTypeVar = ""
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    
    //MARK:- button click action
    @IBAction func btnBack(_ sender: UIButton) {
        
        callBodyTypeAPI()
        //        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEctomorphOnClick(_ sender: UIButton) {
        
        if Gender == "Female" {
            if  self.imgEctomorph.image == UIImage(named: ectomorph) {
                self.imgEctomorph.image = UIImage(named: ectomorphSelect)
                
                self.imgMesomorph.image = UIImage(named: mesomorph) //
                self.imgEndomorph.image = UIImage(named: endomorph) //
            } else {
                imgEctomorph.image = UIImage(named: ectomorph)
            }
            
        } else {
            if  self.imgEctomorph.image == UIImage(named: ectomorph_male) {
                self.imgEctomorph.image = UIImage(named: ectomorph_maleSelect)
                
                self.imgMesomorph.image = UIImage(named: mesomorph_male) //
                self.imgEndomorph.image = UIImage(named: endomorph_male) //
            } else {
                self.imgEctomorph.image = UIImage(named: ectomorph_male)
            }
        }
        bodyTypeVar = "Ectomorph"
    }
    @IBAction func btnMesomorphOnClick(_ sender: UIButton) {
        if Gender == "Female" {
            if  imgMesomorph.image == UIImage(named: mesomorph) {
                imgMesomorph.image = UIImage(named: mesomorphSelect)
                
                imgEctomorph.image = UIImage(named: ectomorph) //
                imgEndomorph.image = UIImage(named: endomorph) //
            } else {
                imgMesomorph.image = UIImage(named: mesomorph)
            }
            
        } else {
            if  imgMesomorph.image == UIImage(named: mesomorph_male) {
                imgMesomorph.image = UIImage(named: mesomorph_maleSelect)
                
                imgEctomorph.image = UIImage(named: ectomorph_male) //
                imgEndomorph.image = UIImage(named: endomorph_male) //
            } else {
                imgMesomorph.image = UIImage(named: mesomorph_male)
            }
        }
        bodyTypeVar = "Mesomorph"
    }
    @IBAction func btnEndomorphOnClick(_ sender: UIButton) {
        if Gender == "Female" {
            if  imgEndomorph.image == UIImage(named: endomorph) {
                imgEndomorph.image = UIImage(named: endomorphSelect)
                
                imgEctomorph.image = UIImage(named: ectomorph) //
                imgMesomorph.image = UIImage(named: mesomorph) //
            } else {
                imgEndomorph.image = UIImage(named: endomorph)
            }
            
        } else {
            if  imgEndomorph.image == UIImage(named: endomorph_male) {
                imgEndomorph.image = UIImage(named: endomorph_maleSelect)
                
                imgEctomorph.image = UIImage(named: ectomorph_male) //
                imgMesomorph.image = UIImage(named: mesomorph_male) //
            } else {
                imgEndomorph.image = UIImage(named: endomorph_male)
            }
        }
        bodyTypeVar = "Endomorph"
    }
    
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self
        
        //        if Gender == "Female" {
        //            imgEctomorph.image = UIImage(named: ectomorphSelect)
        //            imgMesomorph.image = UIImage(named: mesomorph)
        //            imgEndomorph.image = UIImage(named: endomorph)
        //        } else {
        //            imgEctomorph.image = UIImage(named: ectomorph_maleSelect)
        //            imgMesomorph.image = UIImage(named: mesomorph_male)
        //            imgEndomorph.image = UIImage(named: endomorph_male)
        //        }
        
        //  get value from user default and store into image  //kakarot
        if utils.getBodyType() != nil {
            bodyTypeVar = "Ectomorph"
            if Gender == "Female" {
                imgEctomorph.image = UIImage(named: ectomorphSelect)
                imgMesomorph.image = UIImage(named: mesomorph)
                imgEndomorph.image = UIImage(named: endomorph)
            } else {
                imgEctomorph.image = UIImage(named: ectomorph_maleSelect)
                imgMesomorph.image = UIImage(named: mesomorph_male)
                imgEndomorph.image = UIImage(named: endomorph_male)
            }
        } else {
            bodyTypeVar = utils.getBodyType() ?? ""
            print(bodyTypeVar)
            
            if bodyTypeVar == " " {
                
            } else if bodyTypeVar == "Endomorph"{
                
            } else {
                
            }
            //            "Mesomorph"
            //            "Endomorph"
            //            "Ectomorph"
        }
    }
    
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
    
    // MARK:- API Calling & Parsing
    func callBodyTypeAPI(){
        if utils.connected(){
            
            let param = [
                "body" : [
                    "bodytype" : bodyTypeVar,
                    "carbs" : 51,
                    "protein" : 31,
                    "fat" : 21
                ]
            ]
            
            let webPath = BaseURL + UpdateBodyType
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["Authorization":utils.getUserAuth()!]
            //            self.serviceManager.callWebServiceWithGET(webpath: webPath, withTag: UpdateBodyType, header: header)
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: UpdateBodyType, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parseBodyType(response: Any) {
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                
                utils.setBodyType(data: bodyTypeVar)
                let alertController = UIAlertController(title: AppName, message: "Body type updated successfully", preferredStyle: .alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                    self.navigationController?.popViewController(animated: true)
                }
                let Cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                    self.navigationController?.popViewController(animated: true)
                }
                
                alertController.addAction(OKAction)
                alertController.addAction(Cancel)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}

// MARKS:- API Calling
extension SettingBodyTypeVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == UpdateBodyType {
                self.parseBodyType(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

