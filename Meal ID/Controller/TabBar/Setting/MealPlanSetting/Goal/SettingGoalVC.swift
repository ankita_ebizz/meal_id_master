//
//  SettingGoalVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 27/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class SettingGoalVC: UIViewController {
    
    @IBOutlet var tblGoal: UITableView!
    
    var arrGoalList = ["I want to eat healthy and maintain my weight","Gain weight","Shed Body Fat"]
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    var goalId = Int()
    var SubTitle = String()
    var percentage = Float()
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        tblGoal.register(UINib(nibName: id_SelectGoalTVC, bundle: nil), forCellReuseIdentifier: id_SelectGoalTVC)
    }
    
    // handle notification
    @objc func showChooseALevelData(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            
            if let NotiSubTitle = dict["SubTitle"] as? String{
                SubTitle = NotiSubTitle
                arrGoalList[2] = "Shed Body Fat(\(NotiSubTitle))"
                tblGoal.reloadData()
            }
            if let NotiPercentage = dict["percentage"] as? Float{
                percentage = NotiPercentage
            }
            callPhysicalActivityAPI(goalId: goalId, SubTitle: SubTitle, percentage: percentage)
        }
    }
    
    //MARK:- button click action
    @IBAction func btnBack(_ sender: UIButton) {
        if goalId != 0 {
            if goalId != 1 && goalId != 2 {
                NotificationCenter.default.addObserver(self, selector: #selector(self.showChooseALevelData), name: NSNotification.Name(rawValue: "ChooseALevel"), object: nil)
                
            } else {
                callPhysicalActivityAPI(goalId: goalId, SubTitle: SubTitle, percentage: percentage)
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
    
    // MARK:- API Calling & Parsing
    func callPhysicalActivityAPI(goalId : Int, SubTitle : String, percentage : Float){
        
        if utils.connected(){
            
            let param = [
                "goal_id" : goalId,
                "goal_sub_title":SubTitle,
                "goal_sub_percent":percentage
                ] as [String : Any]
            
            print(param)
            let webPath = BaseURL + UpdateGoal
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["Authorization":utils.getUserAuth()!]
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: UpdateGoal, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parsePhysicalActivity(response: Any) {
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                setPresentAlert(withTitle: AppName, message: "Goal updated successfully")
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    
}

// MARKS:- API Calling
extension SettingGoalVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == UpdateGoal {
                self.parsePhysicalActivity(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}



//SelectGoalTVC
extension SettingGoalVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrGoalList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_SelectGoalTVC, for: indexPath) as! SelectGoalTVC
        cell.lblGoalTitle.text = arrGoalList[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            goalId = 1
            percentage = 0
            SubTitle = ""
        } else if indexPath.row == 1 {
            goalId = 2
            percentage = 0
            SubTitle = ""
        }
        
        if indexPath.row == 2 {
            goalId = 3
            let vc = MainInStoryboard.instantiateViewController(identifier: id_ChooseLevelVC) as! ChooseLevelVC
            vc.isSettingChooseALevel = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let cell:SelectGoalTVC = tblGoal.cellForRow(at: indexPath) as! SelectGoalTVC
        if cell.vwBackground.backgroundColor == yellowColour  {
            cell.vwBackground.backgroundColor = bgCell
            cell.lblGoalTitle.textColor = CellLabelTextColour
        } else {
            cell.vwBackground.backgroundColor = yellowColour
            cell.lblGoalTitle.textColor = UIColor.black
        }
    }
}
