//
//  SettingBodyFatVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 18/03/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SettingBodyFatVC: UIViewController {
    
    @IBOutlet var PickerFatParentVw: UIView!
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    
    //MARK:- button click action
    @IBAction func btnBack(_ sender: UIButton) {
                let alertController = UIAlertController(title: AppName, message: "Body fat updated successfully", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            self.navigationController?.popViewController(animated: true)
        }
        let Cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        
        alertController.addAction(OKAction)
        alertController.addAction(Cancel)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func btnHowToCalcFat(_ sender: UIButton) {
        let vc = SignUpStoryboard.instantiateViewController(identifier: id_BodyFatCalcVC) as! BodyFatCalcVC
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Initialization
    func initialize() {
        wieghtAndFatPicker = "FatpickerVal"
        DispatchQueue.main.async {
            let myView = Bundle.main.loadNibNamed(id_CustomeWeightPicker, owner: self, options: nil)![0] as! UIView
            myView.frame = CGRect(x: 0, y: 0, width: self.PickerFatParentVw.frame.width , height: 180)
            self.PickerFatParentVw .addSubview(myView)
        }
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
    
}

