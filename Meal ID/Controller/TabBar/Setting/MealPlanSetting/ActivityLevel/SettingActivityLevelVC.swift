//
//  SettingActivityLevelVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 27/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class SettingActivityLevelVC: UIViewController {
    
    @IBOutlet var tblWorkOutFreq: UITableView!
    
    let arrWorkOutFreq = ["Sedentary","Lightly Active","Moderately Active","Very Active","Extremely Active"]
    let arrWorkOutReason = ["I don’t work out","I workout 1 to 3 days a week","I workout 3 to 5 days a week","I workout 6 to 7 days a week","I do intense workouts everyday"]
    
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    var workOutType = String()
    var percentage = Double()
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        //  WorkOutFreqTVC
        tblWorkOutFreq.register(UINib(nibName: id_WorkOutFreqTVC, bundle: nil), forCellReuseIdentifier: id_WorkOutFreqTVC)
    }
    
    //MARK:- button click action
    @IBAction func btnBack(_ sender: UIButton) {
        print(workOutType)
        if workOutType != "" {
            callPhysicalActivityAPI(workOutType: workOutType, percentage: percentage)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:- Initialization
    func initialize(){
        serviceManager.delegate = self
        
    }
    //MARK:- Setup Layout
    func setUpLayout(){
        
    }
    
    // MARK:- API Calling & Parsing
    func callPhysicalActivityAPI(workOutType : String, percentage : Double){
        
        if utils.connected(){
            
            let param = [
                "workouttype" : workOutType,
                "workpercentage" : percentage
                ] as [String : Any]
            
            let webPath = BaseURL + UpdatePhysicalActivity
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["Authorization":utils.getUserAuth()!]
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: UpdatePhysicalActivity, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parsePhysicalActivity(response: Any) {
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                setPresentAlert(withTitle: AppName, message: "Physical activity updated successfully")
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}
// MARKS:- API Calling
extension SettingActivityLevelVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == UpdatePhysicalActivity {
                self.parsePhysicalActivity(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}


extension SettingActivityLevelVC : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrWorkOutFreq.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_WorkOutFreqTVC, for: indexPath) as! WorkOutFreqTVC
        cell.lblWorkOutFreq.text = arrWorkOutFreq[indexPath.row]
        cell.lblWorkOutReason.text = arrWorkOutReason[indexPath.row]
        cell.selectionStyle = .none
        
        //        cell.btnCheckUncheckOnClick = {
        //            if cell.btnCheckUncheck.currentImage == UIImage(named: ic_checkbox) {
        //                cell.btnCheckUncheck.setImage(UIImage(named: ic_Uncheckbox), for: .normal)
        //            } else {
        //                cell.btnCheckUncheck.setImage(UIImage(named: ic_checkbox), for: .normal)
        //            }
        //        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell:WorkOutFreqTVC = tblWorkOutFreq.cellForRow(at: indexPath) as! WorkOutFreqTVC
        
        
        if indexPath.row == 0 {
            workOutType = "Sedentary"
            percentage = 1.2
            
            if cell.btnCheckUncheck.currentImage == UIImage(named: ic_checkbox) {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_Uncheckbox), for: .normal)
            } else {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_checkbox), for: .normal)
            }
            
        } else if indexPath.row == 1 {
            workOutType = "Lightly Active"
            percentage = 1.375
            
            if cell.btnCheckUncheck.currentImage == UIImage(named: ic_checkbox) {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_Uncheckbox), for: .normal)
            } else {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_checkbox), for: .normal)
            }
            
        } else if indexPath.row == 2 {
            workOutType = "Moderately Active"
            percentage = 1.55
            
            if cell.btnCheckUncheck.currentImage == UIImage(named: ic_checkbox) {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_Uncheckbox), for: .normal)
            } else {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_checkbox), for: .normal)
            }
            
        } else if indexPath.row == 3 {
            workOutType = "Very Active"
            percentage = 1.725
            
            if cell.btnCheckUncheck.currentImage == UIImage(named: ic_checkbox) {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_Uncheckbox), for: .normal)
            } else {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_checkbox), for: .normal)
            }
            
        } else {
            workOutType = "Extremely Active"
            percentage = 1.9
            
            if cell.btnCheckUncheck.currentImage == UIImage(named: ic_checkbox) {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_Uncheckbox), for: .normal)
            } else {
                cell.btnCheckUncheck.setImage(UIImage(named: ic_checkbox), for: .normal)
            }
        }
    }
}



// kakarot
// checkmark should only be once at time
// api calling after single check mark and cell click
