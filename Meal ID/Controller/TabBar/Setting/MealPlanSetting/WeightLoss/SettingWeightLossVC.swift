//
//  SettingWeightLossVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 27/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SettingWeightLossVC: UIViewController {
    
    @IBOutlet var SegmentButtonParentVw: UIView!
    @IBOutlet var PickerHeightParentVw: UIView!
 
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        wieghtAndFatPicker = "WeightpickerVal"
        initialize()
        setUpLayout()
    }
 
    //MARK:- button click action
    @IBAction func btnBack(_ sender: UIButton) {
        let alertController = UIAlertController(title: AppName, message: "Body weight updated successfully", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            self.navigationController?.popViewController(animated: true)
        }
        let Cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        
        alertController.addAction(OKAction)
        alertController.addAction(Cancel)
        self.present(alertController, animated: true, completion: nil)
        
        
//        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Initialization
    func initialize() {
        
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
        DispatchQueue.main.async {
            
            let myView = Bundle.main.loadNibNamed(id_CustomSegmentButton, owner: self, options: nil)![0] as! UIView
            myView.frame = CGRect(x: 40.0, y: 5, width: self.SegmentButtonParentVw.frame.width - 80, height: 50)
            self.SegmentButtonParentVw .addSubview(myView)
            
            let myPickerView = Bundle.main.loadNibNamed(id_CustomeWeightPicker, owner: self, options: nil)![0] as! UIView
            myPickerView.frame = CGRect(x: 0.0, y: 0, width: self.PickerHeightParentVw.frame.width, height: 220)
            self.PickerHeightParentVw .addSubview(myPickerView)
            
        }
    }
}


//UpdateWeight
