//
//  SettingDietTypeVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 27/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class SettingDietTypeVC: UIViewController {
    
    @IBOutlet var tblDietType: UITableView!
    
    let arrDietType = ["Paleo","Mediterranean","No preference"]
    var dietTypeNum = Int()
    let serviceManager = ServiceManager()
    let utils = Utils()
 
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        
        let nibName = UINib(nibName: id_DietTypeTVC, bundle:nil)
        tblDietType.register(nibName, forCellReuseIdentifier: id_DietTypeTVC)
    }
    
    //MARK:- button click action
    @IBAction func btnBack(_ sender: UIButton){
        if dietTypeNum != 0 {
            callUpdateDietTypeAPI(dietTypeNum: dietTypeNum)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    //MARK:- Initialization
    func initialize(){
        serviceManager.delegate = self
        dietTypeNum = 0
    }
    //MARK:- Setup Layout
    func setUpLayout(){
        
    }
    
    
    // MARK:- API Calling & Parsing
    func callUpdateDietTypeAPI(dietTypeNum : Int){
        
        if utils.connected(){
            let param = [
                "diettype" : dietTypeNum
            ]
            
            print(param)
            let webPath = BaseURL + UpdateDietType
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["Authorization":utils.getUserAuth()!]
            
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: UpdateDietType, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parseUpdateDietType(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                let alertController = UIAlertController(title: AppName, message: "Diet type updated successfully", preferredStyle: .alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                    self.navigationController?.popViewController(animated: true)
                }
                let Cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                    self.navigationController?.popViewController(animated: true)
                }
                
                alertController.addAction(OKAction)
                alertController.addAction(Cancel)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}
// MARKS:- API Calling
extension SettingDietTypeVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == UpdateDietType {
                self.parseUpdateDietType(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        print("Tagname: ",tagname ?? "" + "Error: " , error ?? "")
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}


extension SettingDietTypeVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDietType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_DietTypeTVC, for: indexPath) as! DietTypeTVC
        cell.lblDietType.text = arrDietType[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dietTypeNum = indexPath.row + 1
        let cell:DietTypeTVC = tblDietType.cellForRow(at: indexPath) as! DietTypeTVC
        if cell.vwBackground.backgroundColor == yellowColour  {
            cell.vwBackground.backgroundColor = bgCell
            cell.lblDietType.textColor = CellLabelTextColour
        } else {
            cell.vwBackground.backgroundColor = yellowColour
            cell.lblDietType.textColor = UIColor.black
        }
    }
}
