//
//  AccountSettingVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 12/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class AccountSettingTVC : UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblTitleInfo: UILabel!
}

//AccountSettingCell
class AccountSettingVC: UIViewController {
    
    @IBOutlet var tblAccountSetting: UITableView!
    @IBOutlet var vwChangePass: UIView!
    
    @IBOutlet var tfCurrentPass: UITextField!
    @IBOutlet var tfNewPass: UITextField!
    @IBOutlet var tfConfirmPass: UITextField!
    
    
    var arrMenuTitle = ["Email","Password","Delete account"]
    //    var arrPersonalDetals = ["John.cena@gmail.com","******",""]
    var arrPersonalDetals = [String]()
    //    var dicResponseData = NSMutableDictionary()
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    //MARK:- button action
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHIdePassPopUp(_ sender: UIButton) {
        vwChangePass.isHidden = true
    }
    
    @IBAction func btnChangePassSave(_ sender: UIButton) {
        //        vwChangePass.isHidden = true
        if tfCurrentPass.text == "" {
            setPresentAlert(withTitle: AppName, message: "Please enter current password")
            return
        } else if tfNewPass.text == "" {
            setPresentAlert(withTitle: AppName, message: "Please enter new password")
            return
        }  else if tfConfirmPass.text == "" {
            setPresentAlert(withTitle: AppName, message: "Please enter confirm password")
            return
        } else if tfNewPass.text != tfConfirmPass.text {
            setPresentAlert(withTitle: AppName, message: "New password and confirm password should be same.")
            clearPassField()
            
            return
        } else if tfCurrentPass.text == tfNewPass.text {
            setPresentAlert(withTitle: AppName, message: "New password should be different from current password.")
            clearPassField()
            
            return
        } else if (isValidPassword(tfNewPass.text!) != true) {
            setPresentAlert(withTitle: AppName, message: "Please type minimum of 6 characters with at least 1 uppercase,1 lowercase in password field")
            clearPassField()
            return
        } else {
            callChangePassAPI()
        }
    }
    func clearPassField(){
        tfNewPass.text = ""
        tfCurrentPass.text = ""
        tfConfirmPass.text = ""
    }
    @IBAction func btnChangePassCancel(_ sender: UIButton) {
        vwChangePass.isHidden = true
    }
    
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self
        
        let dicResponseData = utils.getUserData()
        arrPersonalDetals.append(dicResponseData!["email"] as! String)
        arrPersonalDetals.append("******")
        arrPersonalDetals.append(" ")
        
        tblAccountSetting.reloadData()
    }
    
    //MARK:- Setup Layout
    func setUpLayout() {
        vwChangePass.isHidden = true
    }
    
}
extension AccountSettingVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_AccountSettingCell, for: indexPath) as! AccountSettingTVC
        cell.lblTitle.text = arrMenuTitle[indexPath.row]
        cell.lblTitleInfo.text = arrPersonalDetals[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            vwChangePass.isHidden = false
            
        } else if indexPath.row == 2 { // delete Account
            
            let alertController = UIAlertController(title: AppName, message: "Are you sure you want to delete your account?\nNote: All records will be permanently deleted.", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                self.callDeleteAccountAPI()
            }
            let Cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(OKAction)
            alertController.addAction(Cancel)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK:- API Calling & Parsing
    func callDeleteAccountAPI(){
        
        if utils.connected(){
            let webPath = BaseURL + DeleteAccount
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["Authorization":utils.getUserAuth()!]
            self.serviceManager.callWebServiceWithGET(webpath: webPath, withTag: DeleteAccount, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parseDeleteAccount(response: Any) {
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                setPresentAlert(withTitle: AppName, message: "Account deleted successfully")
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func callChangePassAPI(){
        
        if utils.connected(){
            
            let param = [
                "currentpassword" : tfCurrentPass.text!,
                "newpassword" : tfNewPass.text!,
                "confirmpassword" : tfConfirmPass.text!
            ]
            
            let webPath = BaseURL + ChangePassword
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["Authorization":utils.getUserAuth()!]
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: ChangePassword, params: param, header: header)
            
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    func parseChangePass(response: Any) {
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
                clearTextField()
            } else {
                vwChangePass.isHidden = true
                setPresentAlert(withTitle: AppName, message: "Password changed successfully")
                clearTextField()
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    func clearTextField(){
        tfConfirmPass.text = ""
        tfNewPass.text = ""
        tfCurrentPass.text = ""
    }
}

// MARKS:- API Calling
extension AccountSettingVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == DeleteAccount {
                self.parseDeleteAccount(response: response)
            } else if tagname == ChangePassword {
                self.parseChangePass(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

