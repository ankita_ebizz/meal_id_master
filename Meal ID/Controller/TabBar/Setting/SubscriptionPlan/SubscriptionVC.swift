//
//  SubscriptionVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 25/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SubscriptionVC: UIViewController {
    
    @IBOutlet var viewCurvedBottom: UIView!
    @IBOutlet var tblPremiumFeaturesInfo: UITableView!
    @IBOutlet var btnLetsStart: UIButton!
    
    let arrPremiumFeaturesInfo = ["Generate a meal plan","Paleo","Mediterranean","Ketogenic","Customize your meal plan","Caloric Cycling","Body composition tracking","Track before and after fitness photos"]
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        let nibName = UINib(nibName: id_SubscriptionOfferDescTVC, bundle:nil)
        tblPremiumFeaturesInfo.register(nibName, forCellReuseIdentifier: id_SubscriptionOfferDescTVC)
    }
    
    override func viewDidLayoutSubviews() {
        viewCurvedBottom.vwBottomCurved()
        
    }
    
    //MARK:- button action
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLetsStartOnClick(_ sender: UIButton) {
        
    }
    
    @IBAction func btnSeeAllPlans(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: id_SubscriptionCatVC) as! SubscriptionCatVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Initialization
    func initialize() {
        tblPremiumFeaturesInfo.estimatedRowHeight = 70
        tblPremiumFeaturesInfo.rowHeight = UITableView.automaticDimension
    }
    
    //MARK:- Setup Layout
    func setUpLayout() {
        btnLetsStart.layer.borderWidth = 1
        btnLetsStart.layer.borderColor = lightGrey.cgColor
        btnLetsStart.layer.cornerRadius = btnLetsStart.layer.frame.height/2
    }
    
}

extension SubscriptionVC : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPremiumFeaturesInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id_SubscriptionOfferDescTVC, for: indexPath) as! SubscriptionOfferDescTVC
        
        if indexPath.row == 0 {
            cell.lblFree.isHidden = false
            cell.lblPremium.isHidden = false
            cell.btnFreeOffer.setImage(UIImage(named: ic_right_yellow), for: .normal)
            //            ic_lock
            //            ic_right_yellow
        } else {
            cell.lblFree.isHidden = true
            cell.lblPremium.isHidden = true
            cell.btnFreeOffer.setImage(UIImage(named: ic_lock), for: .normal)
            
        }
        cell.lblTitle.text = arrPremiumFeaturesInfo[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 80.0
        }
        return UITableView.automaticDimension
    }
    
}
