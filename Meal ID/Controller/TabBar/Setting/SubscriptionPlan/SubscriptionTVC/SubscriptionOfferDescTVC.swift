//
//  SubscriptionOfferDescTVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 25/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SubscriptionOfferDescTVC: UITableViewCell {
    
    @IBOutlet var lblFree: UILabel!
    @IBOutlet var lblPremium: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnFreeOffer: UIButton!
    @IBOutlet var btnPremiumOffer: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
