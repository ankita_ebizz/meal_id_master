//
//  SubscriptionCategoryCell1.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 26/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SubscriptionCategoryCell1: UITableViewCell {
    
    @IBOutlet var vwBgDescription: UIView!
    @IBOutlet var lblTimePeriod: UILabel!
    @IBOutlet var lblSubscriptionAmount: UILabel!
    @IBOutlet var lblTotalAmount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
