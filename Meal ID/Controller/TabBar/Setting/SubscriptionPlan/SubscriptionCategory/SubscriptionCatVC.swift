//
//  SubscriptionCatVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 25/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class SubscriptionCatVC: UIViewController {
    
    @IBOutlet var tblSubcriptionData: UITableView!
    @IBOutlet var btnStartPlan: CutomButtonUi!
    
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    var arrTableDataSec1 = [
        "TimePeriod" : ["12 MONTHS"],
        "SubscriptionAmount" : ["Subscription $47.99"],
        "TotalAmount" :["$4"]
    ]
    var arrTableDataSec2 = [
        "TimePeriod" : ["6 MONTHS","MONTHLY"],
        "SubscriptionAmount" : ["Subscription $29.99","Subscription $8.99"],
        "TotalAmount" :["$5","$9"]
    ]
    
    var arrPlanDetails = NSMutableArray()
    var arrPrePlanDetails = NSMutableArray()

    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        
        let nibName = UINib(nibName: id_SubscriptionCategoryCell1, bundle:nil)
        tblSubcriptionData.register(nibName, forCellReuseIdentifier: id_SubscriptionCategoryCell1)
        
        let nibName2 = UINib(nibName: id_SubscriptionCategoryCell2, bundle:nil)
        tblSubcriptionData.register(nibName2, forCellReuseIdentifier: id_SubscriptionCategoryCell2)
    }
    
    //MARK:- Button action
    @IBAction func btnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnTermsAndCondi(_ sender: UIButton) {
        
    }
    
    @IBAction func btnStartPlanOnClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self
        callListplanSubscriptionAPI()
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
    
    // MARK:- API Calling & Parsing
    func callListplanSubscriptionAPI(){
        if utils.connected(){
            let webPath = BaseURL + ListplanSubscription
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            
            self.serviceManager.callWebServiceWithGET(webpath: webPath, withTag: ListplanSubscription, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    func parseGetUserMealPlan(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                let data = (response["data"] as! NSArray)
                for i in 0..<data.count {
                    let dicData = data[i] as? NSDictionary
                    if dicData!["isBestValue"] as! Bool == true {
                        arrPrePlanDetails.add(dicData!)
                    } else {
                        arrPlanDetails.add(dicData!)
                    }
                }
                tblSubcriptionData.reloadData()
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}
// MARK:- API Calling
extension SubscriptionCatVC : ServiceManagerDelegate  {
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == ListplanSubscription {
                self.parseGetUserMealPlan(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

// MARK:- tableview
extension SubscriptionCatVC: UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrPrePlanDetails.count
        } else {
            return arrPlanDetails.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: id_SubscriptionCategoryCell1, for: indexPath) as! SubscriptionCategoryCell1
            cell.selectionStyle = .none
            
            if arrPlanDetails.count != 0 {
                let finalData = arrPrePlanDetails[indexPath.row] as? NSDictionary
                
                cell.lblTimePeriod.text = (finalData?["plantitle"] as! String).capitalized
                let subAmount = String(finalData?["subscription"] as! Double)
                cell.lblSubscriptionAmount.text = "Subscription $" + subAmount
                cell.lblTotalAmount.text = "$ " + String(finalData?["planprice"] as! Double)
            }
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: id_SubscriptionCategoryCell2, for: indexPath) as! SubscriptionCategoryCell2
            cell.selectionStyle = .none
            
            if arrPlanDetails.count != 0 {
                let finalData = arrPlanDetails[indexPath.row] as? NSDictionary
                
                cell.lblTimePeriod.text = (finalData?["plantitle"] as! String).capitalized
                let subAmount = String(finalData?["subscription"] as! Double)
                cell.lblSubscriptionAmount.text = "Subscription $" + subAmount
                cell.lblTotalAmount.text = "$ " + String(finalData?["planprice"] as! Double)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 110
        } else {
            return 85
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            btnStartPlan.setTitle("START 12 MONTHS PLAN" , for: .normal)
        } else {
            let cell:SubscriptionCategoryCell2 = tblSubcriptionData.cellForRow(at: indexPath) as! SubscriptionCategoryCell2
            if indexPath.row == 0 {
                btnStartPlan.setTitle("START 6 MONTHS PLAN" , for: .normal)
                cell.vwBackGround.backgroundColor = yellowColour
                cell.lblTimePeriod.textColor = UIColor.black
                cell.lblTotalAmount.textColor = UIColor.black
            } else if indexPath.row == 1{
                btnStartPlan.setTitle("START MONTHLY PLAN" , for: .normal)
                cell.vwBackGround.backgroundColor = yellowColour
                cell.lblTimePeriod.textColor = UIColor.black
                cell.lblTotalAmount.textColor = UIColor.black
            } else {
                cell.vwBackGround.backgroundColor = SegmentBGColour
                cell.lblTimePeriod.textColor = UIColor.white
                cell.lblTotalAmount.textColor = UIColor.lightGray
            }
        }
    }
}
