//
//  PersonalDetailVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 11/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD


class PersonalDetailTVC : UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblTitleInfo: UILabel!
}

class PersonalDetailVC: UIViewController {
    
    @IBOutlet var tblPersonalDetails: UITableView!
    
    var arrMenuTitle = ["Name","Date of birth","Gender","Weight","Body fat"]
    //    var arrPersonalDetails = ["John","1996-5-25","Male","40.2 kg","50%"]
    var arrPersonalDetails = [String]()
    
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    //MARK:- button action
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Initialization
    func initialize(){
        serviceManager.delegate = self
        callPersonalDetailAPI()
    }
    
    //MARK:- Setup Layout
    func setUpLayout(){
        
    }
    
    
}
extension PersonalDetailVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPersonalDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: id_PersonalDetailCell, for: indexPath) as! PersonalDetailTVC
        
        cell.lblTitle.text = arrMenuTitle[indexPath.row]
        cell.lblTitleInfo.text = arrPersonalDetails[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    // MARK:- API Calling & Parsing
    func callPersonalDetailAPI(){
        
        if utils.connected(){
            let webPath = BaseURL + PersonalDetails
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["Authorization":utils.getUserAuth()!]
            self.serviceManager.callWebServiceWithGET(webpath: webPath, withTag: PersonalDetails, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parsePersonalDetails(response: Any) {
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                arrPersonalDetails.append((response["data"] as! NSDictionary)["name"] as? String ?? "")
                arrPersonalDetails.append((response["data"] as! NSDictionary)["dob"] as? String ?? "")
                arrPersonalDetails.append((response["data"] as! NSDictionary)["gender"] as? String ?? "")
                arrPersonalDetails.append(String(describing: (response["data"] as! NSDictionary)["weight"] as? Float ?? 0.0))
                arrPersonalDetails.append(String(describing: (response["data"] as! NSDictionary)["bodyfat"] as? Double ?? 00))
                tblPersonalDetails.reloadData()
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

// MARKS:- API Calling
extension PersonalDetailVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == PersonalDetails {
                self.parsePersonalDetails(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
