//
//  SettingTVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 11/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SettingTVC: UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var switchNoti: UISwitch!
    @IBOutlet var imgRightArrw: UIImageView!
    
    var switchNotificationOnOff: (() -> Void)?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func switchNotificationOnOff(_ sender: UISwitch) {
        switchNotificationOnOff?()
    }
    
    
}
