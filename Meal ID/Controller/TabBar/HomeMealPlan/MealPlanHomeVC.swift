//
//  MealPlanHomeVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 08/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import FSCalendar
import MBProgressHUD

class MealPlanHomeVC: UIViewController {
    
    @IBOutlet var vwInitialviewWithoutRecord: UIView!
    @IBOutlet var vwWithRecords: UIView!
    
    @IBOutlet var calendar: FSCalendar!
    @IBOutlet var clvMealList: UICollectionView!
    @IBOutlet var clvWaterMeter: UICollectionView!
    @IBOutlet weak var lblMealWaterData: UILabel!
    @IBOutlet var lblMealTitle: UILabel!
    
    var arrMealPlanItem = NSArray()
    var dicSwapMeal = NSMutableDictionary()
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    var mealwaterdata: Int = 0
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        serviceManager.delegate = self
        initialize()
        setUpLayout()
        
        let nibName = UINib(nibName: id_MealListCVC, bundle:nil)
        clvMealList.register(nibName, forCellWithReuseIdentifier: id_MealListCVC)
        
        let WaterNibName = UINib(nibName: id_WaterMeterCVC, bundle:nil)
        clvWaterMeter.register(WaterNibName, forCellWithReuseIdentifier: id_WaterMeterCVC)
    }
    override func viewWillAppear(_ animated: Bool)  {
        navigationController?.interactivePopGestureRecognizer?.isEnabled =     false
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    //MARK:- button click action
    @IBAction func btnNotiAlert(_ sender: UIButton){
        //        let vc = MainInStoryboard.instantiateViewController(identifier: "NotificationListVc") as! NotificationListVc
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        // kakarot
        let vc = MainInStoryboard.instantiateViewController(identifier: "UpdateWeightFatVC") as! UpdateWeightFatVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Initialization
    func initialize() {
        lblMealWaterData.text = "\(mealwaterdata) glasses"
        calendar.delegate = self
        callGetUserMealPlanAPI(Date())
    }
    
    //MARK:- Setup Layout
    func setUpLayout() {
        calendar.scope = .week
        calendar.appearance.titleTodayColor = yellowColour
    }
    
    // MARK:- API Calling & Parsing
    func callGetUserMealPlanAPI(_ date: Date){
        if utils.connected(){
            let webPath = BaseURL + GetUserMealPlan
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization":TempAuth]
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
            dateFormatter.dateFormat = "yyyy-MM-dd" // "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            var param = [String: Any]()
            param["pickedDate"] = "\(dateFormatter.string(from: date))T00:00:00.000Z" // "2020-04-03T00:00:00.000Z"
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: GetUserMealPlan, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    func parseGetUserMealPlan(response: Any) {
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                let data = (response["data"] as? NSDictionary)
                //                data["mealwaterdata"] as!
                if data != nil {
                    arrMealPlanItem = data!["mealplan"] as! NSArray
                    mealwaterdata = data!["mealwaterdata"] as! Int
                    
                    // kakarot
                    dicSwapMeal["diettype"] = data!["diettype"] as! Int
                    dicSwapMeal["usermealId"] = data!["_id"] as! String
                    self.view.sendSubviewToBack(vwInitialviewWithoutRecord)
                } else {
                    arrMealPlanItem = []
                    mealwaterdata = 0
                    self.view.bringSubviewToFront(vwInitialviewWithoutRecord)
                }
                lblMealWaterData.text = "\(mealwaterdata) glasses"
                clvWaterMeter.reloadData()
                clvMealList.reloadData()
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

// MARK:- API Calling
extension MealPlanHomeVC : ServiceManagerDelegate {
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == GetUserMealPlan {
                self.parseGetUserMealPlan(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

extension MealPlanHomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == clvWaterMeter {
            return mealwaterdata
        } else {
            return arrMealPlanItem.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == clvWaterMeter {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id_WaterMeterCVC, for: indexPath) as! WaterMeterCVC
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id_MealListCVC, for: indexPath) as! MealListCVC
            cell.arrMeal = arrMealPlanItem[indexPath.row] as! NSArray
            cell.dicSwapMeal = dicSwapMeal // kakarot
            
            cell.ReloadActionSwap = {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.callGetUserMealPlanAPI(Date())
                cell.tblMealItem.reloadData()
                MBProgressHUD.hide(for: self.view, animated: true)
            }
            
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == clvWaterMeter {
            return CGSize(width: 50, height: 50)
        } else {
            let size = CGSize(width: collectionView.frame.size.width, height: collectionView.layer.frame.size.height)
            return size
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("chamatkar")
    }
}

// MARK:- FSCalendar Delegate
extension MealPlanHomeVC: FSCalendarDelegate
{
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        callGetUserMealPlanAPI(date)
    }
}
