//
//  MealItemSubCategopryTVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 21/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class MealItemSubCategopryTVC: UITableViewCell {

    @IBOutlet weak var imgMealItem: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblCarbs: UILabel!
    @IBOutlet weak var lblProteins: UILabel!
    @IBOutlet weak var lblFats: UILabel!
    
    var btnSwapMealOnClick: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnSwapMealOnClick(_ sender: UIButton) {
         btnSwapMealOnClick?()
    }
    
}
