//
//  MealListCVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 21/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class MealListCVC: UICollectionViewCell {
    
    @IBOutlet var tblMealItem: UITableView!
    
    var arrMeal = NSArray()
    var dicSwapMeal = NSMutableDictionary()
    let serviceManager = ServiceManager()
    let utils = Utils()
    var ReloadActionSwap: (() -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        tblMealItem.delegate = self
        tblMealItem.dataSource = self
        serviceManager.delegate = self
        
        let nibName = UINib(nibName: id_MealItemSubCategopryTVC, bundle:nil)
        tblMealItem.register(nibName, forCellReuseIdentifier: id_MealItemSubCategopryTVC)
        
        //               let myDict = (arrMeal[0] as! NSDictionary).allValues[0] as! NSDictionary
        //        print(myDict)
    }
    
    // MARK:- API Calling & Parsing
    func callSwapMealFoodAPI() {
        if utils.connected(){
            let webPath = BaseURL + SwapMealFood
            //            MBProgressHUD.showAdded(to: self.view, animated: true)
//                        let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            
            //            dicSwapMeal["diettype"] = data!["diettype"] as! Int
            //            dicSwapMeal["category"] = data!["_id"] as! String
            
//            MBProgressHUD start ad stop in cell file
//            let param = [
//                "usermealId" : dicSwapMeal["usermealId"] as! String,
//                "foodId" : dicSwapMeal["foodId"] as! String,
//                "mealno" : dicSwapMeal["mealno"] as! Int,
//                "diettype" : dicSwapMeal["diettype"] as! Int,
//                "category" : dicSwapMeal["category"] as! String,
//                "categoryvalue" : dicSwapMeal["categoryvalue"] as! NSNumber
//                ] as [String : Any]
            
//            print(param)
            let param = [
            "usermealId": "5e9536a24a2f9c23f7252f57",
            "foodId": "5e59e1b1102893ea394da153",
            "mealno": 1,
            "diettype": 1,
            "category": "cal",
            "categoryvalue": 344.0
                ] as [String : Any]
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: SwapMealFood, params: param, header: header)
        } else {
            //            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parseSwapMealFood(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                //                setPresentAlert(withTitle: "Please try again.", message: "")
                print("try again")
            } else {
                ReloadActionSwap?()
            }
        }
        //        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}

extension MealListCVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMeal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = tableView.dequeueReusableCell(withIdentifier: id_MealItemSubCategopryTVC, for: indexPath) as! MealItemSubCategopryTVC
        myCell.selectionStyle = .none
        
        let myDict = (arrMeal[indexPath.row] as! NSDictionary).allValues[0] as! NSDictionary
        print(myDict)
        dicSwapMeal["foodId"] = myDict["_id"] as! String
 
        myCell.lblCalories.text = "\(myDict["cal"] as! Double)"
        myCell.lblCarbs.text = "\(myDict["carbs"] as! Double)"
        myCell.lblFats.text = "\(myDict["fats"] as! Double)"
        myCell.lblTitle.text = (myDict["name"] as! String)
        myCell.imgMealItem.sd_setImage(with: URL(string: myDict["image_name"] as! String), completed: nil)
        
        myCell.btnSwapMealOnClick = {
            // kakarot
            let keyName = (self.arrMeal[indexPath.row] as! NSDictionary).allKeys[0] as! String
            
            if keyName == "calories" {
                self.dicSwapMeal["categoryvalue"] = myDict["cal"] as! Int
                self.dicSwapMeal["category"] = "cal"
                
            } else if keyName == "carbs"{
                self.dicSwapMeal["categoryvalue"] = myDict["carbs"] as! Int
                self.dicSwapMeal["category"] = "carbs"
                
            } else if keyName == "proteins"{
                self.dicSwapMeal["categoryvalue"] = myDict["prot"] as! Int
                self.dicSwapMeal["category"] = "prot"
                
            } else { //            fats
                self.dicSwapMeal["categoryvalue"] = myDict["fats"] as! NSNumber
                self.dicSwapMeal["category"] = "fats"
                
            }
            self.dicSwapMeal["mealno"] = indexPath.row + 1
            
            self.callSwapMealFoodAPI()
        }
        //        print(myDict)
        return myCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 83
    }
    
}

// MARK:- API Calling
extension MealListCVC : ServiceManagerDelegate {
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == SwapMealFood {
                self.parseSwapMealFood(response: response)
            }
        }
        //        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        //        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        //        self.setPresentAlert(withTitle: AppName, message: "Something went wrong")
        //
        //        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

//        let cat = myDict["category"] as! String
//        if cat == "calories" {
//            dicSwapMeal["category"] = "cal"
//        } else if cat == "carbs"{
//            dicSwapMeal["category"] = "carbs"
//        } else if cat == "proteins"{
//            dicSwapMeal["category"] = "prot"
//        } else { //            fats
//            dicSwapMeal["category"] = "fats"
//        }
