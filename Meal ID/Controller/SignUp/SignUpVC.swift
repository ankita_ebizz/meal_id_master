//
//  SIgnUpVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//
import UIKit
import FacebookLogin
import AuthenticationServices
import FBSDKLoginKit
import MBProgressHUD

class SignUpVC: UIViewController {
    
    let utils = Utils()
    let serviceManager = ServiceManager()
    var fbDict: [String : AnyObject]!
    var login_type = "0"
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
        
        //        if let accessToken = FBSDKAccessToken.current(){
        //            getFBUserData()
        //        }
    }
    
    @IBAction func btnSelectPrivacy(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "ic_yellow_Uncheckbox") {
            sender.setImage(UIImage(named: "ic_yellow_checkbox"), for: .normal)
        } else {
            sender.setImage(UIImage(named: "ic_yellow_Uncheckbox"), for: .normal)
        }
     }
    
    @IBAction func btnOpenPrivacy(_ sender: UIButton) {
        //PrivacyPolicyVC
        let vc = self.storyboard?.instantiateViewController(identifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func btnSignInOnClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: id_SignInVC) as! SignInVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnEmailOnClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: id_SignUpInfoVC) as! SignUpInfoVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnFBOnClick(_ sender: UIButton) {
        loginWithReadPermissions()
        //        let vc = self.storyboard?.instantiateViewController(identifier: id_SignUpInfoVC) as! SignUpInfoVC
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnAppleOnClick(_ sender: UIButton) {
        handleAuthorizationAppleIDButtonPress()
        //        let vc = self.storyboard?.instantiateViewController(identifier: id_SignUpInfoVC) as! SignUpInfoVC
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self
    }
    
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
    
    // MARK:- Apple Login
    func handleAuthorizationAppleIDButtonPress() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    // MARK:- Facebook Login
    func loginManagerDidComplete(_ result: LoginResult) {
        let alertController: UIAlertController
        switch result {
        case .cancelled:
            alertController = UIAlertController(
                title: "Login Cancelled",
                message: "User cancelled login.",
                preferredStyle: .alert
            )
        case .failed(let error):
            alertController = UIAlertController(
                title: "Login Fail",
                message: "Login failed with error \(error)",
                preferredStyle: .alert
            )
        case .success(let grantedPermissions, let declinedPermissions, let accessToken):
            alertController = UIAlertController(
                title: "Login Success",
                message: "Login succeeded with granted permissions: \(grantedPermissions)",
                preferredStyle: .alert
            )
            getFBUserData()
        }
        
        alertController.addAction(UIAlertAction(title: "Done", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    func getFBUserData(){
        if(AccessToken.current != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.fbDict = result as! [String : AnyObject]
                    self.login_type = "1"
                    self.callSignInAPI(self.fbDict["email"] as! String, login_type: self.login_type, social_key: self.fbDict["id"] as! String)
                }
            })
        }
    }
    
    private func loginWithReadPermissions() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let loginManager = LoginManager()
        loginManager.logIn(
            permissions: [.publicProfile, .email], //, .userFriends
            viewController: self
        ) { result in
            self.loginManagerDidComplete(result)
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    private func logOut() {
        let loginManager = LoginManager()
        loginManager.logOut()
        
        let alertController = UIAlertController(
            title: "Logout",
            message: "Logged out.",
            preferredStyle: .alert
        )
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- API Calling & Parsing
    func callSignInAPI(_ email: String, login_type: String, social_key: String){
        if utils.connected(){
            let param = [
                "email" : email,
                "login_type" : login_type,
                "social_key" : social_key,
                "device_token" : FireBaseToken,
            ]
            
            print(param)
            let webPath = BaseURL + LogIn
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let header = ["":""]
            
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: LogIn, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
            print("No Internet Connection")
        }
    }
    func parseLogIn(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                //                setPresentAlert(withTitle: "Please try again.", message: "")
                let vc = self.storyboard?.instantiateViewController(identifier: id_SignUpInfoVC) as! SignUpInfoVC
                vc.fbDict = fbDict
                vc.login_type = login_type
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                utils.setUserData(data: (response["data"] as! [String : Any]))
                utils.setUserAuth(data: (response["data"] as! NSDictionary)["authToken"] as! String )
                //                print(utils.getUserData()!)
                //                print(utils.getUserAuth()!)
                
                utils.showToast(message: "Login done successfully.", uiView: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    let vc = MainInStoryboard.instantiateViewController(withIdentifier: id_HomePage) as! HomePage
                    user.setValue(true, forKey: K_isLogIn)
                    print(user.value(forKey: K_isLogIn)!)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

extension SignUpVC: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            
            print("\n\n\n\n\(userIdentifier), \(fullName), \(email)\n\n\n\n")
            
            // For the purpose of this demo app, store the `userIdentifier` in the keychain.
            //            self.saveUserInKeychain(userIdentifier)
            
            // For the purpose of this demo app, show the Apple ID credential information in the `ResultViewController`.
            //            self.showResultViewController(userIdentifier: userIdentifier, fullName: fullName, email: email)
            
        case let passwordCredential as ASPasswordCredential:
            
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            print("\n\n\n\n\(username), \(password)\n\n\n\n")
            // For the purpose of this demo app, show the password credential as an alert.
            //            DispatchQueue.main.async {
            //                self.showPasswordCredentialAlert(username: username, password: password)
        //            }
        default:
            break
        }
    }
}

extension SignUpVC: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

extension SignUpVC: ServiceManagerDelegate
{
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == LogIn {
                self.parseLogIn(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        print("Tagname: ",tagname ?? "" + "Error: " , error ?? "")
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

