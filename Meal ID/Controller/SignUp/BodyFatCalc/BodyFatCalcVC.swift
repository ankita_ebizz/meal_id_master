//
//  BodyFatCalcVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 06/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class BodyFatCalcVC: UIViewController {
    
    @IBOutlet var SegmentButtonParentVw: UIView!
    @IBOutlet var PickerHeightParentVw: UIView!
    @IBOutlet var viewBMRvalueRouded: UIView!
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        wieghtAndFatPicker = "HeightpickerVal"
        
        initialize()
        setUpLayout()
    }
    
    // MARK:- button action
    
    @IBAction func btnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDone(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- Initialization
    func initialize(){
        
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        DispatchQueue.main.async {
            
            let myView = Bundle.main.loadNibNamed(id_CustomSegmentButton, owner: self, options: nil)![0] as! UIView
            myView.frame = CGRect(x: 40.0, y: 5, width: self.SegmentButtonParentVw.frame.width - 80, height: 50)
            self.SegmentButtonParentVw .addSubview(myView)
            
            let myPickerView = Bundle.main.loadNibNamed(id_CustomeWeightPicker, owner: self, options: nil)![0] as! UIView
            myPickerView.frame = CGRect(x: 0.0, y: 0, width: self.PickerHeightParentVw.frame.width, height: 180)
            self.PickerHeightParentVw .addSubview(myPickerView)
            
        }
        
        viewBMRvalueRouded.setviewRadius()
    }
}
