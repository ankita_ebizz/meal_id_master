//
//  SignUpStep5CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SignUpStep5CVC: UICollectionViewCell {
    
    @IBOutlet var PickerFatParentVw: UIView!
    
    var btnHowToCalcFat: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        NotificationCenter.default.post(name: Notification.Name("FatpickerVal"), object: nil)
        wieghtAndFatPicker = "FatpickerVal"
        
        DispatchQueue.main.async {
            
            let myView = Bundle.main.loadNibNamed(id_CustomeWeightPicker, owner: self, options: nil)![0] as! UIView
            myView.frame = CGRect(x: 0, y: 0, width: self.PickerFatParentVw.frame.width , height: 180)
            self.PickerFatParentVw .addSubview(myView)
        }
    }
    
    @IBAction func btnHowToCalcFat(_ sender: UIButton) {
        btnHowToCalcFat?()
    }
    
}
