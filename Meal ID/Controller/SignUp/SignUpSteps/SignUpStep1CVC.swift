//
//  SignUpStep1CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SignUpStep1CVC: UICollectionViewCell,UITextFieldDelegate {
    
    @IBOutlet var tfName: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tfName.delegate = self  
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location == 0 && string == " " { // prevent space on first character
            return false
        }
        if textField.text?.last == " " && string == " " { // allowed only single space
            return false
        }
        if string == " " { return true } // now allowing space between name
        if string.rangeOfCharacter(from: CharacterSet.letters.inverted) != nil {
            return false
        }
        return true
    }
}
