//
//  SignUpStep2CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SignUpStep2CVC: UICollectionViewCell {
    
    @IBOutlet var dptBirthdateParentVw: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            let myView = Bundle.main.loadNibNamed(id_CustomeDatePicker, owner: self, options: nil)![0] as! UIView
            myView.frame = CGRect(x: 0, y: 0, width: self.dptBirthdateParentVw.frame.width , height: 200)
            self.dptBirthdateParentVw .addSubview(myView)
            
            let datePicker = myView.subviews[0].subviews[0] as! UIDatePicker
            let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())
            datePicker.maximumDate = date
         }
    }
 }
