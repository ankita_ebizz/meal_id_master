//
//  SignUpStep3CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SignUpStep3CVC: UICollectionViewCell {
    
    @IBOutlet var imgMale: UIImageView!
    @IBOutlet var imgFemale: UIImageView!
    
    var btnMaleOnClick: (() -> Void)?
    var btnFemaleOnClick: (() -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        imgMale.image = UIImage(named: maleSelect)
    }
    
    @IBAction func btnMaleOnClick(_ sender: UIButton) {
        btnMaleOnClick?()
    }
    
    @IBAction func btnFemaleOnClick(_ sender: UIButton) {
        btnFemaleOnClick?()
    }
    
}
