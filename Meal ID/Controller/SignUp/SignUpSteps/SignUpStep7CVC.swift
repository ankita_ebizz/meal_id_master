//
//  SignUpStep7CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SignUpStep7CVC: UICollectionViewCell {
    @IBOutlet var btnEye: UIButton!
    
    @IBOutlet var tfPassword: UITextField!
    
    var btnShowPassOnClick: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnShowPassOnClick(_ sender: UIButton) {
        btnShowPassOnClick?()
    }
    
}
