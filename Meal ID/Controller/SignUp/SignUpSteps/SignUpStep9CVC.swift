//
//  SignUpStep9CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SignUpStep9CVC: UICollectionViewCell,UITextFieldDelegate {
    
    @IBOutlet var tfOTP1: UITextField!
    @IBOutlet var tfOTP2: UITextField!
    @IBOutlet var tfOTP3: UITextField!
    @IBOutlet var tfOTP4: UITextField!
    
    @IBOutlet var vwPhoneVerified: UIView!
    
    var btnResendOTP: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tfOTP1.delegate = self
        tfOTP2.delegate = self
        tfOTP3.delegate = self
        tfOTP4.delegate = self
        
        
        tfOTP1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        tfOTP2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        tfOTP3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        tfOTP4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case tfOTP1:
                tfOTP2.becomeFirstResponder()
            case tfOTP2:
                tfOTP3.becomeFirstResponder()
            case tfOTP3:
                tfOTP4.becomeFirstResponder()
            case tfOTP4:
                tfOTP4.resignFirstResponder()
             default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case tfOTP1:
                tfOTP1.text = ""
                tfOTP2.text = ""
                tfOTP3.text = ""
                tfOTP4.text = ""
                tfOTP1.becomeFirstResponder()
                vwPhoneVerified.isHidden = true
            case tfOTP2:
                tfOTP1.text = ""
                tfOTP2.text = ""
                tfOTP3.text = ""
                tfOTP4.text = ""
                tfOTP1.becomeFirstResponder()
                vwPhoneVerified.isHidden = true
            case tfOTP3:
                tfOTP1.text = ""
                tfOTP2.text = ""
                tfOTP3.text = ""
                tfOTP4.text = ""
                tfOTP1.becomeFirstResponder()
                vwPhoneVerified.isHidden = true
            case tfOTP4:
                tfOTP1.text = ""
                tfOTP2.text = ""
                tfOTP3.text = ""
                tfOTP4.text = ""
                tfOTP1.becomeFirstResponder()
                vwPhoneVerified.isHidden = true
            default:
                break
            }
        }
        else{
            
        }
    }
    
    @IBAction func btnResendOTP(_ sender: UIButton) {
        btnResendOTP?()
    }
    
}
