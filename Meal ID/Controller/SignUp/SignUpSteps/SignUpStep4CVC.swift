//
//  SignUpStep4CVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SignUpStep4CVC: UICollectionViewCell {
    
    @IBOutlet var SegmentButtonParentVw: UIView!
    @IBOutlet var PickerWeightParentVw: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        NotificationCenter.default.post(name: Notification.Name("WeightpickerVal"), object: nil)
        wieghtAndFatPicker = "WeightpickerVal"
        
        DispatchQueue.main.async {
            
            let myView = Bundle.main.loadNibNamed(id_CustomSegmentButton, owner: self, options: nil)![0] as! UIView
            myView.frame = CGRect(x: 40.0, y: 5, width: self.SegmentButtonParentVw.frame.width - 80, height: 50)
            self.SegmentButtonParentVw .addSubview(myView)
            
            let myPickerView = Bundle.main.loadNibNamed(id_CustomeWeightPicker, owner: self, options: nil)![0] as! UIView
            myPickerView.frame = CGRect(x: 0.0, y: 0, width: self.PickerWeightParentVw.frame.width, height: 180)
            self.PickerWeightParentVw .addSubview(myPickerView)
        }
        
    }
}

