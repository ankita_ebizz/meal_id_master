//
//  SignUpInfoVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD
import FlagPhoneNumber

class SignUpInfoVC: UIViewController {
    
    @IBOutlet var clvSignUpStep: UICollectionView!
    @IBOutlet var lblCurrentPageValue: UILabel!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    
    @IBOutlet var ConstbtnNextBottom: NSLayoutConstraint!
    
    var CurrentPage = 0 // to track registeration xib flow
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    var insertRegisterationDataDic = [String: Any]()
    var fbDict: [String : AnyObject]?
    var login_type = "0"
    
    var countryCode: String? = "+1"
    var txtTelephoneValid: String? = ""
    var checkMobiId: String!
    var mobileNumber: String!
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    
    //MARK:- Initialization
    func initialize() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        serviceManager.delegate = self
    }
    
    //MARK:- Setup Layout
    func setUpLayout() {
        btnSignUp.isHidden = true
        btnNext.setCircularButton()
    }
    
    //MARK:- button click action
    @IBAction func btnPrevClick(_ sender: UIButton) {
        btnSignUp.isHidden = true
        btnNext.isHidden = false
        
        if(CurrentPage > 0) && CurrentPage < 7 {
            CurrentPage -= 1
            let indexPath = IndexPath(item: CurrentPage, section: 0)
            clvSignUpStep.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            lblCurrentPageValue.text = String(CurrentPage + 1)+"/7"
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnNextClick(_ sender: UIButton) {
        
        switch CurrentPage
        {
        case 0:
            let index = IndexPath(row: CurrentPage, section: 0)
            let cell: SignUpStep1CVC = self.clvSignUpStep.cellForItem(at: index) as! SignUpStep1CVC
            //            if (cell.tfName.text?.containsWhitespace == true) {
            //                setPresentAlert(withTitle: AppName, message: "White space not allowed in Name.")
            //                return
            //            } else if(cell.tfName.text == "") {
            //                setPresentAlert(withTitle: AppName, message: "Please enter your Name.")
            //                return
            //            }
            insertRegisterationDataDic["name"] = cell.tfName.text
            break;
            
        case 1:
            //            if registerationDOB == "" {
            //                setPresentAlert(withTitle: AppName, message: "Please select your date of birth.")
            //                return
            //            }
            insertRegisterationDataDic["dob"] = registerationDOB
            insertRegisterationDataDic["age"] = DOBAge
            break;
            
        case 2:
            //            if Gender == "" {
            //                setPresentAlert(withTitle: AppName, message: "Please select your Gender.")
            //                return
            //            }
            insertRegisterationDataDic["gender"] = Gender
            break;
            
        case 3:
            let index = IndexPath(row: CurrentPage, section: 0)
            let cell: SignUpStep6CVC = self.clvSignUpStep.cellForItem(at: index) as! SignUpStep6CVC
            //            if (cell.tfEmailAddress.text?.containsWhitespace == true) {
            //                setPresentAlert(withTitle: AppName, message: "White space not allowed in Name.")
            //                return
            //            } else if(cell.tfEmailAddress.text! == "") {
            //                setPresentAlert(withTitle: AppName, message: "Please enter your Email.")
            //                return
            //            } else if(isValidEmail(testStr: cell.tfEmailAddress.text!) != true) {
            //                setPresentAlert(withTitle: AppName, message: "invalid email address")
            //                cell.tfEmailAddress.text = ""
            //                return
            //            }
            insertRegisterationDataDic["email"] = cell.tfEmailAddress.text
            break;
            
        case 4:
            let index = IndexPath(row: CurrentPage, section: 0)
            let cell : SignUpStep7CVC = self.clvSignUpStep.cellForItem(at: index) as! SignUpStep7CVC
            
            //            if(cell.tfPassword.text! == "") {
            //                setPresentAlert(withTitle: AppName, message: "Please enter your Password")
            //                return
            //            } else if (isValidPassword(cell.tfPassword.text!) != true) {
            //                setPresentAlert(withTitle: AppName, message: "Please type minimum of 6 characters with at least 1 uppercase,1 lowercase in password field")
            //                return
            //            }
            insertRegisterationDataDic["password"] = cell.tfPassword.text
            break;
            
        case 5 :
            let index = IndexPath(row: CurrentPage, section: 0)
            let cell: SignUpStep8CVC = self.clvSignUpStep.cellForItem(at: index) as! SignUpStep8CVC
            
            //            if(cell.tfMobileNum.text! == "") {
            //                setPresentAlert(withTitle: AppName, message: "Please enter your mobile num")
            //                return
            //            } else if (txtTelephoneValid == "") {
            //                setPresentAlert(withTitle: AppName, message: "Please enter a valid mobile number.")
            //                return
            //            }
            cell.tfMobileNum.set(phoneNumber: (cell.tfMobileNum.text ?? ""))
            insertRegisterationDataDic["mobilenumber"] = cell.tfMobileNum.text
            insertRegisterationDataDic["countrycode"] = countryCode
            
            mobileNumber = "+\(countryCode!)\(cell.tfMobileNum.text!)"
            
            CheckMobiService.init().request(ValidationTypeSMS, forNumber: mobileNumber, withResponse: { (status, response, error) in
                if(status == kStatusSuccessWithContent && response != nil){
                    let dictResponse = response! as NSDictionary
                    self.checkMobiId = (dictResponse.value(forKey: "id") as! String)
                    print("Sam otp = \(self.checkMobiId!)")
                }
                else {
                    if(response != nil) {
                        let dictResponse = response! as NSDictionary
                        if(dictResponse.value(forKey: "error") as? String != nil) {
                            self.setPresentAlert(withTitle: AppName, message: dictResponse.value(forKey: "error") as! String)
                        }
                    }
                    else {
                        print(error!)
                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            })
            
            break;
            
        case 6 :
            
            break;
            
        default:
            print("Default")
            break;
        }
        
        if(CurrentPage < 6) {
            CurrentPage += 1
            var indexPath = IndexPath(item: CurrentPage, section: 0)
            if(fbDict != nil)
            {
                if(CurrentPage == 4)
                {
                    CurrentPage += 1
                    indexPath = IndexPath(item: CurrentPage, section: 0)
                }
            }
            clvSignUpStep.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            lblCurrentPageValue.text = String(CurrentPage + 1)+"/7"
        }
        if CurrentPage == 6 {
            btnNext.isHidden = true
            btnSignUp.isHidden = false
        }
    }
    
    @IBAction func btnSignUpOnClick(_ sender: UIButton) {
        
        let index = IndexPath(row: CurrentPage, section: 0)
        let cell: SignUpStep9CVC = self.clvSignUpStep.cellForItem(at: index) as! SignUpStep9CVC
        
        if(cell.tfOTP1.text == "") {
            setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
            return
        } else if(cell.tfOTP2.text == "") {
            setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
            return
        } else if(cell.tfOTP3.text == "") {
            setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
            return
        } else if(cell.tfOTP4.text == "") {
            setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
            return
        }
        
        let pin = cell.tfOTP1.text! + cell.tfOTP2.text! + cell.tfOTP3.text! + cell.tfOTP4.text!
        MBProgressHUD.showAdded(to: self.view, animated: true)
        CheckMobiService.init().verifyPin(checkMobiId, withPin: pin, withResponse: { (status, response, error) in
            if(status == kStatusSuccessWithContent && response != nil){
                let dictData = response! as NSDictionary
                if(dictData["validated"] as! Int == 1) {
                    cell.vwPhoneVerified.isHidden = false
                    self.utils.showToast(message: "Number verified successfully", uiView: self.view)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                        self.callRegisterationAPI()
                    }
                    
                } else {
                    self.setPresentAlert(withTitle: AppName, message: "Invalid OTP.")
                }
            } else {
                self.setPresentAlert(withTitle: AppName, message: error as! String)
            }
            MBProgressHUD.hide(for: self.view, animated: true)
        })
        
    }
    
    //MARK:- keyboard 
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                let height = keyboardSize.height
                self.view.frame.origin.y += height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                let height = keyboardSize.height
                self.view.frame.origin.y -= height
            }
        }
    }
}

//MARK:- UITableView Delegate & DataSource
extension SignUpInfoVC: UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let nibName = UINib(nibName: id_SignUpStep1CVC, bundle:nil)
            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: id_SignUpStep1CVC)
            let cell = clvSignUpStep.dequeueReusableCell(withReuseIdentifier: id_SignUpStep1CVC, for: indexPath)as! SignUpStep1CVC
            if(fbDict != nil)
            {
                cell.tfName.text = fbDict!["name"] as? String
                insertRegisterationDataDic["name"] = fbDict!["name"] as? String
            }
            return cell
        }
        else if indexPath.row == 1 {
            let nibName = UINib(nibName: id_SignUpStep2CVC, bundle:nil)
            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: id_SignUpStep2CVC)
            let cell = clvSignUpStep.dequeueReusableCell(withReuseIdentifier: id_SignUpStep2CVC, for: indexPath)as! SignUpStep2CVC
            return cell
        }
        else if indexPath.row == 2 {
            let nibName = UINib(nibName: id_SignUpStep3CVC, bundle:nil)
            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: id_SignUpStep3CVC)
            let cell = clvSignUpStep.dequeueReusableCell(withReuseIdentifier: id_SignUpStep3CVC, for: indexPath)as! SignUpStep3CVC
            
            cell.btnMaleOnClick = {
                cell.imgMale.image = UIImage(named: ic_male)
                if  cell.imgMale.image == UIImage(named: ic_male) {
                    cell.imgMale.image = UIImage(named: maleSelect)
                    cell.imgFemale.image = UIImage(named: ic_female) //
                    Gender = "Male"
                } else {
                    cell.imgMale.image = UIImage(named: ic_male)
                    //                    Gender = "Female"
                }
            }
            cell.btnFemaleOnClick = {
                cell.imgFemale.image = UIImage(named: ic_female)
                if  cell.imgFemale.image == UIImage(named: ic_female) {
                    cell.imgFemale.image = UIImage(named: ic_femaleSelect)
                    cell.imgMale.image = UIImage(named: ic_male) //
                    Gender = "Female"
                } else {
                    cell.imgFemale.image = UIImage(named: ic_female)
                }
            }
            return cell
        }
        else if indexPath.row == 3 {
            let nibName = UINib(nibName: id_SignUpStep6CVC, bundle:nil)
            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: id_SignUpStep6CVC)
            let cell = clvSignUpStep.dequeueReusableCell(withReuseIdentifier: id_SignUpStep6CVC, for: indexPath)as! SignUpStep6CVC
            if(fbDict != nil)
            {
                cell.tfEmailAddress.isEnabled = false
                cell.tfEmailAddress.text = fbDict!["email"] as? String
                insertRegisterationDataDic["email"] = fbDict!["email"] as? String
            }
            return cell
        }
        else if indexPath.row == 4 {
            let nibName = UINib(nibName: id_SignUpStep7CVC, bundle:nil)
            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: id_SignUpStep7CVC)
            let cell = clvSignUpStep.dequeueReusableCell(withReuseIdentifier: id_SignUpStep7CVC, for: indexPath)as! SignUpStep7CVC
            cell.btnShowPassOnClick = {
                if cell.btnEye.tag == 0 {
                    cell.tfPassword.isSecureTextEntry = false
                    cell.btnEye.setImage(UIImage(named: "ic_eyeClose"), for: UIControl.State.normal)
                    cell.btnEye.tag += 1
                } else {
                    cell.tfPassword.isSecureTextEntry = true
                    cell.btnEye.setImage(UIImage(named: "ic_eye"), for: UIControl.State.normal)
                    cell.btnEye.tag -= 1
                }
            }
            return cell
        }
        else if indexPath.row == 5 {
            let nibName = UINib(nibName: id_SignUpStep8CVC, bundle:nil)
            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: id_SignUpStep8CVC)
            let cell = clvSignUpStep.dequeueReusableCell(withReuseIdentifier: id_SignUpStep8CVC, for: indexPath)as! SignUpStep8CVC
            
            cell.tfMobileNum.font = UIFont(name: "ARSMaquettePro-Regular", size: 16.0)
            cell.tfMobileNum.hasPhoneNumberExample = false
            cell.tfMobileNum.setFlag(countryCode: FPNCountryCode.US)
            //            cell.tfMobileNum.countryRepository
            cell.tfMobileNum.placeholder = "Mobile number"
            cell.tfMobileNum.delegate = self
            cell.tfMobileNum.setFlag(key: FPNOBJCCountryKey.US)
            
            return cell
        }
        else if indexPath.row == 6 {
            let nibName = UINib(nibName: id_SignUpStep9CVC, bundle:nil)
            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: id_SignUpStep9CVC)
            let cell = clvSignUpStep.dequeueReusableCell(withReuseIdentifier: id_SignUpStep9CVC, for: indexPath)as! SignUpStep9CVC
            
            //            cell.vwPhoneVerified.isHidden = false
            cell.btnResendOTP = {
                cell.tfOTP1.text = ""
                cell.tfOTP2.text = ""
                cell.tfOTP3.text = ""
                cell.tfOTP4.text = ""
                cell.vwPhoneVerified.isHidden = true
                
                MBProgressHUD.showAdded(to: self.view, animated: true)
                CheckMobiService.init().request(ValidationTypeSMS, forNumber: self.mobileNumber, withResponse: { (status, response, error) in
                    if(status == kStatusSuccessWithContent && response != nil){
                        let dictResponse = response! as NSDictionary
                        self.checkMobiId = (dictResponse.value(forKey: "id") as! String)
                        print("Sam otp = \(self.checkMobiId!)")
                    }
                    else {
                        if(response != nil) {
                            let dictResponse = response! as NSDictionary
                            if(dictResponse.value(forKey: "error") as? String != nil) {
                                self.setPresentAlert(withTitle: AppName, message: dictResponse.value(forKey: "error") as! String)
                            }
                        }
                        else {
                            print(error!)
                        }
                    }
                })
                MBProgressHUD.hide(for: self.view, animated: true)
                
            }
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: collectionView.frame.size.width, height: collectionView.layer.frame.size.height)
        return size
    }
    
    // MARK:- API Calling & Parsing
    
    func callRegisterationAPI() {
        if utils.connected() {
            print(insertRegisterationDataDic)
            
            var param = [
                "name": insertRegisterationDataDic["name"] as! String,
                "dob": insertRegisterationDataDic["dob"] as! String,
                "email": insertRegisterationDataDic["email"] as! String,
                "gender": insertRegisterationDataDic["gender"] as! String,
                "age":  insertRegisterationDataDic["age"] as! Int,
                "countrycode": insertRegisterationDataDic["countrycode"] as! String,
                "mobilenumber" : insertRegisterationDataDic["mobilenumber"] as! String,
                "device_token" : FireBaseToken,
                "login_type" : 0,
                "password": insertRegisterationDataDic["password"] as? String ?? "",
                "social_key" : ""
                ] as [String : Any]
            
            if fbDict != nil
            {
                param["password"] = ""
                param["login_type"] = login_type
                param["social_key"] = fbDict?["id"] as? String ?? ""
            }
            let header = ["":""]
            
            let webPath = BaseURL + Registeration
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: Registeration, params: param , header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
            print("No Internet Connection")
        }
    }
    
    func parseRegisteration(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
                self.navigationController?.popToRootViewController(animated: true)
            } else {
                
                user.setValue(true, forKey: K_isLogIn)
                let alertController = UIAlertController(title: AppName, message: "Registeration done successfully.", preferredStyle: .alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                    let vc = MainInStoryboard.instantiateViewController(withIdentifier: id_HomePage) as! HomePage
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                let Cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                    let vc = MainInStoryboard.instantiateViewController(withIdentifier: id_HomePage) as! HomePage
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                alertController.addAction(OKAction)
                alertController.addAction(Cancel)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}

// MARKS:- next button hieght manage
extension SignUpInfoVC : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

// MARKS:- API Calling
extension SignUpInfoVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == Registeration {
                self.parseRegisteration(response: response)
            }
        }
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        print("Tagname: ",tagname ?? "" + "Error: " , error ?? "")
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

extension SignUpInfoVC: FPNTextFieldDelegate {
    func fpnDisplayCountryList() {
        //        let navigationViewController = UINavigationController(rootViewController: listController)
        //
        //        present(navigationViewController, animated: true, completion: nil)
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
        countryCode = dialCode;
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            self.txtTelephoneValid = textField.getRawPhoneNumber()
        } else {
            self.txtTelephoneValid = ""
        }
    }
}
