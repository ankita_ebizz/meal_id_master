//
//  GroceriesListVc.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 01/04/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class NotificationListVcTvc : UITableViewCell {
    @IBOutlet var lblNotificationTitle: UILabel!
    @IBOutlet var lblNotificationHours: UILabel!
}

class NotificationListVc: UIViewController {
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    //MARK:- button click action
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Initialization
    func initialize() {
        
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        
    }
}
extension NotificationListVc : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 14
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListCell", for: indexPath) as! NotificationListVcTvc
        cell.selectionStyle = .none
        cell.lblNotificationTitle.text = "Update your today's weight and fat"
        cell.lblNotificationHours.text = "21 hours ago"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = MainInStoryboard.instantiateViewController(identifier: "UpdateWeightFatVC") as! UpdateWeightFatVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
