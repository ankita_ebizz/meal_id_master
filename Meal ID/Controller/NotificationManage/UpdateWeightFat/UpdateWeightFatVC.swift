//
//  UpdateWeightFatVC.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 07/04/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import MBProgressHUD

class UpdateWeightFatVC: UIViewController {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var btnLeftButton: UIButton!
    @IBOutlet var btnRightButton: UIButton!
    
    let serviceManager = ServiceManager()
    let utils = Utils()
    
    
    // MARK:- App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        setUpLayout()
    }
    //MARK:- button click action
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLeftOnClick(_ sender: UIButton) {
        if btnLeftButton.currentTitleColor == SegmentTitleColour
        {
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(SegmentBGColour, for: .normal)
            btnRightButton.backgroundColor = SegmentBGColour
            btnRightButton.setTitleColor(SegmentTitleColour, for: .normal)
        }
        
    }
    @IBAction func btnRightClick(_ sender: UIButton) {
        if btnRightButton.currentTitleColor == SegmentTitleColour
        {
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(SegmentBGColour, for: .normal)
            btnLeftButton.backgroundColor = SegmentBGColour
            btnLeftButton.setTitleColor(SegmentTitleColour, for: .normal)
        }
    }
    
    //MARK:- Initialization
    func initialize() {
        serviceManager.delegate = self
        
    }
    //MARK:- Setup Layout
    func setUpLayout() {
        btnLeftButton.RoundedButton()
        btnRightButton.RoundedButton()
        contentView.layer.cornerRadius = contentView.frame.height/2
        contentView.layer.masksToBounds = false
        
        btnLeftButton.setTitleColor(SegmentTitleColour, for: .normal)
        btnRightButton.setTitleColor(SegmentTitleColour, for: .normal)
    }
    
    // MARK:- API Calling & Parsing
    
    func callSwitchNotificationAPI(){
        
        if utils.connected(){
            let webPath = BaseURL + UpdateNotificationData
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let param =   [
                "weight" : "182",
                "bodyfat" : "20"
            ]
            
            //            let header = ["Authorization":utils.getUserAuth()!]
            let header = ["Authorization": TempAuth]
            self.serviceManager.callWebServiceWithPOST(webpath: webPath, withTag: UpdateNotificationData, params: param, header: header)
        } else {
            setPresentAlert(withTitle: "Could not connect", message: "Please check your internet connection.")
        }
    }
    
    func parseSwitchNotification(response: Any) {
        print(response)
        if let response = response as? NSDictionary {
            let success = response["success"] as! Bool
            if  success == false {
                setPresentAlert(withTitle: "Please try again.", message: "")
            } else {
                utils.showToast(message: response["message"] as! String, uiView: self.view)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}

// MARKS:- API Calling
extension UpdateWeightFatVC : ServiceManagerDelegate {
    
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
        if let response = response {
            if tagname == UpdateNotificationData {
                self.parseSwitchNotification(response: response)
            }
        }
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
        print("Tagname: ",tagname ?? "" + "Error: " , error ?? "")
        self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
