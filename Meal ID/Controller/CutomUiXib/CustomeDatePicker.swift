//
//  CustomeDatePicker.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 04/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class CustomeDatePicker: UIView {
    
    @IBOutlet var dptBirthDate: UIDatePicker!
    
    override func draw(_ rect: CGRect) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        //        txtDatePicker.text = formatter.string(from: datePicker.date)
        
        dptBirthDate.setValue(UIColor.white, forKeyPath: "textColor")
        
        DispatchQueue.main.async {
            self.dptBirthDate.subviews[0].subviews[1].backgroundColor = UIColor.white
            self.dptBirthDate.subviews[0].subviews[2].backgroundColor = UIColor.white
            self.dptBirthDate.subviews[0].subviews[1].frame.size.height = 1.0
            self.dptBirthDate.subviews[0].subviews[2].frame.size.height = 1.0
        }
    }
    
    @IBAction func dptDatePicker(_ sender: UIDatePicker) {
        let today = Date()
        let age = Calendar.current.dateComponents([.year], from: sender.date, to: today).year
        
//        if age! < 13 {
////             user is under 13
//            registerationDOBAlert = "under13"
//        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat
            registerationDOB = formatter.string(from: sender.date)
            DOBAge = age!
//        }
    }
}

