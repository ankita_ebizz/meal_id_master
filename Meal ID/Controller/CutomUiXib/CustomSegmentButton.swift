//
//  CustomSegmentButton.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 03/02/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class CustomSegmentButton: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var btnLeftButton: UIButton!
    @IBOutlet var btnRightButton: UIButton!
    
    override func draw(_ rect: CGRect) {
        btnLeftButton.RoundedButton()
        btnRightButton.RoundedButton()
        contentView.layer.cornerRadius = contentView.frame.height/2
        contentView.layer.masksToBounds = false
        
        if wieghtAndFatPicker == "HeightpickerVal" {
            btnLeftButton.setTitle(Feet_Inches, for: .normal)
            btnRightButton.setTitle(Centimeters, for: .normal)
        } else {
            //            btnLeftButton.setTitle("Pound", for: .normal)
            //            btnRightButton.setTitle("Kg", for: .normal)
            btnLeftButton.setTitle(Imperial, for: .normal)
            btnRightButton.setTitle(Metric, for: .normal)
        }
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    @IBAction func btnLeftOnClick(_ sender: UIButton) {
        if btnLeftButton.backgroundColor == SegmentBGColour && btnRightButton.backgroundColor == UIColor.white {
            
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(SegmentBGColour, for: .normal)
            btnRightButton.backgroundColor = SegmentBGColour
            btnRightButton.setTitleColor(SegmentTitleColour, for: .normal)
        }
    }
    @IBAction func btnRightClick(_ sender: UIButton) {
        if btnRightButton.backgroundColor == SegmentBGColour && btnLeftButton.backgroundColor == UIColor.white {
            
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(SegmentBGColour, for: .normal)
            btnLeftButton.backgroundColor = SegmentBGColour
            btnLeftButton.setTitleColor(SegmentTitleColour, for: .normal)
        }
    }
}
