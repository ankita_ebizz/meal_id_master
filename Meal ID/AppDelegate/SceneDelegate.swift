//
//  SceneDelegate.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        print(user.value(forKey: K_isLogIn))
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        if(user.value(forKey: K_isLogIn) as? Bool ?? false == true) {
            self.window = UIWindow(windowScene: windowScene)
            guard let rootVC = MainInStoryboard.instantiateViewController(identifier: id_HomePage) as? HomePage else {
                print("ViewController not found")
                return
            }
            let rootNC = UINavigationController(rootViewController: rootVC)
            rootNC.navigationBar.isHidden = true
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
            
        } else {
            guard let rootVC = SignUpStoryboard.instantiateViewController(identifier: id_SignUpVC) as? SignUpVC else {
                print("ViewController not found")
                return
            }
            let rootNC = UINavigationController(rootViewController: rootVC)
            rootNC.navigationBar.isHidden = true
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources ass ociated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}

