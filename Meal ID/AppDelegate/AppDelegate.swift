//
//  AppDelegate.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 31/01/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn
import Firebase
import FirebaseMessaging
import FacebookCore
 
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate,MessagingDelegate {
    
    var window: UIWindow?
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        guard let urlScheme = url.scheme else { return false }
        if urlScheme.hasPrefix("fb") {
            return ApplicationDelegate.shared.application(app, open: url, options: options)
        }
        return GIDSignIn.sharedInstance().handle(url)
    }
    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
//        return GIDSignIn.sharedInstance().handle(url)
//    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

        // firebase notification
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        IQKeyboardManager.shared.overrideKeyboardAppearance = true
        IQKeyboardManager.shared.keyboardAppearance = .dark
        
        GIDSignIn.sharedInstance().clientID = GooGle_ClientID
        GIDSignIn.sharedInstance().delegate = self
        
        //        self.isSignUpdone() // skip sign up screen after reg
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
 
        return true
    }
    // MARK:-
    // MARK:- FCM token
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print(fcmToken)
        FireBaseToken = fcmToken
    }
    
    
    // MARK:- onBoard screens For 1st time App installing only management
    func isSignUpdone() {
        print(user.value(forKey: K_isLogIn))
        
        if(user.value(forKey: K_isLogIn) as? Bool ?? false == true) {
            let nextViewController = MainInStoryboard.instantiateViewController(withIdentifier: id_HomePage) as! HomePage
            let navC = UINavigationController(rootViewController: nextViewController)
            navC.navigationBar.isHidden = true
            UIApplication.shared.windows.first?.rootViewController = navC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            let nextViewController = SignUpStoryboard.instantiateViewController(withIdentifier: id_SignUpVC) as! SignUpVC
            let navC = UINavigationController(rootViewController: nextViewController)
            navC.navigationBar.isHidden = true
            UIApplication.shared.windows.first?.rootViewController = navC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        // ...
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
}


//MARK:- UNUserNotificationCenter Delegate
 extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Device Token: ",deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("didReceiveRemoteNotification", userInfo as NSDictionary)
       
//        if getUserRole() == "0" {
//            self.handleNotification(response: userInfo as NSDictionary)
//        }
//        else if getUserRole() == "1" {
//            handleDriverNotification(response: userInfo as NSDictionary)
//        }
        
        completionHandler(UIBackgroundFetchResult.newData)
        // if app is running
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("didReceive",response)
        completionHandler()
        // when clicked on notification
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
                print("willPresent", notification.request.content.userInfo)
        
//        let Resultdict = notification.request.content.userInfo as NSDictionary
//        let notificationType = Resultdict.value(forKey: "gcm.notification.type") as! String
        
//        if notificationType == "11" {
//            flagForClosingRequest = "rider_cancel"
//        }
//        else if notificationType == "10" {
//            flagForRequestScreen_DriverSide = true
//        }
        completionHandler([.alert, .badge, .sound])
    }
}
