
//  Utils.swift
//  SwiftProjectStructure
//
//  Created by EbitNHP-i1 on 31/10/19.
//  Copyright © 2019 EbitNHP-i1. All rights reserved.
//

import UIKit
import Reachability
import MBProgressHUD


class Utils: NSObject {
    
    func setUserData(data: [String: Any]) {
        user.set(data, forKey: "userDetails")
    }
    func getUserData()-> NSDictionary? {
        if let userDetails = UserDefaults.standard.object(forKey: "userDetails") as? NSDictionary {
            return userDetails
        } else {
            return nil
        }
    }
    func setUserAuth(data: String) {
        user.set(data, forKey: "userAuthToken")
    }
    func getUserAuth()-> String? {
        if let auth = UserDefaults.standard.string(forKey: "userAuthToken") {
            return auth
        } else {
            return nil
        }
    }
    // MARK:-
    // MARK:- Meal plan setting
    func setBodyType(data : String) {
        user.set(data, forKey: "userBodyType")
    }
    func getBodyType()-> String? {
        if let body = UserDefaults.standard.string(forKey: "userBodyType") {
            return body
        } else {
            return nil
        }
    }
 
    func connected() -> Bool {
        let reachability = Reachability.forInternetConnection()
        let status : NetworkStatus = reachability!.currentReachabilityStatus()
        if status == .NotReachable {
            return false
        } else {
            return true
        }
    }
    
    func getAppDelegate() -> Any? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    //MARK: - SHOW HUD
    func ShowHUD(inView : UIView)  {
        MBProgressHUD.showAdded(to: inView, animated: true)
    }
    func dismissHUD(fromView : UIView ) {
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    //    func getNotiData()-> Data? {
    //        if let arrAlertsData = UserDefaults.standard.data(forKey: "arrAlerts") as Data? {
    //            return arrAlertsData
    //        } else {
    //            return nil
    //        }
    //    }
    
    func showToast(message : String,uiView: UIView) {
        let toastLabel = UILabel(frame: CGRect(x: 35, y: uiView.frame.size.height-200, width:uiView.frame.size.width-70, height: 35))
        toastLabel.numberOfLines = 0
        toastLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "ARSMaquettePro-Regular", size: 16.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = toastLabel.frame.size.height/2
        toastLabel.clipsToBounds  =  true
        uiView.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.8, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }) { (isCompleted) in
            toastLabel.removeFromSuperview()
        }
    }
}
