//
//  ServiceManager.swift
//  SwiftProjectStructure
//
//  Created by EbitNHP-i1 on 31/10/19.
//  Copyright © 2019 EbitNHP-i1. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

@objc protocol ServiceManagerDelegate: NSObjectProtocol {
    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?)
    func webServiceCallFailure(_ error: Error?, forTag tagname: String?)
}

class ServiceManager: NSObject {
    
    var delegate:ServiceManagerDelegate?
     let utils = Utils()
    
    //GET Service Calling
    func callWebServiceWithGET(webpath: String?, withTag tagname: String?, header : HTTPHeaders) {
 
        Alamofire.request(webpath!, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: header).authenticate(user: "", password: "").responseSwiftyJSON(completionHandler:{ (response) in
            print(header)
            if let json = response.result.value {
                 guard let rawData = try? json.rawString()?.data(using: .utf8) else {
                    return
                }
                 var dicResponse : [String : AnyObject] = [:]
                do {
                    dicResponse =  (try JSONSerialization.jsonObject(with: rawData, options: []) as? [String:AnyObject])!
                    print(dicResponse)
                } catch let error as NSError {
                    print(error)
                    if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                        self.delegate?.webServiceCallFailure(error, forTag: tagname)
                    }
                    return
                }
                 if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallSuccess(_:forTag:))))! {
                    self.delegate?.webServiceCallSuccess(dicResponse, forTag: tagname)
                }
            } else {
                if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                    self.delegate?.webServiceCallFailure(response.result.error, forTag: tagname)
                }
            }
        })
    }
    
    
    //DELETE Service Calling
    func callWebServiceWithDELETE(webpath: String?, withTag tagname: String?, header : HTTPHeaders) {
 
            Alamofire.request(webpath!, method: .delete, parameters: nil, encoding: URLEncoding.httpBody, headers: header).authenticate(user: "", password: "").responseSwiftyJSON(completionHandler:{ (response) in
                print(header)
                if let json = response.result.value {
                     guard let rawData = try? json.rawString()?.data(using: .utf8) else {
                        return
                    }
                     var dicResponse : [String : AnyObject] = [:]
                    do {
                        dicResponse =  (try JSONSerialization.jsonObject(with: rawData, options: []) as? [String:AnyObject])!
                        print(dicResponse)
                    } catch let error as NSError {
                        print(error)
                        if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                            self.delegate?.webServiceCallFailure(error, forTag: tagname)
                        }
                        return
                    }
                    
                    if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallSuccess(_:forTag:))))! {
                        self.delegate?.webServiceCallSuccess(dicResponse, forTag: tagname)
                    }
                } else {
                    if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                        self.delegate?.webServiceCallFailure(response.result.error, forTag: tagname)
                    }
                }
            })
        }
    
    //POST Service Calling
    func callWebServiceWithPOST(webpath: String?, withTag tagname: String?, params: Parameters, header : HTTPHeaders) {
 
        Alamofire.request(webpath!, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: header).authenticate(user: "", password: "").responseSwiftyJSON(completionHandler:{ (response) in
            
            print(response.request)
            print(response.result)
            
            if let json = response.result.value { 
                /*  guard !json["data"].isEmpty else{
                 return
                 }*/
                guard let rawData = try? json.rawString()?.data(using: .utf8) else {
                    return
                }
                
                var dicResponse : [String : AnyObject] = [:]
                do {
                    dicResponse =  (try JSONSerialization.jsonObject(with: rawData, options: []) as? [String:AnyObject])!
                    print(dicResponse)
                } catch let error as NSError {
                    print(error)
                    if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                        self.delegate?.webServiceCallFailure(error, forTag: tagname)
                    }
                    return
                }
                
                if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallSuccess(_:forTag:))))! {
                    self.delegate?.webServiceCallSuccess(dicResponse, forTag: tagname)
                }
                
            } else {
                if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                    self.delegate?.webServiceCallFailure(response.result.error, forTag: tagname)
                }
            }
            
        })
    }
    
    func callWebServiceWithPOST(webpath: String?, withTag tagname: String?, params: Parameters, imgArray:[AttachmentViewModel], header : HTTPHeaders)  {
        let unit64:UInt64 = 10_000_000
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for model in imgArray  {
                let imgData = model.Image.jpegData(compressionQuality: 0.7)
                multipartFormData.append(imgData!, withName: model.ImageFileName, fileName: "\(model.ImageFileName).png", mimeType: "image/png")
            }
            
            for (key, value) in params {
//                multipartFormData.append((value as AnyObject).data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue).rawValue)!, withName: key)
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
            print(multipartFormData)
        }, usingThreshold: unit64, to: webpath!, method: .post, headers: header, encodingCompletion: { (encodingResult) in
            print("encoding result:\(encodingResult)")
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    //send progress using delegate
                })
                upload.responseSwiftyJSON(completionHandler: { (response) in
                    print("response:==>\(response)")
                    
                    if let json = response.result.value {
                        guard let rawData = try? json.rawString()?.data(using: .utf8) else {
                            return
                        }
                        
                        var dicResponse : [String : AnyObject] = [:]
                        do {
                            dicResponse =  (try JSONSerialization.jsonObject(with: rawData, options: []) as? [String:AnyObject])!
                            print(dicResponse)
                        } catch let error as NSError {
                            print(error)
                            if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                                self.delegate?.webServiceCallFailure(error, forTag: tagname)
                            }
                            return
                        }
                        
                        if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallSuccess(_:forTag:))))! {
                            self.delegate?.webServiceCallSuccess(dicResponse, forTag: tagname)
                        }
                        
                    }
                })
                
            case .failure(let encodingError):
                if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                    self.delegate?.webServiceCallFailure(encodingError, forTag: tagname)
                }
            }
        })
    }
    
    //POST Service Calling image upload without param
    
    func callWebServiceWithPOSTImage(webpath: String?, withTag tagname: String?,imgArray:[AttachmentViewModel])  {
        let unit64:UInt64 = 10_000_000
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for model in imgArray  {
                let imgData = model.Image.jpegData(compressionQuality: 0.7)
                multipartFormData.append(imgData!, withName: model.ImageFileName, fileName: model.ImageFileName, mimeType: "image/png")
            }
            
        }, usingThreshold: unit64, to: webpath!, method: .post, headers: nil, encodingCompletion: { (encodingResult) in
            print("encoding result:\(encodingResult)")
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    //send progress using delegate
                })
                upload.responseSwiftyJSON(completionHandler: { (response) in
                    print("response:==>\(response)")
                    
                    if let json = response.result.value {
                        guard let rawData = try? json.rawString()?.data(using: .utf8) else {
                            return
                        }
                        var dicResponse : [String : AnyObject] = [:]
                        do {
                            dicResponse =  (try JSONSerialization.jsonObject(with: rawData, options: []) as? [String:AnyObject])!
                            print(dicResponse)
                        } catch let error as NSError {
                            print(error)
                            if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                                self.delegate?.webServiceCallFailure(error, forTag: tagname)
                            }
                            return
                        }
                        if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallSuccess(_:forTag:))))! {
                            self.delegate?.webServiceCallSuccess(dicResponse, forTag: tagname)
                        }
                    }
                })
                
            case .failure(let encodingError):
                if (self.delegate?.responds(to: #selector(self.delegate?.webServiceCallFailure(_:forTag:))))! {
                    self.delegate?.webServiceCallFailure(encodingError, forTag: tagname)
                }
            }
        })
    }
}
