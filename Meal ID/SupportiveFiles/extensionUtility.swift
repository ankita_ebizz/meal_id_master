//
//  Utility.swift
//  TAXISYS
//
//  Created by EbitNHP-i1 on 16/05/19.
//  Copyright © 2019 EbitNHP-i1. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import MapKit

var vSpinner : UIView?

extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}

extension UIButton
{
    func RoundedButtonWithBorderCLR(radius : CGFloat,colour : UIColor) {
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 1
        self.layer.borderColor = colour.cgColor
    }
    func RoundedButton() {
        self.layer.cornerRadius = self.frame.height/2
    }
    func setCircularButton() {
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
    }
}
extension UIImageView
{
    //MARK:- for round profile image
    func roundedProfileImage(borderWidth : CGFloat , borderColour : UIColor)
    {
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
        self.layer.borderColor = borderColour.cgColor
        self.layer.cornerRadius = self.frame.height/2
    }
    @IBInspectable override var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        } set {
            self.layer.cornerRadius = newValue
        }
    }
    func roundedImage(curvedPercent:CGFloat) {
        let maskLayer = CAShapeLayer(layer: self.layer)
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x:0, y:0))
        bezierPath.addLine(to: CGPoint(x:self.bounds.size.width, y:0))
        bezierPath.addLine(to: CGPoint(x:self.bounds.size.width, y:self.bounds.size.height*curvedPercent))
        
        bezierPath.addQuadCurve(to: CGPoint(x:0, y:self.bounds.size.height*curvedPercent), controlPoint: CGPoint(x:self.bounds.size.width/2, y:self.bounds.size.height))
        bezierPath.addLine(to: CGPoint(x:0, y:0))
        bezierPath.close()
        maskLayer.path = bezierPath.cgPath
        maskLayer.frame = self.bounds
        maskLayer.masksToBounds = true
        self.layer.mask = maskLayer
    }
    
    func BottomRounded()
    {
        let maskLayer = CAShapeLayer(layer: self.layer)
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x:0, y:0))
        bezierPath.addLine(to: CGPoint(x:self.bounds.size.width, y:0))
        bezierPath.addLine(to: CGPoint(x:self.bounds.size.width, y:self.bounds.size.height))
        //        bezierPath.addQuadCurve(to: CGPoint(x:0, y:self.bounds.size.height), controlPoint: CGPoint(x:self.bounds.size.width/2, y:self.bounds.size.height-self.bounds.size.height*0.3))
        bezierPath.addQuadCurve(to: CGPoint(x:0, y:self.bounds.size.height/2), controlPoint: CGPoint(x:self.bounds.size.width/2, y:self.bounds.size.height-self.bounds.size.height*0.3))
        bezierPath.addLine(to: CGPoint(x:0, y:0))
        bezierPath.close()
        maskLayer.path = bezierPath.cgPath
        maskLayer.frame = self.bounds
        maskLayer.masksToBounds = true
        self.layer.mask = maskLayer
    }
}

extension String {
    
    var isContainsLetters : Bool{
        let letters = CharacterSet.letters
        return self.rangeOfCharacter(from: letters) != nil
    }
    var containsWhitespace : Bool {
        return false
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
    func indexExplode(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func explode(from: Int) -> String {
        let fromIndex = indexExplode(from: from)
        return String(self[fromIndex...])
    }
    
    func explode(to: Int) -> String {
        let toIndex = indexExplode(from: to)
        return String(self[..<toIndex])
    }
    
    func explode(with r: Range<Int>) -> String {
        let startIndex = indexExplode(from: r.lowerBound)
        let endIndex = indexExplode(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            let htmlToString = try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            return newAttrSize(blockQuote: htmlToString)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    func newAttrSize(blockQuote: NSAttributedString) -> NSAttributedString
    {
        let yourAttrStr = NSMutableAttributedString(attributedString: blockQuote)
        yourAttrStr.enumerateAttribute(.font, in: NSMakeRange(0, yourAttrStr.length), options: .init(rawValue: 0)) {
            (value, range, stop) in
            // *** Create instance of `NSMutableParagraphStyle`
            let paragraphStyle = NSMutableParagraphStyle()
            
            // *** set LineSpacing property in points ***
            paragraphStyle.lineSpacing = 4 // Whatever line spacing you want in points
            
            let font = UIFont(name: "ARSMaquettePro-Regular", size: 16)!
            yourAttrStr.addAttribute(.font, value: font, range: range)
            yourAttrStr.addAttribute(.paragraphStyle,value: paragraphStyle, range: NSRange(location: 0, length: yourAttrStr.length))
        }
        
        return yourAttrStr
    }
}

extension NSMutableAttributedString {
    
    func setColor(color: UIColor, forText stringValue: String) {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
}

extension UITextView
{
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

extension UITextField
{
    //    func KeyboardReturnBtnName(KeyType : UIReturnKeyType) {
    //        self.returnKeyType = KeyType
    //    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    //MARK:- for Placeholder name Colour
    func setPlaceholdernameColour(titleName : String , titleColour : UIColor){
        self.attributedPlaceholder = NSAttributedString(string: titleName, attributes: [NSAttributedString.Key.foregroundColor: titleColour])
    }
    //MARK:- for Placeholder padding
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}

extension UIViewController
{
    
    //MARK:- for Showing Alert
    func setPresentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:-  regex method
    //MARK:- for mail validation
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    //MARK:- for phone validation
    func isValidPhoneNum(testStr:String) -> Bool {
        let PhoneRegEx = "^[7-9][0-9]{9}$"
        let PhoneTest = NSPredicate(format:"SELF MATCHES %@", PhoneRegEx)
        return PhoneTest.evaluate(with: testStr)
    }
    func isValidPassword(_ passwordString: String?) -> Bool {
        let stricterFilterString = "^(?=.*[A-Z])(?=.*[a-z]).{6,}$"
        //      let stricterFilterString = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        return passwordTest.evaluate(with: passwordString)
    }
    
}
//MARK:- UIview extension
extension UIView
{
    //MARK:- for addShadowWithCorner view
    
    func addShadowWithCorner(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    //MARK:- for fully circular rounded view
    func setroundedviewRadius(borderWidth : CGFloat , borderColour : UIColor)
    {
        self.layer.borderWidth = borderWidth
        //        self.layer.masksToBounds = true
        self.layer.borderColor = borderColour.cgColor
        self.layer.cornerRadius = self.frame.height/2
    }
    
    //  Top round Corners
    func ToproundCorners(corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect:self.bounds,
                                byRoundingCorners:corners,
                                cornerRadii: CGSize(width: radius, height:  radius))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    // animation on view
    func setAnimationOnView(hidden: Bool)
    {
        UIView.transition(with: self, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.isHidden = hidden
        })
    }
    
    func ShadowAsSideChoice(side : String)
    {
        switch side {
        case "All":
            // corner radius
            self.layer.cornerRadius = 10
            
            // border
            self.layer.borderWidth = 0.2
            self.layer.borderColor = UIColor.clear.cgColor
            
            // shadow
            self.layer.shadowColor = UIColor.lightGray.cgColor
            self.layer.shadowOffset = CGSize(width: 0, height: 3) //CGSize.zero
            self.layer.shadowOpacity = 2
            self.layer.shadowRadius = 3
            break
        case "Top":
            // corner radius
            self.layer.cornerRadius = 1
            
            // border
            self.layer.borderWidth = 0.2
            self.layer.borderColor = UIColor.clear.cgColor
            
            // shadow
            self.layer.shadowColor = UIColor.lightGray.cgColor
            self.layer.shadowOffset = CGSize(width: 0, height: -10)
            self.layer.shadowOpacity = 0.1
            self.layer.shadowRadius = 3
            break
            
        case "Top&Bottom" :
            // corner radius
            self.layer.cornerRadius = 10
            
            // border
            self.layer.borderWidth = 0.5
            self.layer.borderColor = UIColor.clear.cgColor
            
            // shadow
            self.layer.shadowColor = UIColor.lightGray.cgColor
            self.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.layer.shadowOpacity = 0.7
            self.layer.shadowRadius = 4.0
            break
        case "Bottom" :
            
            // corner radius
            self.layer.cornerRadius = 10
            
            // border
            self.layer.borderWidth = 0.5
            self.layer.borderColor = UIColor.clear.cgColor
            // shadow
            self.layer.shadowColor = UIColor.lightGray.cgColor
            self.layer.shadowOffset = CGSize(width: 0, height: 3)
            self.layer.shadowOpacity = 0.3
            self.layer.shadowRadius = 0.5
            break
        case "None" :
            
            // corner radius
            self.layer.cornerRadius = 10
            
            // border
            self.layer.borderWidth = 0.2
            self.layer.borderColor = UIColor.clear.cgColor
            // shadow
            self.layer.shadowColor = UIColor.clear.cgColor
            self.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.layer.shadowOpacity = 0
            self.layer.shadowRadius = 0
            break
        default:
            print("default")
        }
    }
    
    //MARK:- for rounded view
    func setviewRadius()
    {
        self.layer.cornerRadius = 10
    }
    
    // shadow on all side
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        } set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        } set {
            self.layer.cornerRadius = self.layer.frame.height/10
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    @IBInspectable var forceRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    func addShadow(shadowColor: CGColor = UIColor.lightGray.cgColor,
                   shadowOffset: CGSize = CGSize.zero,
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 5.0)
    {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    func vwBottomCurved() {
        let rect = self.bounds
        let y = rect.size.height - 60.0
        let curveTo = rect.size.height
        
        let myBez = UIBezierPath()
        myBez.move(to: CGPoint(x: 0.0, y: y))
        myBez.addQuadCurve(to: CGPoint(x: rect.size.width, y: y), controlPoint: CGPoint(x: rect.size.width / 2.0, y: curveTo))
        myBez.addLine(to: CGPoint(x: rect.size.width, y: 0.0))
        myBez.addLine(to: CGPoint(x: 0.0, y: 0.0))
        myBez.close()
        
        let maskForPath = CAShapeLayer()
        maskForPath.path = myBez.cgPath
        self.layer.mask = maskForPath
        
    }
    
}

extension UIColor {
    
    /// Initializes and returns a color object using the specified HexString values. Returns nil if hexstring is not valid.
    ///
    /// - Parameters:
    ///   - hexString: The `HexString` of color.
    /// - Returns: Returns color object. The color information represented by this object is in an RGB colorspace.
    public convenience init?(hexString: String) {
        
        var chars = Array(hexString.uppercased().hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            print("Invalid HexString, number of characters after '#' should be either 3, 6 or 8")
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: 1)
    }
    
}

extension UITableView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleRows?.last else {
            return false
        }
        
        return lastIndexPath == indexPath
    }
}

extension UITableViewCell {
    func setPresentAlert(withTitle title: String, message : String, delegate: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
        }
        alertController.addAction(OKAction)
        delegate.present(alertController, animated: true, completion: nil)
        
    }
}

extension UICollectionView{
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleItems.last else {
            return false
        }
        
        return lastIndexPath == indexPath
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        var hexInt: UInt32 = 0
        let scanner = Scanner(string: hexString)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)
        
        let red = CGFloat((hexInt & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexInt & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexInt & 0xff) >> 0) / 255.0
        let alpha = alpha
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}

 
