////
////  SignUpVC.swift
////  TAXISYS
////
////  Created by EbitNHP-i1 on 17/05/19.
////  Copyright © 2019 EbitNHP-i1. All rights reserved.
////
//
//import UIKit
//import MBProgressHUD
//
//class demovc: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,ServiceManagerDelegate{
//
//    @IBOutlet weak var progressView: UIProgressView!
//    @IBOutlet weak var clvSignUpStep: UICollectionView!
//    @IBOutlet weak var btnNextSetCorner: UIButton!
//
//    @IBOutlet weak var imgTaxiAnimation: UIImageView!
//    @IBOutlet weak var cnstTaxiToMove: NSLayoutConstraint!
//    @IBOutlet weak var cnstBottomNextbuttonMove: NSLayoutConstraint!
//
//    var CurrentPage = 0
//
//    var insertDataDic = [String: Any]()
//    let serviceManager = ServiceManager()
//    var user_role: String!
//    var isNormalSignup = true
//    var signUp: String!
//
//    var checkMobiId: String!
//
//    override func viewDidLoad()
//    {
//        super.viewDidLoad()
//        initiliazation()
//        SetUpLayout()
//    }
//    override func viewDidLayoutSubviews()
//    {
//        btnNextSetCorner.roundCorners(topLeft: 5.0, topRight: btnNextSetCorner.frame.height/2, bottomLeft: 5.0, bottomRight: btnNextSetCorner.frame.height/2)
//    }
//
//    //MARK:- Set progress bar Value
//    func setProgress(progress: Int)
//    {
//        let processTotValue: Float = user_role == "1" ? 8.0 : 6.0
//        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
//            self.cnstTaxiToMove.constant = self.progressView.frame.width/CGFloat(processTotValue) * CGFloat(progress)
//        }, completion: nil)
//        progressView.setProgress(Float(progress)/processTotValue, animated: true)
//    }
//
//    //MARK:- operation on btnNextClick
//    @IBAction func btnNextClick(_ sender: UIButton)
//    {
//        sender.isEnabled = false
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            sender.isEnabled = true
//        }
//        if(user_role == "0")
//        {
//            switch CurrentPage
//            {
//            // "What should we call you ?"
//            case 0:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep1VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep1VC
//                if (cell.tfFirstName.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in first name.")
//                    return
//                }
//                else if(cell.tfFirstName.text! == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your First Name.")
//                    return
//                }
//                else if (cell.tfLastName.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in last name.")
//                    return
//                }
//                else if(cell.tfLastName.text! == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your Last Name.")
//                    return
//                }
//                btnNextSetCorner.setTitle("NEXT", for: UIControl.State.normal)
//                insertDataDic["first_name"] = cell.tfFirstName.text
//                insertDataDic["last_name"] = cell.tfLastName.text
//                break;
//
//            // What's your email ?
//            case 1:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep2VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep2VC
//                if (cell.tfEmailID.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in email.")
//                    return
//                }
//                else if(cell.tfEmailID.text! == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your Email.")
//                    return
//                }
//                else if(isValidEmail(testStr: cell.tfEmailID.text!) != true){
//                    setPresentAlert(withTitle: AppName, message: "invalid email address")
//                    cell.tfEmailID.text = ""
//                    return
//                }
//                insertDataDic["email"] = cell.tfEmailID.text
//                btnNextSetCorner.setTitle("NEXT", for: UIControl.State.normal)
//                break;
//
//            // Choose a password
//            case 2:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep3VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep3VC
//                if (cell.tfPassword.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in password.")
//                    return
//                }
//                else if(cell.tfPassword.text! == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your Password")
//                    cell.lblPassWarning.isHidden = true
//                    return
//                }else if (isValidPassword(cell.tfPassword.text!) != true)
//                {
//                    cell.lblPassWarning.isHidden = false
//                    setPresentAlert(withTitle: AppName, message: "Please follow instruction as shown below password field")
//                    return
//                }else{
//                    cell.lblPassWarning.isHidden = true
//
//                }
//                insertDataDic["password"] = cell.tfPassword.text
//                btnNextSetCorner.setTitle("SEND OTP", for: UIControl.State.normal)
//                break;
//
//            // Enter your mobile number
//            case 3:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep4VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep4VC
//                var tmpMobile = "+\(COUNTRY_CODE)\(cell.tfMobileNum.text!)"
//                if (cell.tfMobileNum.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in mobile number.")
//                    return
//                }
//                else if(cell.tfMobileNum.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your Mobile number.")
//                    return
//                }
//                else if((cell.tfMobileNum.text?.starts(with: "91"))! && cell.tfMobileNum.text!.count == 12)
//                {
//                    tmpMobile = "+\(cell.tfMobileNum.text!)"
//                }
//                else if(cell.tfMobileNum.text?.count != MOBILE_NUMBER_VALIDATION){
//                    setPresentAlert(withTitle: AppName, message: "Please enter valid number.")
//                    return
//                }
//                //                btnNextSetCorner.setTitle("SEND OTP", for: UIControl.State.normal)
//                CurrentPage -= 1
//                MBProgressHUD.showAdded(to: self.view, animated: true)
//                self.insertDataDic["phone"] = tmpMobile
//                CheckMobiService.init().request(ValidationTypeSMS, forNumber: tmpMobile, withResponse: { (status, response, error) in
//                    if(status == kStatusSuccessWithContent && response != nil){
//                        let dictResponse = response! as NSDictionary
//                        self.checkMobiId = (dictResponse.value(forKey: "id") as! String)
//
//                        self.CurrentPage += 1
//                        let indexPath = IndexPath(item: self.CurrentPage, section: 0)
//                        self.clvSignUpStep.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//                        self.setProgress(progress: self.CurrentPage)
//
//                        let tempIndex = IndexPath(row: self.CurrentPage, section: 0)
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
//                            MBProgressHUD.hide(for: self.view, animated: true)
//                            let tempCell = self.clvSignUpStep.cellForItem(at: tempIndex) as? SignupStep5VC
//                            if tempCell != nil
//                            {
//                                tempCell!.lblDisplayPhoneNum.text = "sent to \(self.insertDataDic["phone"] as! String)"
//                                tempCell!.txtOTP1.text = ""
//                                tempCell!.txtOTP2.text = ""
//                                tempCell!.txtOTP3.text = ""
//                                tempCell!.txtOTP4.text = ""
//                                tempCell!.txtOTP.text = ""
//                            }
//                        }
//                        self.btnNextSetCorner.setTitle("SUBMIT", for: UIControl.State.normal)
//                    }
//                    else{
//                        if(response != nil)
//                        {
//                            let dictResponse = response! as NSDictionary
//                            if(dictResponse.value(forKey: "error") as? String != nil)
//                            {
//                                self.setPresentAlert(withTitle: AppName, message: dictResponse.value(forKey: "error") as! String)
//                            }
//                        }
//                        else
//                        {
//                            print(error)
//                        }
//                        MBProgressHUD.hide(for: self.view, animated: true)
//                    }
//                })
//                break;
//            // Verify code
//            case 4:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep5VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep5VC
//                if(cell.txtOTP1.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
//                    return
//                }else if(cell.txtOTP2.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
//                    return
//                }else if(cell.txtOTP3.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
//                    return
//                }else if(cell.txtOTP4.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
//                    return
//                }
//                
//                let pin = cell.txtOTP1.text! + cell.txtOTP2.text! + cell.txtOTP3.text! + cell.txtOTP4.text!
//                MBProgressHUD.showAdded(to: self.view, animated: true)
//                
//                CheckMobiService.init().verifyPin(checkMobiId, withPin: pin, withResponse: { (status, response, error) in
//                    if(status == kStatusSuccessWithContent && response != nil)
//                    {
//                        let dictData = response! as NSDictionary
//                        if(dictData["validated"] as! Int == 1)
//                        {
//                            self.insertDataDic["phone"] = dictData["number"] as! String
//                            self.insertDataDic["user_role"] = self.user_role
//                            self.insertDataDic["device_type"] = "I"
//                            self.insertDataDic["device_token"] = device_token
//                            self.insertDataDic["user_image"] = ""
//
//                            if(self.signUp == GOOGLE_LOGIN_URL)
//                            {
//                                self.serviceManager.callWebServiceWithPOST(webpath: BASE_URL + GOOGLE_LOGIN_URL, withTag: GOOGLE_LOGIN_URL, params: self.insertDataDic)
//                            }
//                            else if(self.signUp == FB_LOGIN_URL)
//                            {
//                                self.serviceManager.callWebServiceWithPOST(webpath: BASE_URL + FB_LOGIN_URL, withTag: FB_LOGIN_URL, params: self.insertDataDic)
//                            }
//                            else
//                            {
//                                self.serviceManager.callWebServiceWithPOST(webpath: BASE_URL + ADD_USER_URL, withTag: ADD_USER_URL, params: self.insertDataDic)
//                            }
//                        }
//                        else
//                        {
//                            MBProgressHUD.hide(for: self.view, animated: true)
//                            self.setPresentAlert(withTitle: AppName, message: "Invalid OTP.")
//                        }
//                    }
//                    else
//                    {
//                        MBProgressHUD.hide(for: self.view, animated: true)
//                        //                        self.setPresentAlert(withTitle: "", message: error as! String)
//                        print(error)
//                    }
//                })
//                return
//            default:
//                print("Default")
//                break;
//            }
//            CurrentPage += 1
//            if(CurrentPage < 5)
//            {
//                let indexPath = IndexPath(item: CurrentPage, section: 0)
//                clvSignUpStep.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//                self.setProgress(progress: CurrentPage)
//            }
//        }
//        else
//        {
//            switch CurrentPage
//            {
//            // "What should we call you ?"
//            case 0:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep1VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep1VC
//                if (cell.tfFirstName.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in first name.")
//                    return
//                }
//                else if(cell.tfFirstName.text! == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your First Name.")
//                    return
//                }
//                else if (cell.tfLastName.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in last name.")
//                    return
//                }
//                else if(cell.tfLastName.text! == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your Last Name.")
//                    return
//                }
//                btnNextSetCorner.setTitle("NEXT", for: UIControl.State.normal)
//                insertDataDic["first_name"] = cell.tfFirstName.text
//                insertDataDic["last_name"] = cell.tfLastName.text
//                break;
//
//            // What's your email ?
//            case 1:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep2VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep2VC
//                if (cell.tfEmailID.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in email.")
//                    return
//                }
//                else if(cell.tfEmailID.text! == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your Email.")
//                    return
//                }
//                else if(isValidEmail(testStr: cell.tfEmailID.text!) != true){
//                    setPresentAlert(withTitle: AppName, message: "invalid email address")
//                    cell.tfEmailID.text = ""
//                    return
//                }
//                insertDataDic["email"] = cell.tfEmailID.text
//                btnNextSetCorner.setTitle("NEXT", for: UIControl.State.normal)
//                break;
//
//            // Choose a password
//            case 2:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep3VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep3VC
//
//                if (cell.tfPassword.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in password.")
//                    return
//                } else if(cell.tfPassword.text! == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your Password")
//                    cell.lblPassWarning.isHidden = true
//                    return
//                } else if (isValidPassword(cell.tfPassword.text!) != true) {
//                    cell.lblPassWarning.isHidden = false
//                    setPresentAlert(withTitle: AppName, message: "Please follow instruction as shown below password field")
//                    return
//                } else {
//                    cell.lblPassWarning.isHidden = true
//                }
//                insertDataDic["password"] = cell.tfPassword.text
//                btnNextSetCorner.setTitle("NEXT", for: UIControl.State.normal)
//                break;
//
//            case 3:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep6VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep6VC
//
//                if(cell.tfVehicleNumber.text?.trimmingCharacters(in:  NSCharacterSet.whitespacesAndNewlines) == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your vehicle number.")
//                }
//                insertDataDic["vehicle_number"] = cell.tfVehicleNumber.text!
//                btnNextSetCorner.setTitle("NEXT", for: UIControl.State.normal)
//                break;
//            case 4:
//                let carData = getCarDetails()
//                print(carData)
//                //                if carData.count != 0 {
//                insertDataDic["vehicle_type"] = carData["car_type"] as! String
//                //                } else  {
//                //                    insertDataDic["vehicle_type"] = carData["car_type"] as! String
//                //                }
//                btnNextSetCorner.setTitle("SEND OTP", for: UIControl.State.normal)
//                break;
//
//            // Enter your mobile number
//            case 5:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep4VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep4VC
//                var tmpMobile = "+\(COUNTRY_CODE)\(cell.tfMobileNum.text!)"
//                if (cell.tfMobileNum.text?.containsWhitespace == true) {
//                    setPresentAlert(withTitle: AppName, message: "White space not allowed in mobile number.")
//                    return
//                }
//                else if(cell.tfMobileNum.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your Mobile number.")
//                    return
//                }
//                else if((cell.tfMobileNum.text?.starts(with: "91"))! && cell.tfMobileNum.text!.count == 12)
//                {
//                    tmpMobile = "+\(cell.tfMobileNum.text!)"
//                }
//                else if(cell.tfMobileNum.text?.count != MOBILE_NUMBER_VALIDATION){
//                    setPresentAlert(withTitle: AppName, message: "Please enter valid number.")
//                    return
//                }
//                //                btnNextSetCorner.setTitle("SEND OTP", for: UIControl.State.normal)
//                //                btnNextSetCorner.setTitle("SUBMIT", for: UIControl.State.normal)
//                CurrentPage -= 1
//                MBProgressHUD.showAdded(to: self.view, animated: true)
//                insertDataDic["phone"] = tmpMobile
//                CheckMobiService.init().request(ValidationTypeSMS, forNumber: tmpMobile, withResponse: { (status, response, error) in
//                    if(status == kStatusSuccessWithContent && response != nil){
//                        let dictResponse = response! as NSDictionary
//                        self.checkMobiId = (dictResponse.value(forKey: "id") as! String)
//
//                        self.CurrentPage += 1
//                        let indexPath = IndexPath(item: self.CurrentPage, section: 0)
//                        self.clvSignUpStep.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//                        self.setProgress(progress: self.CurrentPage)
//
//                        let tempIndex = IndexPath(row: self.CurrentPage, section: 0)
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
//                            MBProgressHUD.hide(for: self.view, animated: true)
//                            let tempCell = self.clvSignUpStep.cellForItem(at: tempIndex) as? SignupStep5VC
//                            if tempCell != nil
//                            {
//                                tempCell!.lblDisplayPhoneNum.text = "sent to \(self.insertDataDic["phone"] as! String)"
//                                tempCell!.txtOTP1.text = ""
//                                tempCell!.txtOTP2.text = ""
//                                tempCell!.txtOTP3.text = ""
//                                tempCell!.txtOTP4.text = ""
//                                tempCell!.txtOTP.text = ""
//                            }
//                        }
//                        self.btnNextSetCorner.setTitle("SUBMIT", for: UIControl.State.normal)
//                    }
//                    else {
//                        if(response != nil)
//                        {
//                            let dictResponse = response! as NSDictionary
//                            if(dictResponse.value(forKey: "error") as? String != nil)
//                            {
//                                self.setPresentAlert(withTitle: AppName, message: dictResponse.value(forKey: "error") as! String)
//                            }
//                        }
//                        else
//                        {
//                            print(error!)
//                        }
//                        MBProgressHUD.hide(for: self.view, animated: true)
//                    }
//                })
//                break;
//
//            // Verify code
//            case 6:
//                let index = IndexPath(row: CurrentPage, section: 0)
//                let cell: SignupStep5VC = self.clvSignUpStep.cellForItem(at: index) as! SignupStep5VC
//                if(cell.txtOTP1.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
//                    return
//                }else if(cell.txtOTP2.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
//                    return
//                }else if(cell.txtOTP3.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
//                    return
//                }else if(cell.txtOTP4.text == ""){
//                    setPresentAlert(withTitle: AppName, message: "Please enter your OTP code.")
//                    return
//                }
//                let pin = cell.txtOTP1.text! + cell.txtOTP2.text! + cell.txtOTP3.text! + cell.txtOTP4.text!
//                MBProgressHUD.showAdded(to: self.view, animated: true)
//                CheckMobiService.init().verifyPin(checkMobiId, withPin: pin, withResponse: { (status, response, error) in
//                    if(status == kStatusSuccessWithContent && response != nil)
//                    {
//                        let dictData = response! as NSDictionary
//                        if(dictData["validated"] as! Int == 1)
//                        {
//                            self.insertDataDic["phone"] = dictData["number"] as! String
//                            self.insertDataDic["user_role"] = self.user_role
//                            self.insertDataDic["device_type"] = "I"
//                            self.insertDataDic["device_token"] = device_token
//                            self.insertDataDic["user_image"] = ""
//
//                            if(self.signUp == GOOGLE_LOGIN_URL)
//                            {
//                                self.serviceManager.callWebServiceWithPOST(webpath: BASE_URL + GOOGLE_LOGIN_URL, withTag: GOOGLE_LOGIN_URL, params: self.insertDataDic)
//                            }
//                            else if(self.signUp == FB_LOGIN_URL)
//                            {
//                                self.serviceManager.callWebServiceWithPOST(webpath: BASE_URL + FB_LOGIN_URL, withTag: FB_LOGIN_URL, params: self.insertDataDic)
//                            }
//                            else
//                            {
//                                self.serviceManager.callWebServiceWithPOST(webpath: BASE_URL + ADD_USER_URL, withTag: ADD_USER_URL, params: self.insertDataDic)
//                            }
//                        }
//                        else
//                        {
//                            MBProgressHUD.hide(for: self.view, animated: true)
//                            self.setPresentAlert(withTitle: AppName, message: "Invalid OTP.")
//                        }
//                    }
//                    else
//                    {
//                        MBProgressHUD.hide(for: self.view, animated: true)
//                        //                        self.setPresentAlert(withTitle: "", message: error as! String)
//                        print(error)
//                    }
//                })
//                return
//            default:
//                print("Default")
//                break;
//            }
//            CurrentPage += 1
//            if(CurrentPage < 7)
//            {
//                let indexPath = IndexPath(item: CurrentPage, section: 0)
//                clvSignUpStep.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//                self.setProgress(progress: CurrentPage)
//            }
//        }
//    }
//    @IBAction func btnPrevClick(_ sender: UIButton)
//    {
//        self.view.endEditing(true)
//        MoveTaxiBack()
//    }
//
//    func MoveTaxiBack()
//    {
//        if(CurrentPage <= 3 && !isNormalSignup)
//        {
//            self.navigationController?.popViewController(animated: true)
//            return
//        }
//        if(CurrentPage > 0){
//            CurrentPage -= 1
//
//            if((user_role == "1" && CurrentPage == 5) || user_role == "0" && CurrentPage == 3) {
//
//                btnNextSetCorner.setTitle("SEND OTP", for: UIControl.State.normal)
//            } else  {
//                btnNextSetCorner.setTitle("NEXT", for: UIControl.State.normal)
//            }
//            let indexPath = IndexPath(item: CurrentPage, section: 0)
//            clvSignUpStep.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//            self.setProgress(progress: CurrentPage)
//        }else{
//            self.navigationController?.popViewController(animated: true)
//        }
//    }
//
//    // MARK:- btn LoginNow
//    @IBAction func btnLoginNow(_ sender: UIButton)
//    {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//
//    // MARK:- Collectionview delegate
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
//    {
//        return user_role == "1" ? 7 : 5
//    }
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
//    {
//        if(indexPath.row == 0){
//            let nibName = UINib(nibName: "SignupStep1VC", bundle:nil)
//            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: "SignupStep1VC")
//            let cell = clvSignUpStep
//                .dequeueReusableCell(withReuseIdentifier: "SignupStep1VC", for: indexPath)as! SignupStep1VC
//            cell.viewFirstName.setviewRadius()
//            cell.viewLastName.setviewRadius()
//            cell.tfFirstName.delegate = self
//            cell.tfLastName.delegate = self
//            return cell
//        } else if(indexPath.row == 1) {
//            let nibName = UINib(nibName: "SignupStep2VC", bundle:nil)
//            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: "SignupStep2VC")
//            let cell = clvSignUpStep
//                .dequeueReusableCell(withReuseIdentifier: "SignupStep2VC", for: indexPath)as! SignupStep2VC
//            cell.viewEmail.setviewRadius()
//            cell.tfEmailID.delegate = self
//            return cell
//        }else if(indexPath.row == 2){
//            let nibName = UINib(nibName: "SignupStep3VC", bundle:nil)
//            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: "SignupStep3VC")
//            let cell = clvSignUpStep
//                .dequeueReusableCell(withReuseIdentifier: "SignupStep3VC", for: indexPath)as! SignupStep3VC
//            cell.viewPass.setviewRadius()
//            cell.lblPassWarning.isHidden = true
//            cell.tfPassword.delegate = self
//            cell.btnActionShowPassOnClick =
//                {
//                    if cell.btnShowPass.tag == 0{
//                        cell.tfPassword.isSecureTextEntry = false
//                        cell.btnShowPass.setImage(UIImage(named: "iconhidden"), for: UIControl.State.normal)
//                        cell.btnShowPass.tag += 1
//                    }
//                    else{
//                        cell.tfPassword.isSecureTextEntry = true
//                        cell.btnShowPass.setImage(UIImage(named: "iconShow"), for: UIControl.State.normal)
//                        cell.btnShowPass.tag -= 1
//                    }
//            }
//            return cell
//        }
//        else if(indexPath.row == 3 && user_role == "1")
//        {
//            let nibName = UINib(nibName: "SignupStep6VC", bundle:nil)
//            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: "SignupStep6VC")
//            let cell = clvSignUpStep.dequeueReusableCell(withReuseIdentifier: "SignupStep6VC", for: indexPath)as! SignupStep6VC
//            cell.viewVehicleNumber.setviewRadius()
//            return cell
//        }
//        else if(indexPath.row == 4 && user_role == "1")
//        {
//            let nibName = UINib(nibName: "SignupStep7VC", bundle:nil)
//            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: "SignupStep7VC")
//            let cell = clvSignUpStep
//                .dequeueReusableCell(withReuseIdentifier: "SignupStep7VC", for: indexPath)as! SignupStep7VC
//            return cell
//        }
//        else if(indexPath.row == 3 || indexPath.row == 5){
//            let nibName = UINib(nibName: "SignupStep4VC", bundle:nil)
//            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: "SignupStep4VC")
//            let cell = clvSignUpStep
//                .dequeueReusableCell(withReuseIdentifier: "SignupStep4VC", for: indexPath)as! SignupStep4VC
//            cell.viewMobileNum.setviewRadius()
//            cell.viewCountryCode.setviewRadius()
//            cell.tfMobileNum.delegate = self
//            return cell
//        }else{
//            let nibName = UINib(nibName: "SignupStep5VC", bundle:nil)
//            clvSignUpStep.register(nibName, forCellWithReuseIdentifier: "SignupStep5VC")
//            let cell = clvSignUpStep
//                .dequeueReusableCell(withReuseIdentifier: "SignupStep5VC", for: indexPath)as! SignupStep5VC
//            cell.btnResendCode.RoundedButtonWithBorderCLR(radius: cell.btnResendCode.frame.height/2, colour: UIColor.clear)
//            cell.btnActionEditNum =
//                {
//                    self.btnNextSetCorner.setTitle("SEND OTP", for: UIControl.State.normal)
//                    self.MoveTaxiBack()
//            }
//            cell.btnActionDidntRecieveCode = {
//                print("i didn't recieve code action called")
//            }
//            cell.btnActionResendCode = {
//                MBProgressHUD.showAdded(to: self.view, animated: true)
//                CheckMobiService.init().request(ValidationTypeSMS, forNumber: (self.insertDataDic["phone"] as! String), withResponse: { (status, response, error) in
//                    if(status == kStatusSuccessWithContent && response != nil){
//                        let dictResponse = response! as NSDictionary
//                        self.checkMobiId = (dictResponse.value(forKey: "id") as! String)
//                    }
//                    else {
//                        if(response != nil)
//                        {
//                            let dictResponse = response! as NSDictionary
//                            if(dictResponse.value(forKey: "error") as? String != nil)
//                            {
//                                self.setPresentAlert(withTitle: AppName, message: dictResponse.value(forKey: "error") as! String)
//                            }
//                        }
//                        else
//                        {
//                            print(error!)
//                        }
//                    }
//                    MBProgressHUD.hide(for: self.view, animated: true)
//                })
//            }
//            return cell
//        }
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
//        let width = collectionView.frame.size.width //SCREEN_WIDTH
//        let size = CGSize(width: width, height: collectionView.layer.frame.size.height)
//        return size
//    }
//
//    //    MARK:- initiliazation
//    func initiliazation()
//    {
//        serviceManager.delegate = self
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            let indexPath = IndexPath(item: self.CurrentPage, section: 0)
//            self.clvSignUpStep.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
//            //            self.clvSignUpStep.isHidden = false
//            self.setProgress(progress: self.CurrentPage)
//        }
//    }
//    //    MARK:- SetUpLayout
//    func SetUpLayout()
//    {
//        //        self.clvSignUpStep.isHidden = true
//    }
//
//    // MARKS:- API Response
//    func webServiceCallSuccess(_ response: Any?, forTag tagname: String?) {
//        if tagname == ADD_USER_URL || tagname == GOOGLE_LOGIN_URL || tagname == FB_LOGIN_URL || tagname == EDIT_IMAGE_URL
//        {
//            if(((response as! NSDictionary)["status"] as! Int) == 1)
//            {
//                let userData = (response as! NSDictionary)["data"] as! [String: Any]
//                if(userData["userrole"] as! Bool)
//                {
//                    setUserRole(data: "1")
//                    setDriverDetails(data: userData)
//                    setUserId(data: String(userData["driver_id"] as? Int ?? 0))
//                    print(getDriverDetails())
//                }
//                else
//                {
//                    setUserRole(data: "0")
//                    setRiderDetails(data: userData)
//                    setUserId(data: String(userData["user_id"] as? Int ?? 0))
//                    print(getRiderDetails())
//                }
//                if(tagname == GOOGLE_LOGIN_URL || tagname == FB_LOGIN_URL)
//                {
//                    var urlString: String!
//                    if tagname == FB_LOGIN_URL
//                    {
//                        urlString = "https://graph.facebook.com/\(userData["fb_id"] as! String)/picture?width=480&height=480"
//                    }
//                    else
//                    {
//                        urlString = googleImgUrl
//                    }
//
//                    setSocialLogin(data: true)
//                    do
//                    {
//                        let imageData = try Data(contentsOf: URL(string: urlString)!)
//                        let imageDic: [String: String] = ["user_role": getUserRole()!, "user_id": getUserId()!, "user_image": "data:image/jpeg;base64,\(imageData.base64EncodedString().replacingOccurrences(of: "\r\n", with: "")))"]
//
//                        serviceManager.callWebServiceWithPOST(webpath: BASE_URL + EDIT_IMAGE_URL, withTag: EDIT_IMAGE_URL, params: imageDic)
//                        return
//                    }
//                    catch let error {
//                        print("Error: \(error)")
//                    }
//                }
//                let vc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//            else
//            {
//                if(EDIT_IMAGE_URL == tagname)
//                {
//                    // Image Upload fail
//                    let vc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
//
//                setPresentAlert(withTitle: AppName, message: (response as! NSDictionary)["message"] as! String)
//                //                CurrentPage -= 1
//            }
//            MBProgressHUD.hide(for: self.view, animated: true)
//        }
//    }
//
//    func webServiceCallFailure(_ error: Error?, forTag tagname: String?) {
//        if tagname == ADD_USER_URL || tagname == GOOGLE_LOGIN_URL || tagname == FB_LOGIN_URL
//        {
//            //            error?.localizedDescription as! String
//            self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
//            MBProgressHUD.hide(for: self.view, animated: true)
//            //            CurrentPage -= 1
//        }
//        else if(EDIT_IMAGE_URL == tagname)
//        {
//            // Image Upload fail
//            self.setPresentAlert(withTitle: AppName, message: (error?.localizedDescription)!)
//            MBProgressHUD.hide(for: self.view, animated: true)
//
//            let vc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//    }
//}
