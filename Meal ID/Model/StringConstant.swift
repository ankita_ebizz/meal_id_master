//
//  StringConstant.swift
//  Meal ID
//
//  Created by EbitNHP-i1 on 04/03/20.
//  Copyright © 2020 EbitNHP-i1. All rights reserved.
//

import Foundation
 
let TempAuth = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoSWQiOiI1ZTZiMjZjMjg1YzM2NjZjZDU3MmM2OWYiLCJuYW1lIjoiU3VtaXQiLCJkb2IiOiIyMC0xMS0xOTk2IiwiYWdlIjoyMywiZ2VuZGVyIjoibWFsZSIsImVtYWlsIjoic2JAZWJpenppbmZvdGVjaC5jb20iLCJtb2JpbGVudW1iZXIiOiI3MDY0ODIzMjI1Iiwid2VpZ2h0IjoxNTAsImJvZHlmYXQiOjIwLjIsImxibSI6MCwiYm1yIjoyMCwibG9naW5fdHlwZSI6MCwic29jaWFsX2tleSI6IiIsImJvZHl0eXBlIjoiRWN0b21vcnBoIiwiY2FyYnMiOjUxLCJwcm90ZWluIjozMSwiZmF0IjoyMSwid29ya291dHR5cGUiOiIiLCJ3b3JrcGVyY2VudGFnZSI6MCwiZGlldHR5cGUiOjAsImdvYWwiOjAsImxldmVsdHlwZSI6IiIsImxldmVscGVyY2VudGFnZSI6MCwiaXNBY3RpdmUiOnRydWUsImlzTm90aWZpY2F0aW9uIjp0cnVlLCJwbGFudGRldGFpbHMiOnt9LCJpc1ByZW1pdW0iOmZhbHNlLCJpYXQiOjE1ODY3NzU4NDYsImV4cCI6MTU4OTM2Nzg0Nn0.WzXdZXt__zY06CZki8B80Hgz1vFZjA4MWDG7rtFxCkA"

let GooGle_ClientID = "552962212940-vnh7fcesbdvg4oek173v830qgcenm9c1.apps.googleusercontent.com"
var FireBaseToken = String()
//let checkMobiApikeys = "38FF9ADD-04EF-43CC-A90A-2EE70C81C1C6"
 
//dXdDuqcu4Et2oSq3Gu4BYf:APA91bEfHx4UHOcfeyzT-eL5ZUBlbBIBLKw0vX9LeBglsKL_CVon46I0kIbfGGW2BqFKoGZMKqYZB0kWjlsmZGUrc5UCGKcy819OjpWsO9yrnbBxgGwagY20BAHCOF2ocy3wJAORR2m9

let Feet_Inches = "Feet/Inches"
let Centimeters = "Centimeters"
let Imperial = "Imperial"
let Metric = "Metric"
let dateFormat = "dd/MM/yyyy"
let FontName = "HelveticaNeue-Light"

let warningMsg = "Something went to wrong!"
let warningMsg2 = "Not getting response from server"
let warningMsg3 = "Unable to connect with server"

let ectomorph = "ic_ectomorph"
let mesomorph = "ic_mesomorph"
let endomorph = "ic_endomorph"

let ectomorphSelect = "ic_ectomorphSelect"
let mesomorphSelect = "ic_mesomorphSelect"
let endomorphSelect = "ic_endomorphSelect"

let ectomorph_male = "ic_ectomorph_male"
let mesomorph_male = "ic_mesomorph_male"
let endomorph_male = "ic_endomorph_male"

let ectomorph_maleSelect = "ic_ectomorph_maleSelect"
let mesomorph_maleSelect = "ic_mesomorph_maleSelect"
let endomorph_maleSelect = "ic_endomorph_maleSelect"

let maleSelect = "ic_maleSelect"
let ic_male = "ic_male"
let ic_female = "ic_female"
let ic_femaleSelect = "ic_femaleSelect"
let ic_add = "ic_add"
let ic_checkbox = "ic_checkbox"
let ic_Uncheckbox = "ic_Uncheckbox"
let ic_right_yellow = "ic_right_yellow"
let ic_lock = "ic_lock"
let ic_leftarrow = "ic_leftarrow"


// MARK:- StoryBoard identifier
let id_HomePage = "HomePage"
let id_SignUpVC = "SignUpVC"
let id_ForgotPassVC = "ForgotPassVC"
let id_SignInVC = "SignInVC"
let id_SignUpInfoVC = "SignUpInfoVC"
let id_GenerateMealPlanVC = "GenerateMealPlanVC"
let id_SubscriptionCatVC = "SubscriptionCatVC"
let id_ChooseLevelVC = "ChooseLevelVC"
let id_PersonalDetailVC = "PersonalDetailVC"
let id_SettingBodyTypeVC = "SettingBodyTypeVC"
let id_SettingActivityLevelVC = "SettingActivityLevelVC"
let id_SettingGoalVC = "SettingGoalVC"
let id_SettingWeightLossVC = "SettingWeightLossVC"
let id_SettingBodyFatVC = "SettingBodyFatVC"
let id_SettingDietTypeVC = "SettingDietTypeVC"
let id_AccountSettingVC = "AccountSettingVC"
let id_SubscriptionVC = "SubscriptionVC"
let id_GroceriesMenuTVC = "GroceriesMenuTVC"
let id_SelectMacroVC = "SelectMacroVC"
let id_BodyFatCalcVC = "BodyFatCalcVC"
let id_UploadPhotoVC = "UploadPhotoVC"


// MARK:- XIB identifier
let id_CustomeWeightPicker = "CustomeWeightPicker"
let id_CustomSegmentButton = "CustomSegmentButton"
let id_CustomeDatePicker = "CustomeDatePicker"
let id_SignUpStep1CVC = "SignUpStep1CVC"
let id_SignUpStep2CVC = "SignUpStep2CVC"
let id_SignUpStep3CVC = "SignUpStep3CVC"
let id_SignUpStep4CVC = "SignUpStep4CVC"
let id_SignUpStep5CVC = "SignUpStep5CVC"
let id_SignUpStep6CVC = "SignUpStep6CVC"
let id_SignUpStep7CVC = "SignUpStep7CVC"
let id_SignUpStep8CVC = "SignUpStep8CVC"
let id_SignUpStep9CVC = "SignUpStep9CVC"
let id_DietTypeTVC = "DietTypeTVC"
let id_SelectGoalTVC = "SelectGoalTVC"
let id_WorkOutFreqTVC = "WorkOutFreqTVC"
let id_PersonalDetailCell = "PersonalDetailCell"
let id_AccountSettingCell = "AccountSettingCell"
let id_SubscriptionOfferDescTVC = "SubscriptionOfferDescTVC"
let id_SubscriptionCategoryCell1 = "SubscriptionCategoryCell1"
let id_SubscriptionCategoryCell2 = "SubscriptionCategoryCell2"
let id_SettingTVC = "SettingTVC"
let id_GenerateMealStep1CVC = "GenerateMealStep1CVC"
let id_GenerateMealStep2CVC = "GenerateMealStep2CVC"
let id_GenerateMealStep3CVC = "GenerateMealStep3CVC"
let id_GenerateMealStep6CVC = "GenerateMealStep6CVC"
let id_instructionCell = "instructionCell"
let id_ChooseLevelTVC = "ChooseLevelTVC"
let id_macroCell = "macroCell"
let id_UploadPhotoTVC = "UploadPhotoTVC"
let id_PickerForActivity = "PickerForActivity"
let id_MealListCVC = "MealListCVC"
let id_WaterMeterCVC = "WaterMeterCVC"
let id_MealItemSubCategopryTVC = "MealItemSubCategopryTVC"

