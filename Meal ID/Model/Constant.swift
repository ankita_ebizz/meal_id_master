//
//  Constant.swift
//  PalaceApp
//
//  Created by EbitNHP-i1 on 01/11/19.
//  Copyright © 2019 EbitNHP-i1. All rights reserved.
//

import Foundation
import UIKit

let AppName = "Meal ID"
//let K_isOnBoardSlideShow = "isOnBoard"
let K_isLogIn       = "isLogIn"
let K_isinstalled   = "isInstalled" //kakarot

//MARK: - user deafault
var user = UserDefaults()
var registerationData = NSMutableDictionary()
var insertMealPlanDic = [String: Any]()

let SignUpStoryboard = UIStoryboard(name: "SignUp", bundle: nil)
let MainInStoryboard = UIStoryboard(name: "Main", bundle: nil)

let SegmentBGColour = UIColor(red: 34/255, green: 34/255, blue: 35/255, alpha: 1.0)
let SegmentTitleColour = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1.0)

let lightGrey = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1.0)
let purpleColour = UIColor(red: 97/255, green: 34/255, blue: 61/255, alpha: 1.0)
let greyCellColour = UIColor(red: 25.0/255.0, green: 25.0/255.0, blue: 28.0/255.0, alpha: 1.0)

let yellowColourCalendarBG = UIColor(red: 243/255, green: 219/255, blue: 29/255, alpha: 0.16)

let yellowColour = UIColor(red: 243/255, green: 219/255, blue: 29/255, alpha: 1.0)
let greenColour = UIColor(red: 161/255, green: 204/255, blue: 46/255, alpha: 1.0)
let cyanColour = UIColor(red: 0/255, green: 246/255, blue: 208/255, alpha: 1.0)
let orangeColour = UIColor(red: 216/255, green: 112/255, blue: 53/255, alpha: 1.0)
let bgCell = UIColor(red: 25/255, green: 25/255, blue: 28/255, alpha: 1.0)
let CellLabelTextColour = UIColor(red: 83/255, green: 83/255, blue: 83/255, alpha: 1.0)

 // MARK:- User Default Keys
let UserLatitude = "UserLatitude"
let UserLongitude = "UserLongitude"

// MARK:- global var
var wieghtAndFatPicker = "" //WeightpickerVal , HeightpickerVal , FatpickerVal
var PickerForActivityTracker = ""
var leftPickerValue = ""
var RightPickerValue = ""

// MARK:- registeration variable
var registerationDOB = String()
//var registerationDOBAlert = String()
var DOBAge = Int()

var Gender = String()


// MARK:- APIs
let BaseURL = "http://18.212.2.14:9000/"
//let BaseUrlImage = "https://dev.xyz.com/"

let Registeration = "rest/user/register"
let LogIn = "rest/user/login"
let PersonalDetails = "rest/user/getProfile"
let GetAccountDetails = "rest/user/getAccount"
let DeleteAccount = "rest/user/deleteAccount"
let ChangePassword = "rest/user/changePassword"
let ForgotPassword = "rest/user/forgotPassword"
let GetUserMealPlan = "rest/user/getUserMealPlan"
 let GroceriesList = "rest/user/userGroceriesList"
let GenerateMealPlan = "rest/user/generateMealPlan"
let UserUploadPhoto = "rest/user/userUploadPhoto"
let Userphotolist = "rest/user/userphotolist"
let userdeletephoto = "rest/user/userdeletephoto/" //:photo_id"
let switchNotification = "rest/user/switchNotification"
let ListplanSubscription = "rest/premiumplan/listplan"
let SwapMealFood = "rest/user/swapMealFood"
let UserCurrentStatus = "rest/user/userCurrentStatus"
 
let PlanSubscribe = "rest/user/userPlanSubscribe"
let SubscriptionCheck = "userSubscriptionCheck"
let GroceriePdf = "rest/user/groceriePdfGenerate"
let MealplanPdf = "rest/user/mealplanpdfGenerate"
let UserActivityGraph = "rest/user/userActivityGraph"

// update api
let UpdateBodyType = "rest/user/updateBodytype"
let UpdatePhysicalActivity = "rest/user/updatePhysicalActivity"
let UpdateGoal = "rest/user/updateGoal"
let UpdateWeight = "rest/user/updateWeight"
let UpdateDietType = "rest/user/updateDietType"
let UpdateBodyfat = "rest/user/updateBodyfat"
let UpdateNotificationData = "rest/user/update_weight_bodyfat"
