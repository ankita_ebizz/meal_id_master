//
//  AttachmentViewModel.swift
//  SwiftProjectStructure
//

import UIKit

class AttachmentViewModel: NSObject {
    var Image = UIImage()
    var ImageFileName = ""
}

class Event: NSObject {
    var name: String = ""
    var notes: String = ""
    var dateStart = Date()
    var dateEnd = Date()
    var url: URL = URL(string:"https://loqiva.com")!
}
